//
//  QKeyPair.h
//  quis
//
//  Created by Will Skinner on 11/18/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sodium.h>

@interface QKeyPair : NSObject
{
    unsigned char encryption_key_public[crypto_box_PUBLICKEYBYTES];
    unsigned char encryption_key_private[crypto_box_SECRETKEYBYTES];
    unsigned char signing_key_public[crypto_sign_PUBLICKEYBYTES];
    unsigned char signing_key_private[crypto_sign_SECRETKEYBYTES];
}

- (id)initWithEncryptionKeyPublic:(NSData *)eKPu encryptionKeyPrivate:(NSData *)eKPr signingKeyPublic:(NSData *)sKPu signingKeyPrivate:(NSData *)skPr;

- (void)generateKeyPair;
- (NSData *)encryptData:(NSData *)data nonce:(NSData *)nonce forKeyPair:(QKeyPair *)receiver;
- (NSData *)decryptData:(NSData *)data nonce:(NSData *)nonce fromKeyPair:(QKeyPair *)sender;

- (NSData *)signData:(NSData *)data;
- (NSData *)verifySignedData:(NSData *)data;

- (NSData *)encryptionKeyPublic;
- (NSData *)encryptionKeyPrivate;

- (NSData *)signingKeyPublic;
- (NSData *)signingKeyPrivate;


+ (NSData *)nextNonce;

@end
