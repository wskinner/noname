//
//  NSMutableArray+Queue.h
//  quis
//
//  Created by Will Skinner on 12/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Queue)

// remove and return the item at the front of the queue
- (id)deque;

// enqueue an item at the back of the queue
- (void)enqueue:(id)item;

@end
