////
////  KeyExchange.m
////  quis
////
////  Created by Will Skinner on 10/20/13.
////  Copyright (c) 2013 Admin. All rights reserved.
////
//
//#import "KeyExchange.h"
//
//@interface KeyExchange ()
//@property (strong,nonatomic) QAppDelegate* delegate;
//@property (strong, nonatomic) MCPeerID* peerID;
//@end
//
//@implementation KeyExchange
//- (id)init {
//    self = [super init];
//    
//    self.delegate= [[UIApplication sharedApplication] delegate];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(peerDidConnect:) name:@"peerConnected" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(peerDidDisconnect:) name:@"peerDisconnected" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(peerDidReceivedData:) name:@"peerDataReceived" object:nil];
//
//    return self;
//}
//
//#pragma mark peer connected
//
//-(void) peerDidConnect:(NSNotification*) note {
//    
//    self.peerID = note.userInfo[@"peerID"];
//    
//}
//
//#pragma mark peer disconnected
//
//-(void) peerDidDisconnect:(NSNotification*) note {
//    self.peerID = nil;
//}
//
//#pragma send
//
//-(void) sendToPeer:(NSData*) msg {
//    NSError* error;
//    
//    [self.delegate.p2pHelper.sessionLocal.session sendData:msg toPeers:@[self.peerID] withMode:MCSessionSendDataReliable error:&error];
//}
//
//#pragma mark rec data
//
//-(void) peerDidReceivedData:(NSNotification*) note {
//    NSLog(@"didReceiveData: %@",[[NSString alloc] initWithData:note.userInfo[@"data"] encoding:NSUTF8StringEncoding]);
//}


//- (Boolean)exchangeKeysOverSocket:(QSocket *)socket forEmailAddress:(NSString *)emailAddress {
//    SecQContacts *friend = [QPersistenceHelpers queryEmailAddress:emailAddress];
//    SecQContacts *dqc = [QPersistenceHelpers getDqc];
//    // Message types:
//    // CERT
//    // CHLNG
//    // ERROR
//    // ACK
//    
//    NSString *theirCode = nil;
//    NSString *myCode = nil;
//    while (![[friend stateMine] isEqualToString:@"CONFIRMED"] || ![[friend stateTheirs] isEqualToString:@"CONFIRMED"]) {
//        // send a cert if we need to
//        if (![friend stateMine]) {
//            // send the cert and move the state
//            SecQMessage *msg = [[SecQMessage alloc] initForRecipient:friend fromSender:dqc];
//            [msg setMessageType:@"CERT"];
//            [msg setContent:[dqc certificate]];
//            [socket sendQMessage:msg];
//            [friend setStateMine:@"CERT sent"];
//        }
//        
//        if (myCode && theirCode) {
//            // ask user to confirm their code
//            Boolean themConfirmed;
//            if (themConfirmed) {
//                theirCode = nil;
//                [friend setStateTheirs:@"CONFIRMED"];
//                continue;
//            }
//        }
//        
//        SecQMessage *receivedMsg = [socket receiveMessage];
//        if ([[receivedMsg messageType] isEqualToString:@"CERT"]) {
//            Certificate *cert = [[Certificate alloc] initFromBlob:[receivedMsg content]];
//            [friend setSigningKeyPublic:[cert signingKeyPublic]];
//            [friend setEncryptionKeyPublic:[cert encryptionKeyPublic]];
//            [friend setCertificate:[receivedMsg content]];
//            
//            NSUInteger challengeNum = *[KeyPair nextNonce] % 10000;
//            NSString *challengeStr = [[NSString alloc] initWithFormat:@"%d", (unsigned int)challengeNum];
//            myCode = challengeStr;
//            NSData *challengeData = [challengeStr dataUsingEncoding:NSUTF8StringEncoding];
//            SecQMessage *challengeMsg = [[SecQMessage alloc] initForRecipient:friend fromSender:dqc];
//            [challengeMsg setContent:challengeData];
//            [challengeMsg setMessageType:@"CHLNG"];
//            [socket sendQMessage:challengeMsg];
//            [friend setStateTheirs:@"CHLNG sent"];
//        } else if ([[receivedMsg messageType] isEqualToString:@"CHLNG"]) {
//            [receivedMsg decrypt];
//            theirCode = [[NSString alloc] initWithData:[receivedMsg content] encoding:NSUTF8StringEncoding];
//            [friend setStateMine:@"CHLNG received"];
//        } else if ([[receivedMsg messageType] isEqualToString:@"CONFIRMED"]) {
//            [friend setStateMine:@"CONFIRMED"];
//        } else {
//            NSLog(@"Unrecognized message type: %@", [receivedMsg messageType]);
//        }
//        
//    }
//    return [[friend stateTheirs] isEqualToString:@"CONFIRMED"] && [[friend stateMine] isEqualToString:@"CONFIRMED"];
//    // receive their cert
//    
//    // encrypt and send a challenge
//    
//    // receive and confirm their response
//    
//    // send an ACK
//    
//    
//    
//    // send them my cert
//    
//    // receive their challenge
//    
//    // decrypt and send a response to their challenge
//    
//    // recieve their ACK
//    
//}
//@end
