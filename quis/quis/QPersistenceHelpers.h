//
//  QPersistenceHelpers.h
//  quis
//
//  Created by Admin on 16/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UID.h"
#import "SecQContacts.h"
#import <MailCore/MailCore.h>
#import "QAppDelegate.h"

@interface QPersistenceHelpers : NSObject

+ (bool)queryMessageUID:(NSNumber*) uid;

+ (void)insertUID:(NSNumber*) uid;

+ (SecQContacts*)queryEmailAddress:(NSString*) queryAddress;
+ (SecQContacts *)getDqc;
+ (NSManagedObjectContext *)getManagedObjectContext;

+ (void) refreshQuis:(UIRefreshControl*) refreshControl;

+ (void) fetchMessages;

@end
