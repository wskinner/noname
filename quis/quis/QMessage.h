//
//  QMessage.h
//  quis
//
//  Created by Will Skinner on 11/11/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QContacts;

@interface QMessage : NSManagedObject

@property (nonatomic, retain) NSData * content;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * isEncrypted;
@property (nonatomic, retain) NSNumber * messageId;
@property (nonatomic, retain) NSString * messageType;
@property (nonatomic, retain) NSData * nonce;
@property (nonatomic, retain) QContacts *recipient;
@property (nonatomic, retain) QContacts *sender;

@end
