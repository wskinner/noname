//
//  Retryer.m
//  quis
//
//  Created by Will Skinner on 12/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "Retryer.h"
#import "NSMutableArray+Queue.h"

@interface Retryer ()
// round robin processed queue
@property NSMutableArray *retryQueue;

@property NSMutableDictionary *messages;

@end

@implementation Retryer
@synthesize parent;

- (id)init {
    self = [super init];
    if (self) {
        self.retryQueue = [[NSMutableArray alloc] init];
        self.messages = [[NSMutableDictionary alloc] init];
        
        // will first fire in 0.1 seconds
        NSTimer *timer = [[NSTimer alloc] initWithFireDate:[[NSDate alloc] initWithTimeIntervalSinceNow:0.1] interval:0.1 target:self selector:@selector(retry) userInfo:nil repeats:YES];
    }
    return self;
}

- (void)notifyMessageSent:(SecQMessage *)msg {
    [self.retryQueue enqueue:msg];
    
    [self.messages setObject:msg forKey:msg.messageId];
    
}

- (void)notifyAckReceived:(NSUInteger)msgId {
    SecQMessage *msgToRemove = [self.messages objectForKey:[NSString stringWithFormat:@"%ul", msgId]];
    [self.retryQueue removeObject:msgToRemove];
    NSLog(@"removed message with id=%ul from the retry queue", msgId);
}

- (void)retry {
    SecQMessage *toRetry = [self.retryQueue deque];
    [QNetHelper sendMessage:[toRetry toBlob] onStream:((QKeyExchange *)self.parent).outputStream];
    NSLog(@"Retrying message with id=%@", toRetry.messageId);
    [self.retryQueue enqueue:toRetry];
}
@end

