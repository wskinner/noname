//
//  SecQMessage.h
//  quis
//
//  Created by Will Skinner on 10/12/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "QMessage.h"
#import "SecQContacts.h"
#import "KeyPair.h"


@interface SecQMessage : QMessage

@property (nonatomic, strong, retain) SecQContacts *sender;
@property (nonatomic, strong, retain) SecQContacts *recipient;



- (void)initializeFromBase64Blob:(NSData *)blob __attribute__((deprecated));
- (void)initializeFromBlob:(NSData *)blob;
- (void)initializeForRecipient:(SecQContacts *)qRecipient fromSender:(SecQContacts *)qSender;

- (NSData *)toBase64Blob __attribute__((deprecated));
- (NSData *)toBlob;

- (void) encrypt;
- (void) decrypt;

- (BOOL)encrypted;
@end
