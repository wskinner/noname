//
//  KeyDerivation.m
//  firstTry
//
//  Created by Will Skinner on 9/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//
//

#import "KeyDerivationFunction.h"
#import "HMACPRF.h"

@implementation KeyDerivationFunction

- (id)initWithSalt:(NSData *)salt usingIterations:(NSUInteger)iterations {
    self = [super init];
    if (self) {
        [self setSalt:salt];
        [self setIterations:iterations];
    }
    return self;
}

// The algorithm implemented is PBKDF2
// See https://en.wikipedia.org/wiki/PBKDF2
// DK = PBKDF2(PRF, Password, Salt, c, dkLen)
//
// DK = T1 || T2 || ... || Tdklen/hlen
// Ti = F(Password, Salt, Iterations, i)
// F(Password, Salt, Iterations, i) = U1 ^ U2 ^ ... ^ Uc
// U1 = PRF(Password, Salt || INT_msb(i))
// U2 = PRF(Password, U1)
// ...
// Uc = PRF(Password, Uc-1)
- (NSData *)pbkdf2:(NSData *)password {
    if (![self salt]) {
        return NULL;
    }
    if (![self prf]) {
        [self setPrf:[[HMACPRF alloc] init]];
    }
    if (![self iterations]) {
        [self setIterations:10000];
    }
    if (![self keyLength]) {
        [self setKeyLength:64];
    }
    
    NSData * dk = [self computePBKDF2WithPRF:[self prf] forPassword:password withSalt:[self salt] usingIterations:[self iterations] withKeyLength:[self keyLength]];
    return dk;
}

- (NSData *)computePBKDF2WithPRF:(PseudoRandomFunction *)prf forPassword:(NSData *)password withSalt:(NSData *)salt usingIterations:(NSUInteger)iterations withKeyLength:(NSUInteger)keyLength {
    NSData *digest;
    NSData *rawData;
    int length;
    for (int i = 0; i<[self iterations]; i++) {
        char *raw;
        uint32_t i_msb = CFSwapInt32HostToBig(i);
        if (i == 0) {
            length = [salt length] + 4;
            raw = malloc(length);
            memcpy(raw, [salt bytes], [salt length]);
            memcpy(&raw[[salt length]], &i_msb, 4);
        } else {
            length = [digest length];
            raw = malloc(length);
            memcpy(raw, [digest bytes], length);
        }
        rawData = [[NSData alloc] initWithBytes:raw length:length];
        digest = [prf computeHash:rawData withKey:password];
        memset(raw, 0, length);
        // NSLog([digest base64EncodedStringWithOptions:0]);
    }
    return digest;
}

- (NSData *)deriveKey:(NSData *)password {
    return [self pbkdf2:password];
}

@end
