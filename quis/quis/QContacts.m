//
//  QContacts.m
//  quis
//
//  Created by Will Skinner on 12/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QContacts.h"
#import "QMessage.h"


@implementation QContacts

@dynamic certificateData;
@dynamic certificateVersion;
@dynamic emailAddress;
@dynamic encryptionAlgorithm;
@dynamic encryptionKeyPrivate;
@dynamic encryptionKeyPublic;
@dynamic firstName;
@dynamic lastName;
@dynamic mostRecentMessageDate;
@dynamic signingAlgorithm;
@dynamic signingKeyPrivate;
@dynamic signingKeyPublic;
@dynamic status;
@dynamic messageIds;
@dynamic uid;
@dynamic qMessages;

@end
