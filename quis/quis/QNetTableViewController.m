//
//  QNetTableViewController.m
//  quis
//
//  Created by Admin on 28/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QNetTableViewController.h"

@interface QNetTableViewController () <NSNetServiceBrowserDelegate,QBrowserDelegate,QAdvertiserDelegate,QDNSServiceDelegate,NSStreamDelegate>

@property (strong, nonatomic) IBOutlet UITableView *peersTable;
@property (nonatomic, strong) NSMutableArray *nearbyPeers;


@property (strong, nonatomic) NSString* emailAddress;
@property (strong, nonatomic) NSString* selectedHost;

@property (strong, nonatomic) NSMutableDictionary* nameToResolvedHost;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@property (assign,readwrite) BOOL isAdvertising;

@property (strong, nonatomic) QKeyExchange *keyXDelegate;

- (void)cleanUp;
- (void)keyExchangeFinished;
@end

@implementation QNetTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.nearbyPeers = [[NSMutableArray alloc] init];
    self.nameToResolvedHost = [[NSMutableDictionary alloc] init];
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    self.activityIndicator.color = [UIColor blueColor];
    
    [self.activityIndicator startAnimating];
    
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:self.activityIndicator];
    [[self navigationItem] setRightBarButtonItem:barButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(peersDidAdd:) name:@"foundPeer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(peersDidLose:) name:@"lostPeer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearPeers) name:@"clearPeers" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionAccepted:) name:@"connectionAccepted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyExchangeFinished) name:@"keyExchangeFinished" object:nil];
    
    QAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    [delegate.qNetHelper startAll];
    
}

-(void) viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"foundPeer" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"lostPeer" object:nil];
    
    if  ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
        //clean up here
        NSLog(@"Back button pressed");
        NSLog(@"Stop all services");
        
        [self cleanUp];
    }
    
}

- (void)cleanUp {
    QAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    [delegate.qNetHelper stopAll];
    
    [self.nearbyPeers removeAllObjects];
    [self.nameToResolvedHost removeAllObjects];

}

- (void)keyExchangeFinished {
    [self cleanUp];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
# pragma mark clear peers if we go into BG

-(void) clearPeers {
    [self.nearbyPeers removeAllObjects];
    [self.nameToResolvedHost removeAllObjects];
    [self.peersTable reloadData];
}

# pragma mark found peer

-(void) peersDidAdd:(NSNotification*) note {
    if (![self.nearbyPeers containsObject:note.userInfo[@"resolvedHost"]])
        [self.nearbyPeers addObject:note.userInfo[@"resolvedHost"]];
    self.nameToResolvedHost[note.userInfo[@"name"]] =  note.userInfo[@"resolvedHost"];
    [self.peersTable reloadData];
}

-(void) peersDidLose:(NSNotification*) note {
    NSLog(@"peersDidLose name %@",note.userInfo[@"name"]);
    [self.nearbyPeers removeObject:self.nameToResolvedHost[note.userInfo[@"name"]]];
    [self.peersTable reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [self.nearbyPeers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"QNetCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = [self.nearbyPeers objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    self.selectedHost = [self.nearbyPeers objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"netConnect" sender:self];
    
}

- (void)connectionAccepted:(NSNotification *)note {
    self.keyXDelegate = note.userInfo[@"keyExchangeDelegate"];
    [self performSegueWithIdentifier:@"netConnect" sender:self];
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"netConnect"]) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        QNetConnectionViewController *vc = [segue destinationViewController];
        vc.resolvedHost = self.selectedHost;
        if (self.keyXDelegate) {
            vc.serverMode = YES;
            vc.keyXDelegate = self.keyXDelegate;
            ((QKeyExchange *)vc.keyXDelegate).caller = vc;
        } else {
            vc.serverMode = NO;
        }
    }
}




@end
