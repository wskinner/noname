////
////  QConnectionViewController.m
////  quis
////
////  Created by Admin on 26/10/2013.
////  Copyright (c) 2013 Admin. All rights reserved.
////
//
//#import "QConnectionViewController.h"
//
//@interface QConnectionViewController ()
//@property (strong, nonatomic) IBOutlet UILabel *peerIDdisplayname;
//@property (strong, nonatomic) IBOutlet UILabel *connectionStatus;
//
//@property (strong,nonatomic) QAppDelegate* delegate;
//@end
//
//@implementation QConnectionViewController
//
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//	// Do any additional setup after loading the view.
//    
//    self.delegate= [[UIApplication sharedApplication] delegate];
//
//    NSLog(@"Sending invitation to %@",self.peerID);
//    [self.delegate.p2pHelper.serviceBrowser invitePeer:self.peerID toSession:self.delegate.p2pHelper.sessionLocal.session withContext:nil timeout:15.0f];
//
//    self.peerIDdisplayname.text = self.peerID.displayName;
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(peerDidConnect:) name:@"peerConnected" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(peerDidDisconnect:) name:@"peerDisconnected" object:nil];
//}
//
//-(void) viewWillDisappear:(BOOL)animated {
//    // will we always be discon from session? really we just want remote to know conn is dead
//    //[self.delegate.p2pHelper.sessionLocal.session disconnect];
//}
//
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//#pragma send
//
//-(void) sendToPeer:(NSData*) msg {
//    NSError* error;
//    
//    [self.delegate.p2pHelper.sessionLocal.session sendData:msg toPeers:@[self.peerID] withMode:MCSessionSendDataReliable error:&error];
//}
//
//#pragma mark peer connected
//
//-(void) peerDidConnect:(NSNotification*) note {
//    self.connectionStatus.text = @"Connected";
//}
//
//#pragma mark peer disconnected
//
//-(void) peerDidDisconnect:(NSNotification*) note {
//    self.connectionStatus.text = @"Disconnected";
//}
//
//@end
