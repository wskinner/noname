//
//  SecQMessage.m
//  quis
//
//  Created by Will Skinner on 10/12/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "SecQMessage.h"


@implementation SecQMessage
@synthesize  content, date, messageId, sender, recipient, messageType, isEncrypted, nonce;

- (void)initializeForRecipient:(SecQContacts *)qRecipient fromSender:(SecQContacts *)qSender {
    self.recipient = qRecipient;
    self.sender = qSender;
    [self getNonce];
    
    NSNumber *num = [[NSNumber alloc] initWithInteger:[qSender nextMessageIdForRecipient:qSender]];
    self.messageId = num;
    
}

// blob should be {"content": {...}}
- (void)initializeFromBlob:(NSData *)blob {
    NSError *error;
    NSDictionary *blobJson = [NSJSONSerialization JSONObjectWithData:blob options:NSJSONReadingMutableContainers error:&error];
    NSDictionary *json = blobJson[@"content"];
    if (error) {
        NSLog(@"Error initializing message from JSON blob");
        NSLog(@"Error: %@", error);
        return;
    }
    self.content = [[json valueForKey:@"content"] base64DecodedData];
    self.date = [self dateFromString:[json valueForKey:@"date"]];
    self.isEncrypted = [json valueForKey:@"isEncrypted"];
    self.messageId = [json valueForKey:@"messageId"];
    self.messageType = [json valueForKey:@"messageType"];
    self.nonce = [[json valueForKey:@"nonce"] base64DecodedData];
}

- (void)initializeFromBase64Blob:(NSData *)blob {
    //NSData *rawData = [[NSData alloc] initWithBase64EncodedData:blob options:0];
    //[self initializeFromBlob:rawData];
    [self initializeFromBlob:blob];
}

- (NSData *)toBase64Blob {
    return [self toBlob];
    //return [[self toBlob] base64EncodedDataWithOptions:0];
}

- (NSData *)toBlob {
    NSDictionary *json = [self toJson];
    NSError *err;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                       options:0
                                                         error:&err];
    return jsonData;
}

/*
 * Strings and numbers are left as is. Data is base64 encoded.
 */
- (NSMutableDictionary *)toJson {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:[self.content base64EncodedString] forKey:@"content"];
    if (!self.date) {
        self.date = [[NSDate alloc] init];
    }
    [data setValue:[self stringFromDate] forKey:@"date"];
    [data setValue:self.isEncrypted forKey:@"isEncrypted"];
    [data setValue:self.messageId forKey:@"messageId"];
    if (!self.messageType) {
        self.messageType = @"message";
    }
    [data setValue:self.messageType forKey:@"messageType"];
    [data setValue:[self.nonce base64EncodedString] forKey:@"nonce"];
    
    NSMutableDictionary *json = [[NSMutableDictionary alloc] init];
    json[@"content"] = data;
    
    return json;
}

- (NSData *)getNonce {
    if (!self.nonce) {
        char *nonce_bytes = malloc(crypto_box_NONCEBYTES * sizeof(char));
        randombytes_buf(nonce_bytes, crypto_box_NONCEBYTES);
        self.nonce = [[NSData alloc] initWithBytes:nonce_bytes length:crypto_box_NONCEBYTES];
        free(nonce_bytes);
    }
    return self.nonce;
}

// Requires that sender, recipient fields are set
- (void)encrypt {
    NSData *encrypted = [self.sender.keyPair encryptData:self.content nonce:[self getNonce] forKeyPair:self.recipient.keyPair];
    if (encrypted) {
        self.content = encrypted;
        self.isEncrypted = @YES;
        NSLog(@"Encryption succeeded");
    } else {
        NSLog(@"Encryption failed");
    }
}

// Requires that sender, recipient fields are set
- (void) decrypt {
    NSData *decrypted = [self.recipient.keyPair decryptData:self.content nonce:self.nonce fromKeyPair:self.sender.keyPair];
    if (decrypted) {
        self.content = decrypted;
        self.isEncrypted = @NO;
        NSLog(@"Decryption succeeded");
    } else {
        NSLog(@"Decryption failed");
    }

}

- (NSString *)stringFromDate {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString* dateString = [dateFormatter stringFromDate:self.date];
    return dateString;
}

- (NSDate *)dateFromString:(NSString *)dateString {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
    return [dateFormatter dateFromString:dateString];
}

- (BOOL)encrypted {
    return [self.isEncrypted  isEqual: @YES];
}

@end
