//
//  PublicKey.h
//  firstTry
//
//  Created by Will Skinner on 8/31/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PublicKey : NSObject {
    unsigned char *public_key;
}
@property (atomic) unsigned char *public_key;
@property (atomic) unsigned char *public_signing_key;

- (NSData *)encKeyData;
- (NSData *)signKeyData;
@end
