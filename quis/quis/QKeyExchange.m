//
//  QKeyExchange.m
//  quis
//
//  Created by Will Skinner on 11/17/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QKeyExchange.h"
#import "NSMutableArray+Queue.h"
#import "JsonStreamManager.h"
#import "Retryer.h"

@interface QKeyExchange () <KeyExchangeDelegate>

@property JsonStreamManager *streamManager;
@property Retryer *retryer;

@end

@implementation QKeyExchange
@synthesize inputStream, outputStream, moc, dqc, friend, serverMode, caller, myCode, theirCode, streamManager;

- (id)initWithOutputStream:(NSOutputStream *)out inputStream:(NSInputStream *)in managedObjectContext:(NSManagedObjectContext *)managedObjectContext serverMode:(BOOL)mode caller:(id)callingClass {
    self = [super init];
    if (self) {
        self.inputStream = in;
        self.outputStream = out;
        self.moc = managedObjectContext;
        
        self.dqc = [QPersistenceHelpers getDqc];
        [self.dqc generateCertificate];
        
        self.friend = (SecQContacts *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQContacts" inManagedObjectContext:self.moc];
        
        self.serverMode = mode;
        
        self.hasSpace = NO;
        self.hasSentHello = NO;
        
        //self.handleConfirmChallengeDelegate = [[HandleConfirmChlngDelegate alloc] initWithCaller:self];
        
        self.alerts = [[NSMutableArray alloc] init];
        
        self.caller = callingClass;
        
        self.streamManager = [[JsonStreamManager alloc] init];
        
        self.retryer = [[Retryer alloc] init];
        
        NSLog(@"Initialized key exchange");
        
    }
    return self;
}

// show the next alert
- (void)showAlert {
    NSLog(@"showAlert called");
    NSLog(@"alertCondition locked");

    UIAlertView *alert = self.alerts[0];
    [self.alerts removeObjectAtIndex:0];
    [alert show];
}

- (void)handleCert:(SecQMessage *)message {
    NSLog(@"Received a CERT");
    Certificate *cert = [[Certificate alloc] initFromBase64Blob:message.content];
    [self.friend setSigningKeyPublic:cert.signingKeyPublic];
    [self.friend setEncryptionKeyPublic:cert.encryptionKeyPublic];
    [self.friend setCertificate:cert];
    [self.friend populateInfoFromCert:cert];
    
    NSLog(@"friend's email address is: %@", self.friend.emailAddress);
    NSLog(@"Friend's certificate = %@", [[NSString alloc] initWithData:[cert toBlob] encoding:NSUTF8StringEncoding]);
    
    NSUInteger challengeNum = (unsigned int)[QKeyPair nextNonce] % 10000;
    NSString *challengeStr = [[NSString alloc] initWithFormat:@"%d", (unsigned int)challengeNum];
    self.myCode = challengeStr;
    
    NSLog(@"QKeyExchange calling putMyCode");
    [self.caller putMyCode:challengeStr];
    
    NSData *challengeData = [challengeStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"My challenge string = %@", challengeStr);
    
    SecQMessage *challengeMsg = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:self.moc];
    [challengeMsg initializeForRecipient:self.friend fromSender:self.dqc];
    
    [challengeMsg setContent:challengeData];
    [challengeMsg setMessageType:@"CHLNG"];
    [challengeMsg encrypt];
    self.chlngMsg = challengeMsg;
    NSLog(@"Stored chlngMsg for sending later");
}

// server should deal with this first
- (void)handleConfirmChlng:(NSString *)expectedCode {
    NSLog(@"Waiting for user to confirm that their friend was able to decrypt the CHLNG");
}

// client should deal with this first
- (void)handleChlng:(SecQMessage *)message {
    NSLog(@"Received a CHLNG");

    NSString *content = [[NSString alloc] initWithData:message.content encoding:NSUTF8StringEncoding];
    NSLog(@"Friend's CHLNG = %@", content);
    
    self.theirCode = content;
    NSLog(@"QKeyExchange calling putTheirCode");
    [self.caller putTheirCode:content];
}

- (void)handleChlngResp:(SecQMessage *)message {
    NSLog(@"Received a CHLNGRESP");
    
    NSString *result = [[NSString alloc] initWithData:message.content encoding:NSUTF8StringEncoding];
    NSLog(@"Friend's CHLNGRESP = %@", result);
    
    if ([result isEqualToString:@"YES"]) {
        self.myCertConfirmed = YES;
        if (self.theirCertConfirmed) {
            [self keyExchangeDidSucceed];
        } else {
            NSLog(@"Friend's state still not confirmed");
            if (self.hasSpace) {
                [self hasSpaceAvailable];
            }
            
        }
    } else {
        NSLog(@"Key exchange failed because my code was not verified");
        [self keyExchangeDidFail];
    }
}

- (void)keyExchangeDidSucceed {
    self.friend.status = @"CONFIRMED";
    NSLog(@"Key Exchange complete.");
    
    NSError *err;
    [self.moc save:&err];
    if (err) {
        NSLog(@"Error: %@", err);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"keyExchangeDone" object:nil];
    
    [self.caller keyXDone];
    [QKeyExchange keyExchangeDidFinish];
}

- (void)keyExchangeDidFail {
    self.friend.status = @"FAILED";
    NSLog(@"Key Exchange failed.");
    
    [self.moc deleteObject:self.friend];
    [self.moc save:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"keyExchangeFailed" object:nil];
    
    [QKeyExchange keyExchangeDidFinish];
}

// clean up whether it worked or not
+ (void)keyExchangeDidFinish {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"keyExchangeFinished" object:nil];
}

- (void)hasBytesAvailable {
    NSLog(@"NSStreamEventHasBytesAvailable");
    uint8_t buffer[MAX_MSG_LEN];
    NSInteger actuallyRead = [self.inputStream read:buffer maxLength:MAX_MSG_LEN];
    NSLog(@"Actually read %ld", (long)actuallyRead);
    
    NSData *readData = [[NSData alloc] initWithBytes:buffer length:actuallyRead];
    NSLog(@"raw data received: %@", [[NSString alloc] initWithData:readData encoding:NSUTF8StringEncoding]);
    [self.streamManager addData:[[NSString alloc] initWithData:readData encoding:NSUTF8StringEncoding]];

    NSLog(@"All data currently in manager: %@", self.streamManager.data);
    
    NSString *currentMessage = [self.streamManager nextJson];
    while (currentMessage) {
        NSLog(@"current message: %@", currentMessage);

        @try {
            SecQMessage *message = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:self.moc];
            [message initializeFromBlob:[currentMessage dataUsingEncoding:NSUTF8StringEncoding]];
            if (![message.messageType isEqualToString:@"ACK"]) {
                [self sendAckForMessage:message];
            } else {
                [self.retryer notifyAckReceived:(NSUInteger)message.messageId];
            }

            message.sender = self.friend;
            message.recipient = self.dqc;
            NSString *type = message.messageType;
            
            if ([message encrypted]) {
                NSLog(@"message is encrypted");
                [message decrypt];
                NSLog(@"message decrypt done");
            }
            
            if (!self.serverMode && !self.hasReceivedHello) {
                self.hasReceivedHello = YES;
                NSLog(@"Received HELLO");
                if (self.hasSpace) {
                    [self hasSpaceAvailable];
                }
                return;
            }
            if ([type isEqualToString:@"CERT"]) {
                [self handleCert:message];
            } else if ([type isEqualToString:@"CHLNG"]) {
                [self handleChlng:message];
            } else if ([type isEqualToString:@"CHLNGRESP"]) {
                [self handleChlngResp:message];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            NSLog(@"Error in hasBytes with data: %@", currentMessage);
        }
        @finally {
            if (self.hasSpace) {
                [self hasSpaceAvailable];
            }
            
        }
        currentMessage = [self.streamManager nextJson];
    }
    
    if ([self.streamManager hasData]) {
        self.hasBytes = YES;
        NSLog(@"message has not fully arrived yet");
        return;
    } else {
        self.hasBytes = NO;
    }
    
}

- (void)hasSpaceAvailable {
    NSLog(@"hasSpaceAvailable");
    self.hasSpace = YES;
    if (!self.serverMode && self.hasReceivedHello && !self.myCertSent) {
        SecQMessage *certMsg = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:self.moc];
        [certMsg initializeForRecipient:self.friend fromSender:self.dqc];
        Certificate *cert = self.dqc.certificate;
        NSData *certBlob = [cert toBase64Blob];
        certMsg.content = certBlob;
        certMsg.messageType = @"CERT";
        
        NSInteger status = [self sendMessage:certMsg];
        if (status < 0) {
            NSLog(@"Something went wrong sending the CERT. Status = %ul", status);
        }
        
        self.myCertSent = true;
        NSLog(@"Sent a CERT");
        self.hasSpace = NO;
        
    } else if (self.serverMode && !self.hasSentHello) {
        NSLog(@"self.serverMode && !self.hasSentHello");
        SecQMessage *helloMsg = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:self.moc];
        [helloMsg initializeForRecipient:self.friend fromSender:self.dqc];
        NSString *msgString = @"HELLO";
        helloMsg.content = [msgString dataUsingEncoding:NSUTF8StringEncoding];
        int status = [self sendMessage:helloMsg];
        if (status < 0) {
            NSLog(@"Something went wrong sending the HELLO. Status = %ul", status);
        } else {
            NSLog(@"Sent HELLO");
            self.hasSentHello = YES;
        }
        self.hasSpace = NO;
        
    } else if (self.serverMode && !self.myCertSent) {
        NSLog(@"self.serverMode && !self.myCertSent");
        SecQMessage *certMsg = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:self.moc];
        [certMsg initializeForRecipient:self.friend fromSender:self.dqc];
        Certificate *cert = self.dqc.certificate;
        NSData *certBlob = [cert toBase64Blob];
        certMsg.content = certBlob;
        certMsg.messageType = @"CERT";
        
        NSInteger status = [self sendMessage:certMsg];
        if (status < 0) {
            NSLog(@"Something went wrong sending the CERT. Status = %ul", status);
        }
        
        self.myCertSent = true;
        NSLog(@"Sent a CERT");
        self.hasSpace = NO;

    } else if (self.myCertSent && !self.myChallengeSent && self.chlngMsg) {
        NSLog(@"self.myCert && !self.myChallengeSent && self.chlngMsg");
        NSData *challengeBlob = [self.chlngMsg toBase64Blob];
        NSInteger status = [self sendMessage:self.chlngMsg];
        if (status < 0) {
            NSLog(@"Something went wrong sending the CHLNG. Status = %ul", status);
        } else {
            NSLog(@"Sent challenge: %@", [[NSString alloc] initWithData:challengeBlob encoding:NSUTF8StringEncoding]);
        }
        self.myChallengeSent = YES;
        self.hasSpace = NO;
        
        // raise an alert and ask user to confirm the code their friend received
        //[self handleConfirmChlng:self.myCode];
        
    } else if (self.theirCertConfirmed) {
        NSLog(@"Their cert has been confirmed");
        if (self.myCertConfirmed) {
            [self keyExchangeDidSucceed];
        } else {
            [self hasBytesAvailable];
            NSLog(@"My state still not confirmed");
        }
    } else {
        NSLog(@"Catch-all in hasSpace");
        if (self.hasBytes) {
            [self hasBytesAvailable];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"Button Index %d",buttonIndex);
    // send a negative CHLNGRESP
    if (buttonIndex == 0) {
        NSLog(@"Code did not match");
        self.theirCertConfirmed = NO;
        NSString *resp = @"NO";
        NSData *respData = [resp dataUsingEncoding:NSUTF8StringEncoding];
        SecQMessage *msg = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:self.moc];
        [msg initializeForRecipient:self.friend fromSender:self.dqc];
        msg.messageType = @"CHLNGRESP";
        msg.content = respData;
        NSInteger status = [self sendMessage:msg];
        if (status < 0) {
            NSLog(@"Something went wrong sending the CHLNGRESP. Status = %d", status);
        }
        
        // send a positive CHLNGRESP
    } else {
        NSLog(@"Code matched");
        self.theirCertConfirmed = YES;
        NSString *resp = @"YES";
        NSData *respData = [resp dataUsingEncoding:NSUTF8StringEncoding];
        SecQMessage *msg = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:self.moc];
        [msg initializeForRecipient:self.friend fromSender:self.dqc];
        msg.content = respData;
        msg.messageType = @"CHLNGRESP";
        [msg encrypt];
        NSInteger status =  [self sendMessage:msg];
        if (status < 0) {
            NSLog(@"Something went wrong sending the CHLNGRESP. Code = %d", status);
        } else {
            NSLog(@"Sent CHLNGRESP: %@", [[NSString alloc] initWithData:respData encoding:NSUTF8StringEncoding]);
        }
    }
}

- (void)userConfirmedCode:(BOOL)didMatch {
    if (didMatch) {
        NSLog(@"Code matched");
        self.theirCertConfirmed = YES;
        NSString *resp = @"YES";
        NSData *respData = [resp dataUsingEncoding:NSUTF8StringEncoding];
        SecQMessage *msg = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:self.moc];
        [msg initializeForRecipient:self.friend fromSender:self.dqc];
        msg.content = respData;
        msg.messageType = @"CHLNGRESP";
        [msg encrypt];
        NSInteger status = [self sendMessage:msg];
        if (status < 0) {
            NSLog(@"Something went wrong sending the CHLNGRESP. Code = %d", status);
        } else {
            NSLog(@"Sent CHLNGRESP: %@", [[NSString alloc] initWithData:respData encoding:NSUTF8StringEncoding]);
        }
        
        if (self.myCertConfirmed) {
            [self keyExchangeDidSucceed];
        }

    } else {
        NSLog(@"Code did not match");
        
        self.theirCertConfirmed = NO;
        [self keyExchangeDidFail];
//        NSString *resp = @"NO";
//        NSData *respData = [resp dataUsingEncoding:NSUTF8StringEncoding];
//        SecQMessage *msg = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:self.moc];
//        [msg initializeForRecipient:self.friend fromSender:self.dqc];
//        msg.messageType = @"CHLNGRESP";
//        msg.content = respData;
//        NSInteger status = [self sendMessage:msg];
//        if (status < 0) {
//            NSLog(@"Something went wrong sending the CHLNGRESP. Code = %ul", status);
//        }
    }
}

- (void)sendAckForMessage:(SecQMessage *)msg {
    SecQMessage *ack = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:self.moc];

    [ack initializeForRecipient:self.friend fromSender:self.dqc];
    ack.messageType = @"ACK";
    ack.content = [[NSString stringWithFormat:@"%@", msg.messageId] dataUsingEncoding:NSUTF8StringEncoding];
    
    // TODO probably should encrypt this
    [self sendMessage:ack];
    
    NSLog(@"ACK sent for message with id=%@", msg.messageId);
    [self sendMessage:ack];

}

- (NSInteger)sendMessage:(SecQMessage *)msg {
    NSLog(@"sending message: %@", [[NSString alloc] initWithData:[msg toBlob] encoding:NSUTF8StringEncoding]);
    NSInteger status = [QNetHelper sendMessage:[msg toBlob] onStream:self.outputStream];
    [self.retryer notifyMessageSent:msg];
    if (status < 0) {
        NSLog(@"problem sending message with id=%@ul. status=%d", msg.messageId, status);
    }
    return status;
}

@end


