//
//  QContacts.h
//  quis
//
//  Created by Will Skinner on 12/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QMessage;

@interface QContacts : NSManagedObject

@property (nonatomic, retain) NSData * certificateData;
@property (nonatomic, retain) NSString * certificateVersion;
@property (nonatomic, retain) NSString * emailAddress;
@property (nonatomic, retain) NSString * encryptionAlgorithm;
@property (nonatomic, retain) NSData * encryptionKeyPrivate;
@property (nonatomic, retain) NSData * encryptionKeyPublic;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSDate * mostRecentMessageDate;
@property (nonatomic, retain) NSString * signingAlgorithm;
@property (nonatomic, retain) NSData * signingKeyPrivate;
@property (nonatomic, retain) NSData * signingKeyPublic;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * messageIds;
@property (nonatomic, retain) NSNumber * uid;
@property (nonatomic, retain) NSSet *qMessages;
@end

@interface QContacts (CoreDataGeneratedAccessors)

- (void)addQMessagesObject:(QMessage *)value;
- (void)removeQMessagesObject:(QMessage *)value;
- (void)addQMessages:(NSSet *)values;
- (void)removeQMessages:(NSSet *)values;

@end
