//
//  NSDictionary+JsonDictionary.h
//  firstTry
//
//  Created by Will Skinner on 9/24/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JsonDictionary)
- (NSString *)sortRecursivelyAndSerialize;
@end
