//
//  QAddContactViewController.m
//  quis
//
//  Created by Admin on 06/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QAddContactViewController.h"
#import "QContactViewController.h"
#import "SecQContacts.h"

@interface QAddContactViewController ()<UITextFieldDelegate>

@property (strong, nonatomic) UIActivity *airDropActivity;

@property (strong,nonatomic) IBOutlet UILabel *receivedData;

@property (strong, nonatomic) IBOutlet UITextField *contactEmailAddess;
@property (strong, nonatomic) NSPredicate *emailQueryTemplate;
@property (strong, nonatomic) SecQContacts* SecDQC;

-(void) updateContacts;
@end

@implementation QAddContactViewController
@synthesize managedObjectContext = _managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    QAppDelegate* delegate= [[UIApplication sharedApplication] delegate];
    self.managedObjectContext = delegate.managedObjectContext;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *email = [defaults stringForKey:@"username_preference"];

    
    self.SecDQC = [QPersistenceHelpers queryEmailAddress:email];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark database

# pragma mark precomputed query templates

- (NSPredicate *)predicateTemplate {
    
    if (self.emailQueryTemplate == nil) {
        self.emailQueryTemplate = [NSPredicate predicateWithFormat:@"emailAddress == $EMAIL_ADDRESS"];
    }
    return self.emailQueryTemplate;
}

-(void) updateContacts {
    SecQContacts* fetchedObject = [QPersistenceHelpers queryEmailAddress:self.contactEmailAddess.text];
    if (fetchedObject) {
        NSLog(@"Contact already exists");
    } else {
        // notes put on perisistence
        NSLog(@"Contact not found adding with status nil");

        SecQContacts *SecQContact = (SecQContacts*)[NSEntityDescription insertNewObjectForEntityForName:@"SecQContacts"inManagedObjectContext:self.managedObjectContext];
        
        // TODO stop setting name to email address
        SecQContact.firstName = self.contactEmailAddess.text;
        SecQContact.emailAddress = self.contactEmailAddess.text;
        SecQContact.status = nil;
        
        NSError *error;
        
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Error saving: %@", [error localizedDescription]);
        }

    };
}

- (IBAction)Exchange:(UIButton *)sender {

    [self updateContacts]; // put in DB status nil
    [self writeSYNToFile]; // need to put the stuff we are going to send in file for activityVC
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/SYN.bzwx",
                          documentsDirectory];
    
    NSURL *url = [NSURL fileURLWithPath:fileName];
    [QEmailHelpers sendNSURLtoEmailAddress:url emailAddress:self.contactEmailAddess.text];

}

// create the pKey file
-(void) writeSYNToFile {
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/SYN.bzwx",
                          documentsDirectory];
    //create content - four lines of text
    NSString *content = @"test";//[self.SecDQC SYN];
    //NSString *content = self.publicKey.text;
    //save content to the documents directory
    
    //QContacts* DQC = nil; // will need to get from DB
    //[DQC.certificate writeToFile:fileName atomically:NO];
    
    [content writeToFile:fileName
              atomically:NO
                encoding:NSStringEncodingConversionAllowLossy
                   error:nil];
    
}

#pragma mark pk stuff

-(void) processFile:(NSURL *) url {
    NSLog(@"Processing file %@",url);
    
    //NSString *fileName = [NSString stringWithFormat:@"%@/pKey.bzwx",documentsDirectory];
    
    NSString *fileName = url.path;
    NSString *content = [[NSString alloc] initWithContentsOfFile:fileName
                                                    usedEncoding:nil
                                                           error:nil];
    
    NSLog(@"content %@",content);
    
    
    self.receivedData.text = content;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn:");
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}
- (IBAction)cancel:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
