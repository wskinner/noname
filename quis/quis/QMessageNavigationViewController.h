//
//  QMessageNavigationViewController.h
//  quis
//
//  Created by Admin on 05/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMessageNavigationViewController : UINavigationController
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end
