//
//  QContactComposeViewController.h
//  quis
//
//  Created by Admin on 08/12/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QResizingTextView.h"

@interface QContactComposeViewController : UIViewController<QResizingTextViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) NSString* targetEmail;


-(void)resignTextView;
@end