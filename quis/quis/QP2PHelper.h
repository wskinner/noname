//
//  QP2PHelper.h
//  quis
//
//  Created by Admin on 21/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//
//@import MultipeerConnectivity;
//#import "SecQMessage.h"
//#import <Foundation/Foundation.h>
//#import "QSessionLocal.h"
//
//@interface QP2PHelper : NSObject
//@property (strong, nonatomic) MCNearbyServiceBrowser* serviceBrowser;
//@property (strong, nonatomic) MCNearbyServiceAdvertiser* serviceAdvertiser;
//@property (strong, nonatomic) NSDictionary* discoveryInfo;
//
//
//@property (strong, nonatomic) MCPeerID* userEmailPeerID;
//
//@property (strong,nonatomic) NSString* userEmail;
//
//@property (strong,nonatomic) QSessionLocal* sessionLocal;
//
//-(void) disconnectSession;
//-(void) stopBrowsingForPeers;
//-(void) destroySession;
//
//@end
