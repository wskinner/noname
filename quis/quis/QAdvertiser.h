//
//  QAdvertiser.h
//  quis
//
//  Created by Admin on 26/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol QAdvertiserDelegate;

#pragma mark * QAdvertiser

@interface QAdvertiser : NSObject

- (id)initWithDomain:(NSString *)domain type:(NSString *)type name:(NSString *)name port:(NSUInteger)port;

@property (copy,   readonly ) NSString * domain;
@property (copy,   readonly ) NSString * type;
@property (copy,   readonly ) NSString * name;
@property (assign, readonly ) NSUInteger port;


@property (assign, readwrite) id<QAdvertiserDelegate> delegate;

- (void)start;
// Starts the registration process.  Does nothing if the registration is currently started.

- (void)stop;
// Stops a registration, deregistering the service from the network.  Does nothing if the
// registration is not started.

// properties that are set up once the registration is in place

@property (copy,   readonly ) NSString * registeredDomain;
@property (copy,   readonly ) NSString * registeredName;

@end

@protocol QAdvertiserDelegate <NSObject>

// All delegate methods are called on the main thread.

@optional

- (void)QAdvertiserWillRegister:(QAdvertiser *)sender;

- (void)QAdvertiserDidRegister:(QAdvertiser *)sender;

- (void)QAdvertiser:(QAdvertiser *)sender didNotRegister:(NSError *)error;

- (void)QAdvertiserDidStop:(QAdvertiser *)sender;

@end
