//
//  QNetConnectionViewController.h
//  quis
//
//  Created by Admin on 28/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QConnection.h"
#import "QNetHelper.h"
#import "QKeyExchangeViewDelegate.h"

#import <UIKit/UIKit.h>

@interface QNetConnectionViewController : UIViewController <QKeyExchangeViewDelegate>

@property (strong, nonatomic) NSString* resolvedHost;

@property (retain) id <KeyExchangeDelegate> keyXDelegate;
@property (strong, nonatomic) IBOutlet UILabel *theirCode;
@property (strong, nonatomic) IBOutlet UILabel *myCode;
@property (strong, nonatomic) IBOutlet UILabel *exchangeDoneLabel;


@property (nonatomic) BOOL serverMode;



- (IBAction)yesButton:(id)sender;
- (IBAction)noButton:(id)sender;

@end
