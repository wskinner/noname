//
//  QNetHelper.m
//  quis
//
//  Created by Admin on 28/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QNetHelper.h"

@interface QNetHelper() <NSNetServiceBrowserDelegate,QBrowserDelegate,QAdvertiserDelegate,QDNSServiceDelegate,NSStreamDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) QBrowser* browser;
@property (strong, nonatomic) QAdvertiser* advertiser;
@property (strong, nonatomic) QDNSService* service;
@property (strong, nonatomic) QServer* server;
@property (strong, nonatomic) NSString* emailAddress;

@property (assign, readwrite) CFSocketNativeHandle nativeSocketHandle;

@property BOOL hasShownAlert;


@end

static NSString * Quote(NSString * str)
// Returns a quoted form of the string.  This isn't intended to be super-clever,
// for example, it doesn't escape any single quotes in the string.
{
    return [NSString stringWithFormat:@"'%@'", (str == nil) ? @"" : str];
}
@implementation QNetHelper

// listen for alertConnection
-(id) init {
    self = [super init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.emailAddress = [defaults objectForKey:@"username_preference"];
    self.browser = [[QBrowser alloc] initWithDomain:nil type:@"_quis._tcp."];
    assert(self.browser != nil);
    
    self.browser.delegate = self;
    self.server = [[QServer alloc] init];

    self.advertiser = [[QAdvertiser alloc] initWithDomain:nil type:@"_quis._tcp." name:self.emailAddress port:55777];
    assert(self.advertiser != nil);
    
    self.advertiser.delegate = self;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startAll) name:@"startAll" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alertConnection:) name:@"alertConnection" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyExchangeFinished) name:@"keyExchangeFinished" object:nil];
    
    self.hasShownAlert = NO;
    
    return self;
}

#pragma mark advert

-(void) startAll {
    [self.server start];
    NSLog(@"Started server on port %lu",(unsigned long)self.server.port);
    [self.advertiser start];
    NSLog(@"Started advertising on port %lu",(unsigned long)self.server.port);
    [self.browser startBrowse];
}

-(void) stopAll {
    [self.advertiser stop];
    [self.server stop];
    [self.browser stop];
    if (self.service) [self.service stop];
    
}

#pragma mark alert incoming

-(void) alertConnection:(NSNotification*) note {
    
    // one key exchange at a time for the moment
    if ([self.server activeConnections] == 0 &&  !self.hasShownAlert) {
        self.nativeSocketHandle = [note.userInfo[@"CFSocketNativeHandle"] intValue];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contact Exchange" message:@"Do you want to connect?" delegate:self cancelButtonTitle:@"Decline" otherButtonTitles:@"Accept",nil];
        [alert show];
        self.hasShownAlert = YES;
    }
}

#pragma mark aletrview delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"Button Index %d",buttonIndex);

    if (buttonIndex == 0) {
        NSLog(@"Declining Invitation");
        
        (void) close(self.nativeSocketHandle);
        NSLog(@"Socket closed");
        
        [QKeyExchange keyExchangeDidFinish];
    } else {
        NSLog(@"Accepting Invitation");
        [self.server acceptConnection:self.nativeSocketHandle];
        QKeyExchange *keyXDelegate = [self.server getConnection].keyXDelegate;

        NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
        [info setObject:keyXDelegate forKey:@"keyExchangeDelegate"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"connectionAccepted" object:self userInfo:info];
    }
}

#pragma mark QBrowser delegate

- (void) browserWillBrowse:(QBrowser *)browser
{
    assert(browser == self.browser);
#pragma unused(browser)
    
    NSLog(@"will browse %@ / %@", Quote(self.browser.type), Quote(self.browser.domain));
}

- (void) browserDidStopBrowse:(QBrowser *)browser {
    assert(browser == self.browser);
#pragma unused(browser)
    NSLog(@"browserDidStopBrowse stopped");
}

- (void) browser:(QBrowser *)browser didAddService:(QDNSService *)service moreComing:(BOOL)moreComing
{
    assert(browser == self.browser);
#pragma unused(browser)
    
    NSLog(@"   add service %@ / %@ / %@%s", Quote(service.name), Quote(service.type), Quote(service.domain), moreComing ? " ..." : "");
    
    if (![service.name isEqualToString:self.emailAddress]) {
        self.service = [[QDNSService alloc] initWithDomain:service.domain type:service.type name:service.name];
        self.service.delegate = self;
        [self.service startResolve];
    }
}

- (void) browser:(QBrowser *)browser didRemoveService:(QDNSService *)service moreComing:(BOOL)moreComing {
    assert(browser == self.browser);
#pragma unused(browser)
    // NB browser allocates the QDNSService - we should put in array.
    NSLog(@"remove service %@/ %@ / %@%s", Quote(service.name), Quote(service.type), Quote(service.domain), moreComing ? " ..." : "");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"lostPeer" object:self userInfo:@{@"name" : service.name}];
}

- (void) browser:(QBrowser *)browser didNotBrowse:(NSError *)error{
    assert(browser == self.browser);
#pragma unused(browser)
    assert(error != nil);
    
    NSLog(@"did not browse (%@ / %zd)", [error domain], (ssize_t) [error code]);
    //    [self quit];
}

#pragma mark QAdvertiser delegates

- (void)QAdvertiserWillRegister:(QAdvertiser *)sender
{
    assert(sender == self.advertiser);
#pragma unused(sender)
    
    NSLog(@"will register %@ / %@ / %@", Quote(self.advertiser.name), Quote(self.advertiser.type), Quote(self.advertiser.domain));
}

- (void)QAdvertiserDidRegister:(QAdvertiser *)sender
{
    assert(sender == self.advertiser);
#pragma unused(sender)
    
    NSLog(@"registered as %@ / %@ / %@", Quote(self.advertiser.registeredName), Quote(self.advertiser.type), Quote(self.advertiser.registeredDomain));
}
- (void)QAdvertiser:(QAdvertiser *)sender didNotRegister:(NSError *)error
{
    assert(sender == self.advertiser);
#pragma unused(sender)
    assert(error != nil);
    
    NSLog(@"did not register (%@ / %zd)", [error domain], (ssize_t) [error code]);
    //[self quit];
}

- (void)QAdvertiserDidStop:(QAdvertiser *)sender
{
    assert(sender == self.advertiser);
#pragma unused(sender)
    
    NSLog(@"QAdvertiserDidStop stopped");
}

#pragma mark QDNS delegates

- (void)QDNSServiceWillResolve:(QDNSService *)service
{
    //assert(service == self.service);
#pragma unused(service)
    NSLog(@"will resolve %@ / %@ / %@", Quote(self.service.name), Quote(self.service.type), Quote(self.service.domain));
}

- (void)QDNSServiceDidResolveAddress:(QDNSService *)service
{
    //assert(service == self.service);
#pragma unused(service)
    NSLog(@"did resolve to %@:%zu", service.resolvedHost, (size_t) service.resolvedPort);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"foundPeer" object:self userInfo:@{@"name" : service.name, @"domain":service.domain,@"resolvedHost":service.resolvedHost}];
    
}

- (void)QDNSService:(QDNSService *)service didNotResolve:(NSError *)error;
{
    NSLog(@"Assertion service %@ self.service %@" ,service, self.service);
#pragma unused(service)
    NSLog(@"did not resolve (%@ / %zd)", [error domain], (ssize_t) [error code]);
    //    [self quit];
}

- (void)QDNSServiceDidStop:(QDNSService *)service;
{
    //assert(service == self.service);
#pragma unused(service)
    NSLog(@"QDNSServiceDidStop stopped");
}

- (void)keyExchangeFinished {
    self.hasShownAlert = NO;
}


+ (NSUInteger)sendMessage:(NSData *)message onStream:(NSOutputStream *)stream {
    if (message.length > MAX_MSG_LEN) {
        return -1;
    }
    uint8_t msg_buf[MAX_MSG_LEN];
    memcpy(msg_buf, [message bytes], message.length);
    NSInteger written = [stream write:msg_buf maxLength:message.length];
    if (written == message.length) {
        return 0;
    } else {
        return -2;
    }
}


@end
