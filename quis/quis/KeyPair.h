//
//  SecKeyWrapper.h
//  firstTry
//
//  Created by Will Skinner on 8/28/13.
//  Copyright (c) 2013 Admin. All rights reserved.

#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
#import <sodium.h>
#import "PrivateKey.h"
#import "PublicKey.h"
#import "Message.h"
#import <CoreData/CoreData.h>

__attribute__((deprecated))
@interface KeyPair : NSObject {
    PrivateKey *privateKey;
    PublicKey *publicKey;
}
@property (strong, atomic) PrivateKey *privateKey;
@property (strong, atomic) PublicKey *publicKey;

//- (id)initForContact:(Contacts *)contact inContext:(NSManagedObjectContext *)context;
//- (id)initForIdentity:(Identities *)identity inContext:(NSManagedObjectContext *)context;
- (void)generateKeyPair;
- (NSData *)computeKeyFingerprint;

- (NSData *)encrypt:(NSData *)data forKey:(KeyPair *)kp nonce:(NSData *)nonce __attribute__((deprecated));
- (NSData *)decrypt:(NSData *)data fromKey:(KeyPair *)kp nonce:(NSData *)nonce __attribute__((deprecated));

+ (unsigned char *)nextNonce;
+ (NSUInteger)encryptionKeyPublicLength;
+ (NSUInteger)encryptionKeyPrivateLength;


@end