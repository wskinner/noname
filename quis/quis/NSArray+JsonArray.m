//
//  NSArray+JsonArray.m
//  firstTry
//
//  Created by Will Skinner on 9/24/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "NSArray+JsonArray.h"
#import "NSDictionary+JsonDictionary.h"

@implementation NSArray (JsonArray)
- (NSString *)sortRecursivelyAndSerialize {
    NSArray *sorted = [self sortedArrayUsingSelector:@selector(compare:)];
    NSMutableArray *sortedValues = [[NSMutableArray alloc] init];
    for (NSObject *el in sorted) {
        if ([el isKindOfClass:[NSDictionary class]]) {
            [sortedValues addObject:[(NSDictionary *)el sortRecursivelyAndSerialize]];
        } else if ([el isKindOfClass:[NSArray class]]) {
            [sortedValues addObject:[(NSArray *)el sortRecursivelyAndSerialize]];
        } else {
            [sortedValues addObject:el];
        }
    }
    NSString *result = [[@"[" stringByAppendingString:[sortedValues componentsJoinedByString:@","]] stringByAppendingString:@"]"];
    return result;
}

@end
