//
//  SecKeyWrapper.m
//  firstTry
//
//  Created by Will Skinner on 8/28/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "KeyPair.h"
#import <Security/Security.h>
#import <sodium.h>

@implementation KeyPair
@synthesize publicKey, privateKey;

- (id) init {
    self = [super init];
    if (self) {
        [self generateKeyPair];
    }
    return self;
}

- (void)generateKeyPair {
    publicKey = [[PublicKey alloc] init];
    privateKey = [[PrivateKey alloc] init];
    
    crypto_box_keypair([publicKey public_key], [privateKey private_key]);
    crypto_sign_keypair([publicKey public_signing_key], [privateKey private_signing_key]);
}

- (NSData *)computeKeyFingerprint {
    unsigned long long mlen = crypto_box_publickeybytes();
    unsigned char hash[crypto_hash_BYTES];
    crypto_hash(hash, [publicKey public_key], mlen);
    
    NSData *result = [NSData dataWithBytes:(const void *)hash length:crypto_hash_BYTES];
    return result;
}


+ (unsigned char *)nextNonce {
    unsigned char *nonce = malloc(crypto_box_NONCEBYTES*sizeof(unsigned char));
    randombytes(nonce, crypto_box_NONCEBYTES);
    return nonce;
}

+ (NSUInteger)encryptionKeyPublicLength {
    NSUInteger result = (unsigned int )crypto_box_publickeybytes();
    return result;
}

+ (NSUInteger)encryptionKeyPrivateLength {
    NSUInteger result = (unsigned int )crypto_box_secretkeybytes();
    return result;
}


- (NSData *)encrypt:(NSData *)data forKey:(KeyPair *)kp nonce:(NSData *)nonce {
    NSUInteger size = data.length;
    unsigned int mlen = size + crypto_box_zerobytes();
    unsigned char *plain = malloc(mlen * sizeof(unsigned char));
    unsigned char *cipher = malloc(mlen * sizeof(unsigned char));
    memset(plain, 0, crypto_box_zerobytes());
    
    unsigned char *message_start = &plain[crypto_box_zerobytes()];
    memcpy(message_start, (unsigned char *)[data bytes], size);
    
    unsigned char *nonce_bytes = (unsigned char *)[nonce bytes];
    
    NSUInteger pubKeyLength = [KeyPair encryptionKeyPublicLength];
    unsigned char *recipientPub = malloc(sizeof(unsigned char) * pubKeyLength);
    memcpy(recipientPub, self.publicKey.public_key, pubKeyLength);
    
    NSUInteger privKeyLength = [KeyPair encryptionKeyPrivateLength];
    unsigned char *senderPriv = malloc(sizeof(unsigned char) * privKeyLength);
    memcpy(senderPriv, kp.privateKey.private_key, privKeyLength);
    
    int status = crypto_box(cipher,plain,mlen,nonce_bytes,recipientPub,senderPriv);
    if (status != 0) {
        return nil;
    } else {
        NSData *result = [[NSData alloc] initWithBytes:cipher length:mlen];
        return result;
    }
}

- (NSData *)decrypt:(NSData *)data fromKey:(KeyPair *)kp nonce:(NSData *)nonce {
    NSUInteger size = data.length;
    unsigned char *plain = malloc(size * sizeof(unsigned char));
    
    NSUInteger pubKeyLength = [KeyPair encryptionKeyPublicLength];
    unsigned char *senderPub = malloc(sizeof(unsigned char) * pubKeyLength);
    memcpy(senderPub, kp.publicKey.public_key, pubKeyLength);
    
    unsigned char *nonce_bytes = malloc(sizeof(unsigned char) * crypto_box_noncebytes());
    memcpy(nonce_bytes, (const void *)[nonce bytes], crypto_box_noncebytes());
    
    NSUInteger privKeyLength = [KeyPair encryptionKeyPrivateLength];
    unsigned char *recipientPriv = malloc(sizeof(unsigned char) * privKeyLength);
    memcpy(recipientPriv, self.privateKey.private_key, privKeyLength);
    
    int status = crypto_box_open(plain,(unsigned char*)[data bytes],size,nonce_bytes,senderPub,recipientPriv);
    if (status != 0) {
        NSLog(@"data: %@", [data base64EncodedString]);
        NSLog(@"size: %lu", (unsigned long)size);
        NSLog(@"nonce_bytes: %@", [[[NSData alloc] initWithBytes:nonce_bytes length:crypto_box_noncebytes()] base64EncodedString]);
        NSLog(@"senderPub: %@", [[[NSData alloc] initWithBytes:senderPub length:pubKeyLength] base64EncodedString]);
        NSLog(@"recipientPriv: %@", [[[NSData alloc] initWithBytes:recipientPriv length:privKeyLength] base64EncodedString]);
        NSLog(@"------------------------------------------------");
        return nil;
    } else {
//        NSLog(@"data: %@", [data base64EncodedString]);
//        NSLog(@"size: %lu", (unsigned long)size);
//        NSLog(@"nonce_bytes: %@", [[[NSData alloc] initWithBytes:nonce_bytes length:crypto_box_noncebytes()] base64EncodedString]);
//        NSLog(@"senderPub: %@", [[[NSData alloc] initWithBytes:senderPub length:pubKeyLength] base64EncodedString]);
//        NSLog(@"recipientPriv: %@", [[[NSData alloc] initWithBytes:recipientPriv length:privKeyLength] base64EncodedString]);
//        NSLog(@"/////////////////////////////////////////////////");
        unsigned char *message_start = &plain[crypto_box_boxzerobytes()];
        NSData *result = [NSData dataWithBytes:(const void *)&message_start[crypto_box_boxzerobytes()] length:sizeof(unsigned char)*size - crypto_box_zerobytes()];
        
        free(plain);
        free(senderPub);
        free(recipientPriv);
        free(nonce_bytes);
        
        return result;
    }
}


@end
