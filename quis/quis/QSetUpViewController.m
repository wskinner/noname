//
//  QSetUpViewController.m
//  quis
//
//  Created by Admin on 19/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QSetUpViewController.h"
#import "SecQContacts.h"

@interface QSetUpViewController ()
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;

@end

@implementation QSetUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
                // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.username) {
        self.username.keyboardType = UIKeyboardTypeEmailAddress;
        self.username.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.username.autocorrectionType = UITextAutocorrectionTypeNo;
        NSLog(@"Turned off autocap/autocorrect");
    } else {
        NSLog(@"username not yet initialized");
    }

	// Do any additional setup after loading the view.
}

- (IBAction)submit:(UIButton *)sender {
    QAppDelegate* delegate= [[UIApplication sharedApplication] delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObjects:@[@"",@"",@NO,@NO,@NO] forKeys:@[@"username_preference",@"password_preference",@"look_in_email",@"use_contacts",@"exchange_trusted"]];

    [defaults registerDefaults:appDefaults];
    [defaults synchronize];
    

    // show the set up view
    [defaults setValue:self.username.text forKey:@"username_preference"];
    [defaults setValue:self.password.text forKey:@"password_preference"];
    
    // This is the first launch ever
    // we need to make the DQC
    NSLog(@"DQC Query");
    
    if ([QPersistenceHelpers queryEmailAddress:[defaults stringForKey:@"username_preference"]]) {
        
        // we found the DQC
        // if the DQC does not match setting email tell user to delete app
    }
    NSLog(@"Creating DQG");
    SecQContacts *DQC = (SecQContacts *) [NSEntityDescription insertNewObjectForEntityForName:@"SecQContacts" inManagedObjectContext:delegate.managedObjectContext];
    
    [DQC generateCertificate];
    [DQC generateKeyPairs];
    
    // TODO stop setting name to the email address
    DQC.emailAddress = self.username.text;
    DQC.firstName = self.username.text;
    DQC.status = @"DQC";
    
    // also make the file with the syn stuff just once when init DQC
    NSError *error;
    if (![delegate.managedObjectContext save:&error]) {
        NSLog(@"Error saving: %@", [error localizedDescription]);
    }
    
    [defaults setBool:YES forKey:@"HasLaunchedOnce"];
    
     NSLog(@"App defaults %@",[defaults stringForKey:@"username_preference"]);
    [defaults synchronize];
    delegate.session = [QEmailHelpers session];
    delegate.qNetHelper = [[QNetHelper alloc] init];
    [self performSegueWithIdentifier:@"QSetUpDone" sender:self];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
