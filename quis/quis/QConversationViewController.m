//
//  QConversationViewController.m
//  quis
//
//  Created by Admin on 07/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//
#import "QMessageViewController.h"
#import "QConversationViewController.h"
#import "QConversationListViewController.h"
#import <MailCore/MailCore.h>

@interface QConversationViewController ()

@property (strong,nonatomic) QResizingTextView *textView;
@property (strong,nonatomic) UIActivityIndicatorView *refreshSpinner;
@property (strong,nonatomic) QConversationListViewController *childView;

@end

@implementation QConversationViewController
@synthesize managedObjectContext = _managedObjectContext;
@synthesize fetchedResultsController = _fetchedResultsController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    QMessageViewController* parentView = (QMessageViewController*) self.parentViewController;
    self.managedObjectContext = parentView.managedObjectContext;
    
    [self.tabBarController.tabBar setHidden:YES];
    
    //self.view.backgroundColor =[UIColor colorWithRed:1.0f/255.0f green:100.0f/255.0f blue:0.0f/255.0f alpha:1];
    
	if(self){
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillShow:)
													 name:UIKeyboardWillShowNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillHide:)
													 name:UIKeyboardWillHideNotification
												   object:nil];
	}
    NSLog(@"self.containerView.frame %f, %f",self.containerView.frame.size.height,self.containerView.frame.size.width);
    //self.containerView.backgroundColor =[UIColor colorWithRed:219.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1];
    
    NSLog(@"HPGrowingTextView alloc");
    self.textView = [[QResizingTextView alloc] initWithFrame:CGRectMake(6, 3, 240, 40)];
    //self.textView.backgroundColor = [UIColor colorWithRed:219.0f/255.0f green:226.0f/255.0f blue:237.0f/255.0f alpha:1];
    
    self.textView.isScrollable = NO;
    self.textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
	self.textView.minNumberOfLines = 1;
	//self.textView.maxNumberOfLines = 9999;
    // you can also set the maximum height in points with maxHeight
    // maxHeight is whole height less nav bar and keyboard
    self.textView.maxHeight = 800.0f;
	self.textView.returnKeyType = UIReturnKeyGo; //just as an example
	self.textView.font = [UIFont systemFontOfSize:15.0f];
	self.textView.delegate = self;
    self.textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    self.textView.backgroundColor = [UIColor whiteColor];
    self.textView.placeholder = @"quis";
    self.childView = [self.childViewControllers objectAtIndex:0];
    
    
    [self.containerView addSubview:self.textView];
    
    
	UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn.frame = CGRectMake(self.containerView.frame.size.width - 69, 8, 63, 27);
    doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
	[doneBtn setTitle:@"Send" forState:UIControlStateNormal];
    [doneBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    //[doneBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    //doneBtn.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    //doneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    
    //[doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[doneBtn addTarget:self action:@selector(resignTextView) forControlEvents:UIControlEventTouchUpInside];
    
    //[doneBtn setBackgroundImage:sendBtnBackground forState:UIControlStateNormal];
    //[doneBtn setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];
    
	
    [self.containerView addSubview:doneBtn];
    self.containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    NSLog(@"Nav bar height %f y origin %f",self.navigationController.navigationBar.frame.size.height,self.navigationController.navigationBar.frame.origin.y);
    
    
    NSLog(@"self.containerView.frame %f, origin %f %f",self.containerView.frame.size.height,self.tableViewContainer.frame.origin.x,self.containerView.frame.origin.y);
    
    NSLog(@"childView.conversation.contentSize.height %f",self.childView.conversation.contentSize.height);
    // scroll down to the last row
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([self.conversationItems count]-1) inSection:0];
//    [self.childView.conversation scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    
    NSLog(@"tableViewContainer %f, origin %f %f",self.tableViewContainer.frame.size.height,self.tableViewContainer.frame.origin.x,self.tableViewContainer.frame.origin.y);
    NSLog(@"self.view.bounds.size.height %f",self.view.bounds.size.height);
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeLabelValue:) name:@"sendData" object:nil];
    
    self.refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.refreshSpinner.color = [UIColor blueColor];
    self.refreshSpinner.hidesWhenStopped = YES;
    
}
-(void)changeLabelValue:(NSNotification *)iRecognizer {
    
    NSString *myText = iRecognizer.object;
    
    NSLog(@"Received Text %@",myText);
    //Here You can change the value of label
    self.navigationItem.title = myText;
    
    //UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    //UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    
    
    if ([myText isEqualToString:@"Refresh"]) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.refreshSpinner];
        [self.refreshSpinner startAnimating];
    }
    
    if ([myText isEqualToString:@"Wheel"]) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:nil action:nil];
    }
    if ([myText isEqualToString:@""]) {
        self.navigationItem.rightBarButtonItem = nil;
    }    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:nil action:nil];
}


- (void)resignTextView {
    
	[self.textView resignFirstResponder];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/tmp_bz.bzwx",
                          documentsDirectory];
    
    // CRYPTO
    //save content to the document directory
    [self.textView.internalTextView.text writeToFile:fileName
                                          atomically:NO
                                            encoding:NSStringEncodingConversionAllowLossy
                                               error:nil];
    NSURL *qMessageURL = [[NSURL alloc] initFileURLWithPath:fileName];
    [QEmailHelpers sendNSURLtoEmailAddress:qMessageURL emailAddress:self.sender];

}

//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    NSLog(@"keyboardWillShow");
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    
    
    
    
	// get a rect for the textView frame
	CGRect containerFrame = self.containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	
	// set views with new info
	self.containerView.frame = containerFrame;
    
    // have to adjust other container
    
    float navBarSize = self.navigationController.navigationBar.frame.size.height+self.navigationController.navigationBar.frame.origin.y;
    
    // maxHeight is whole height less nav bar and keyboard
    self.textView.maxHeight= keyboardBounds.origin.y - navBarSize - 3.0*5.0f; // 5 is content inset top and bottom
    
//
//    float newHeight = self.view.frame.size.height - navBarSize - (keyboardBounds.size.height + containerFrame.size.height);
//    
//    NSLog(@"New height %f",newHeight);
//    
//    CGRect tableViewContainerFrame = self.tableViewContainer.frame;
//    
//    
//    tableViewContainerFrame.size.height = newHeight;
//    self.tableViewContainer.frame = tableViewContainerFrame;
//    
//    // scroll down for the user if the height is less than the content
//    if (newHeight < self.childView.conversation.contentSize.height) {
//        CGFloat height = self.childView.conversation.contentSize.height - newHeight;
//        [self.childView.conversation setContentOffset:CGPointMake(0, height) animated:YES];
//    }
	// commit animations
	[UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSLog(@"keyboardWillHide");
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
	// get a rect for the textView frame
	CGRect containerFrame = self.containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
	
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
	self.containerView.frame = containerFrame;
    
    // have to adjust other container
    
//    float navBarSize = self.navigationController.navigationBar.frame.size.height+self.navigationController.navigationBar.frame.origin.y;
//    
//    float newHeight = self.view.frame.size.height - navBarSize - containerFrame.size.height;
//    
//    NSLog(@"New height %f",newHeight);
//    
//    CGRect tableViewContainerFrame = self.tableViewContainer.frame;
//    
//    tableViewContainerFrame.size.height = newHeight;
//    self.tableViewContainer.frame = tableViewContainerFrame;
//    
//    // scroll down for the user if the height is less than the content
//    if (newHeight < self.childView.conversation.contentSize.height) {
//        CGFloat height = self.childView.conversation.contentSize.height - newHeight;
//        [self.childView.conversation setContentOffset:CGPointMake(0, height) animated:YES];
//    }
//    
//	
	// commit animations
	[UIView commitAnimations];
}

- (void)growingTextView:(QResizingTextView *)growingTextView willChangeHeight:(float)height
{
    NSLog(@"growingTextView %f",growingTextView.frame.size.height);
    float diff = (growingTextView.frame.size.height - height);
    NSLog(@"diff %f",diff);
	CGRect r = self.containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	self.containerView.frame = r;
    NSLog(@"self.containerView.frame height %f, origin %f",self.containerView.frame.size.height,self.containerView.frame.origin.y);
    
    float navBarSize = self.navigationController.navigationBar.frame.size.height+self.navigationController.navigationBar.frame.origin.y;
    
    float newHeight = r.origin.y - navBarSize;
    
    // if we set height <0 table will never reappear
    if (newHeight < 0) newHeight = 0;
    NSLog(@"New height %f",newHeight);
//    CGRect tableViewContainerFrame = self.tableViewContainer.frame;
//    
//    // the below will move top up
//    
//    tableViewContainerFrame.size.height = newHeight;
//    self.tableViewContainer.frame = tableViewContainerFrame;
//    
//    // scroll down for the user if the height is less than the content
//    if (newHeight < self.childView.conversation.contentSize.height) {
//        CGFloat height = self.childView.conversation.contentSize.height - newHeight;
//        [self.childView.conversation setContentOffset:CGPointMake(0, height) animated:YES];
//    }
//    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    QMessageViewController* parentView = (QMessageViewController*)  self.parentViewController;
    self.managedObjectContext =  parentView.managedObjectContext;
    
    [segue.destinationViewController setConversationItems:self.conversationItems];
    [segue.destinationViewController setManagedObjectContext:self.managedObjectContext];
    [segue.destinationViewController setSender:self.sender];
}
@end
