//
//  Message.m
//  firstTry
//
//  Created by Will Skinner on 8/31/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "Message.h"
#import "KeyPair.h"

@implementation Message
@synthesize text, recipient, subject, sender, nonce, isEncrypted;

- (id)initForRecipient:(KeyPair *)forRecipient text:(NSData *)text withSubject:(NSString *)subject fromSender:(KeyPair *)sender {
    self = [super init];
    if (self) {
        [self setRecipient:forRecipient];
        [self setText:text];
        [self setSubject:subject];
        [self setSender:sender];
        [self setNonce:[KeyPair nextNonce]];
        [self setIsEncrypted:false];
    }
    return self;
}

- (void)encrypt {
    NSUInteger size = [text length] / sizeof(unsigned char);
    unsigned char *plain = malloc((size + crypto_box_zerobytes()) * sizeof(unsigned char));
    unsigned char *cipher = malloc((size + crypto_box_zerobytes()) * sizeof(unsigned char));
    memset(plain, 0, crypto_box_zerobytes());
    unsigned char *message_start = &plain[crypto_box_zerobytes()];
    memcpy(message_start, (unsigned char *)[text bytes], size);
    
    int status = crypto_box(cipher,plain,size + crypto_box_zerobytes(),nonce,recipient.publicKey.public_key,sender.privateKey.private_key);
    if (status != 0) {
        return;
    } else {
        [self setText:[NSData dataWithBytes:(const void *)cipher length:sizeof(unsigned char)*(size + crypto_box_zerobytes())]];
        [self setIsEncrypted:true];
    }
    
}

- (void) decrypt{
    NSUInteger size = [text length] / sizeof(unsigned char);
    unsigned char *plain = malloc(size * sizeof(unsigned char));
    
    int status = crypto_box_open(plain,(unsigned char*)[[self text] bytes],[text length],[self nonce],sender.publicKey.public_key,recipient.privateKey.private_key);
    if (status != 0) {
        return;
    } else {
        [self setIsEncrypted:false];
    }
    unsigned char *message_start = &plain[crypto_box_boxzerobytes()];
    [self setText:[NSData dataWithBytes:(const void *)&message_start[crypto_box_boxzerobytes()] length:sizeof(unsigned char)*size - crypto_box_zerobytes()]];
}

@end