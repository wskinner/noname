//
//  QDNSService.h
//  quis
//
//  Created by Admin on 26/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol QDNSServiceDelegate;

#pragma mark * QDNSService

@interface QDNSService : NSObject <NSCopying>

- (id)initWithDomain:(NSString *)domain type:(NSString *)type name:(NSString *)name;


@property (copy,   readonly ) NSString * domain;
@property (copy,   readonly ) NSString * type;
@property (copy,   readonly ) NSString * name;

// properties that you can change any time

@property (assign, readwrite) id<QDNSServiceDelegate> delegate;

- (void)startResolve;
// Starts a resolve.  Starting a resolve on a service that is currently resolving
// is a no-op.  If the resolve does not complete within 30 seconds, it will fail
// with a time out.

- (void)stop;
// Stops a resolve.  Stopping a resolve on a service that is not resolving is a no-op.

// properties that are set up once the resolve completes

@property (copy,   readonly ) NSString * resolvedHost;
@property (assign, readonly ) NSUInteger resolvedPort;

@end

@protocol QDNSServiceDelegate <NSObject>

// All delegate methods are called on the main thread.

@optional

- (void)QDNSServiceWillResolve:(QDNSService *)service;

- (void)QDNSServiceDidResolveAddress:(QDNSService *)service;

- (void)QDNSService:(QDNSService *)service didNotResolve:(NSError *)error;
- (void)QDNSServiceDidStop:(QDNSService *)service;


@end

