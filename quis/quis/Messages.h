//
//  Messages.h
//  firstTry
//
//  Created by Will Skinner on 9/4/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Contacts, Identities;

@interface Messages : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSData * nonce;
@property (nonatomic, retain) NSData * text;
@property (nonatomic, retain) Identities *identity;
@property (nonatomic, retain) Contacts *recipient;

@end
