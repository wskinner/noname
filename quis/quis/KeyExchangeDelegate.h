//
//  KeyExchangeDelegate.h
//  quis
//
//  Created by Will Skinner on 11/17/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KeyExchangeDelegate <NSObject>

@required
- (void)hasBytesAvailable;

@required
- (void)hasSpaceAvailable;

@required
- (void)userConfirmedCode:(BOOL)didMatch;
@end
