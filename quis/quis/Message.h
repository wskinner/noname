//
//  Message.h
//  firstTry
//
//  Created by Will Skinner on 8/31/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sodium.h>
#import "PublicKey.h"
#import "PrivateKey.h"

@class KeyPair;

@interface Message : NSObject {
    NSData *text;
    NSString *subject;
    KeyPair *recipient;
    KeyPair *sender;
    unsigned char *nonce;
    bool isEncrypted;
}
@property NSData *text;
@property NSString *subject;
@property KeyPair *recipient;
@property KeyPair *sender;
@property unsigned char *nonce;
@property bool isEncrypted;

- (id)initForRecipient:(KeyPair *)forRecipient text:(NSData *)text withSubject:(NSString *)subject fromSender:(KeyPair *)sender;
- (void)encrypt;
- (void)decrypt;

@end
