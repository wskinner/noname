//
//  SecQContacts.h
//  quis
//
//  Created by Will Skinner on 10/12/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "QContacts.h"
#import <UIKit/UIKit.h>
#import "QKeyPair.h"
#import "Certificate.h"
#import "NSDictionary+GetValue.h"

//itWouldnt:be:SoBadIf:IKnewWhatIwas:Doing
@interface SecQContacts : QContacts
@property (strong, nonatomic) QKeyPair *keyPair;
@property (strong, nonatomic) Certificate *certificate;

- (void)generateKeyPairs;
- (void)generateCertificate;
- (void)describeKeys;
- (void)populateInfoFromCert:(Certificate *)cert;

- (NSUInteger)nextMessageIdForRecipient:(SecQContacts *)recipient;

- (NSString *)SYN;
@end




