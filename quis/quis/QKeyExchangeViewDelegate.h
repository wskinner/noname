//
//  QCodeDisplayDelegate.h
//  quis
//
//  Created by Will Skinner on 11/24/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol QKeyExchangeViewDelegate <NSObject>

@required
- (void)putMyCode:(NSString *)code;
@required
- (void)putTheirCode:(NSString *)code;
@required
- (void)keyXDone;

@required
- (NSString *)getTheirCode;
@required
- (NSString *)getMyCode;


@end
