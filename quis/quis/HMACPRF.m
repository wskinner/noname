//
//  SHA512PseudoRandomFunction.m
//  firstTry
//
//  Created by Will Skinner on 9/17/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "HMACPRF.h"
#import <sodium.h>

@implementation HMACPRF

// First hash the password to produce a uniform length
- (NSData *)computeHash:(NSData *)data withKey:(NSData *)key {
    unsigned char *khash = malloc(crypto_hash_BYTES);
    NSUInteger klen = [key length];
    crypto_hash(khash, [key bytes], klen);
    
    unsigned char *dhash = malloc(crypto_auth_BYTES);
    crypto_auth(dhash, [data bytes], [data length], khash);
    
    free(khash);
    free(dhash);
    NSData *result = [NSData dataWithBytes:dhash length:crypto_auth_BYTES];
    return  result;
}
@end
