//
//  QMessage.m
//  quis
//
//  Created by Will Skinner on 11/11/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QMessage.h"
#import "QContacts.h"


@implementation QMessage

@dynamic content;
@dynamic date;
@dynamic isEncrypted;
@dynamic messageId;
@dynamic messageType;
@dynamic nonce;
@dynamic recipient;
@dynamic sender;

@end
