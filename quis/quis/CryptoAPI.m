//
//  CryptoAPI.m
//  firstTry
//
//  Created by Admin on 14/09/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "CryptoAPI.h"
#import "Base64.h"

@implementation CryptoAPI

+ (NSString *)hash:(NSData *)data {
    unsigned long long len = [data length];
    unsigned char *msg = malloc(len);
    memcpy(msg, data.bytes, len);
    unsigned char *h = malloc(crypto_hash_BYTES * sizeof(unsigned char));
    crypto_hash(h,msg,len);
    NSData *hashed = [[NSData alloc] initWithBytes:h length:crypto_hash_BYTES];
    
    NSString *hashString = [hashed base64EncodedString];
    free(msg);
    free(h);
    return hashString;
}

+ (NSData *)sign:(NSData *)data withKeyPair:(QKeyPair *)keyPair {
    return [keyPair signData:data];
}

+ (NSData *)verifySignedData:(NSData *)data withSignerKeyPair:(QKeyPair *)keyPair {
    return [keyPair verifySignedData:data];
}

@end
