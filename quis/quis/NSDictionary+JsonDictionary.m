//
//  NSDictionary+JsonDictionary.m
//  firstTry
//
//  Created by Will Skinner on 9/24/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "NSDictionary+JsonDictionary.h"

@implementation NSDictionary (JsonDictionary)
- (NSString *)sortRecursivelyAndSerialize {
    NSArray *sortedKeys = [[self allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSMutableArray *sortedValues = [[NSMutableArray alloc] init];
    for (NSString *key in sortedKeys) {
        if ([[self objectForKey:key] isKindOfClass:[NSDictionary class]] ||
            [[self objectForKey:key] isKindOfClass:[NSArray class]]) {
            [sortedValues addObject:[[key stringByAppendingString:@":"]
                                     stringByAppendingString:[[self objectForKey:key] sortRecursivelyAndSerialize]]];
        } else {
            [sortedValues addObject:[[key stringByAppendingString:@":"]
                                     stringByAppendingString:[self objectForKey:key]]];
        }
    }
    NSString *result = [[@"{" stringByAppendingString:[sortedValues componentsJoinedByString:@","]] stringByAppendingString:@"}"];
    return result;
}
@end
