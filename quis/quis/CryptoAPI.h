//
//  CryptoAPI.h
//  firstTry
//
//  Created by Admin on 14/09/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyPair.h"
#import <sodium.h>

@interface CryptoAPI : NSObject

// comment
+ (NSString *)hash:(NSData *)data;
+ (NSData *)sign:(NSData *)data withKeyPair:(QKeyPair *)keyPair;
+ (NSData *)verifySignedData:(NSData *)data withSignerKeyPair:(QKeyPair *)keyPair;

@end
