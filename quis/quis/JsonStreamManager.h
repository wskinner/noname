//
//  JsonStreamManager.h
//  quis
//
//  Created by Will Skinner on 12/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface JsonStreamManager : NSObject
@property NSString *data;

- (void)addData:(NSString *)data;
- (NSString *)nextJson;

- (BOOL)hasData;

@end
