//
//  QNetHelper.h
//  quis
//
//  Created by Admin on 28/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "QBrowser.h"
#import "QAdvertiser.h"
#import "QDNSService.h"
#import "QServer.h"
#import "QConnection.h"

#import <CFNetwork/CFNetwork.h>
#import <sys/socket.h>

#import <netinet/in.h>
#import <sys/un.h>
#import <arpa/inet.h>
#import <netdb.h>

#define MAX_MSG_LEN 2048

@interface QNetHelper : NSObject

-(void) startAll;
-(void) stopAll;
+ (NSUInteger)sendMessage:(NSData *)message onStream:(NSOutputStream *)stream;

@end
