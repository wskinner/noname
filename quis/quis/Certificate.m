//
//  Certificate.m
//  firstTry
//
//  Created by Will Skinner on 9/8/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//
// Required keys:
//  certificateHolderInfo
//      name
//  certificateInfo
//      version
//  keyInfo
//      publicKey
//  signature
/*
{
 "data": {
            "subjectInfo": {
                              "firstName": string,
                              "lastName": string,
                              "emailAddress: string
                            },
            "certificateInfo": {
                                  "version": string,
                                  "validity": string
                                },
            "keyInfo": {
                          "encryptionAlgorithm": string,
                          "signingAlgorithm": string,
                          "encryptionKeyPublic": string,
                          "signingKeyPublic": string
                        }
         },
 "signatureAlgorithm": string,
 "signature": string
}
 1. hash data
 2. signature = signed(hash)
 */
// NB these certs are necessarily always self-signed
// keys should be ascii-armored binary representations of the key

#import "Certificate.h"
#import "CryptoAPI.h"

// TODO make this class more POJO style

@implementation Certificate
@synthesize certificateInfo, subjectInfo, keyInfo, signature, signatureAlgorithm, keyPair;

- (id)initFromBase64Blob:(NSData *)blob {
    self = [super init];
    if (self) {
        NSData *rawData = [[NSData alloc] initWithBase64EncodedData:blob options:0];
        return [self initFromBlob:rawData];
    }
    return self;
}

- (id)initFromBlob:(NSData *)blob {
    self = [super init];
    if (self) {
        if (!blob) {
            return nil;
        }
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:blob options:NSJSONReadingMutableContainers error:&error];
        if (error) {
            NSLog(@"Error initializing certificate from JSON");
            NSLog([error debugDescription]);
            NSLog(@"Bad JSON = %@", [[NSString alloc] initWithData:blob encoding:NSUTF8StringEncoding]);
            return nil;
        }
        NSMutableDictionary *data = [json valueForKey:@"data"];
        [self setSubjectInfo:[data valueForKey:@"subjectInfo"]];
        [self setCertificateInfo:[data valueForKey:@"certificateInfo"]];
        [self setKeyInfo:[data valueForKey:@"keyInfo"]];
        [self setSignature:[json valueForKey:@"signature"]];
        [self setSignatureAlgorithm:[json valueForKey:@"signatureAlgorithm"]];
    }
    return self;
}

- (id)intitForSubjectInfo:(NSDictionary *)subjectInfo andCertificateInfo:(NSDictionary *)certificateInfo
               andKeyInfo:(NSDictionary *)keyInfo andSignatureAlgorithm:(NSString *)signatureAlgorithm {
    return NULL;
}

- (id)initForQContacts:(SecQContacts *)contact {
    self = [super init];
    if (self) {
        if (!self.certificateVersion) {
            self.certificateVersion = @"0.0";
        }
        self.keyPair = contact.keyPair;
        
        self.subjectInfo = [[NSMutableDictionary alloc] init];
        [self.subjectInfo setValue:contact.emailAddress forKey:@"emailAddress"];
        [self.subjectInfo setValue:contact.firstName forKey:@"firstName"];
        [self.subjectInfo setValue:contact.lastName forKey:@"lastName"];
        
        self.keyInfo = [[NSMutableDictionary alloc] init];
        [self.keyInfo setValue:[contact.signingKeyPublic base64EncodedString] forKey:@"signingKeyPublic"];
        [self.keyInfo setValue:[contact.encryptionKeyPublic base64EncodedString] forKey:@"encryptionKeyPublic"];
        [self.keyInfo setValue:contact.signingAlgorithm forKey:@"signingAlgorithm"];
        [self.keyInfo setValue:contact.encryptionAlgorithm forKey:@"encryptionAlgorithm"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-mm-dd"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [dateFormatter setLocale:usLocale];
        NSString *dateEndPrev = [dateFormatter stringFromDate:[NSDate date]];
        
        NSDate *validityStart = [NSDate date];
        NSUInteger days = 365*3;
        NSDate *validityEnd = [validityStart dateByAddingTimeInterval:60*60*24*days];
        
        self.certificateInfo = [[NSMutableDictionary alloc] init];
        [self.certificateInfo setValue:[dateFormatter stringFromDate:validityStart] forKey:@"validityStart"];
        [self.certificateInfo setValue:[dateFormatter stringFromDate:validityEnd] forKey:@"validityEnd"];
        [self.certificateInfo setValue:self.certificateVersion forKey:@"version"];
        
    }
    return self;
    
}

- (NSMutableDictionary *)toJson {
    // first we must update the keyInfo in case it has changed
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:[self subjectInfo] forKey:@"subjectInfo"];
    [data setValue:[self certificateInfo] forKey:@"certificateInfo"];
    [data setValue:[self keyInfo] forKey:@"keyInfo"];
    
    NSMutableDictionary *cert = [[NSMutableDictionary alloc] init];
    [cert setValue:data forKey:@"data"];
    [cert setValue:self.signature forKey:@"signature"];
    [cert setValue:self.signatureAlgorithm forKey:@"signatureAlogrithm"];
    return cert;
}

- (NSData *)toBlob {
    NSDictionary *cert = [self toJson];
    NSError *err;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:cert
                                                       options:0
                                                         error:&err];
    return jsonData;
}

- (NSData *)toBase64Blob {
    return [[self toBlob] base64EncodedDataWithOptions:0];
}


- (BOOL)verifySignature {
    NSData *verification = [CryptoAPI verifySignedData:[self.signature base64DecodedData] withSignerKeyPair:self.keyPair];
    NSString *hash = [self computeHash];
    return [hash isEqualToString:[[NSString alloc] initWithData:verification encoding:NSUTF8StringEncoding]];
}

- (void)sign {
    NSString *hash = [self computeHash];
    NSData *toSign = [hash dataUsingEncoding:NSUTF8StringEncoding];
    NSData *signedHash = [CryptoAPI sign:toSign withKeyPair:self.keyPair];
    self.signature = [signedHash base64EncodedString];
}

// Algorithm:
// 1. Recursively sort the keys of the dictionary lexicographically.
// 2. Serialize the dictionary to JSON and compute the hash.
// Does not include the signature field in the hash (if it even exists at this point)
- (NSString *)computeHash {
    NSMutableDictionary *cert = [self toJson];
    NSMutableDictionary *data = [cert valueForKey:@"data"];
    NSString *sortser = [data sortRecursivelyAndSerialize];
    NSData *sortserD = [sortser dataUsingEncoding:NSUTF8StringEncoding];
    NSString *result = [CryptoAPI hash:sortserD];
    return result;
}

- (NSData *)signingKeyPublic {
    return [[self.keyInfo valueForKey:@"signingKeyPublic"] base64DecodedData];
}

- (NSData *)encryptionKeyPublic {
    return [[self.keyInfo valueForKey:@"encryptionKeyPublic"] base64DecodedData];
}

@end
