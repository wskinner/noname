//
//  PublicKey.m
//  firstTry
//
//  Created by Will Skinner on 8/31/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "PublicKey.h"
#import <sodium.h>

@implementation PublicKey
@synthesize public_key, public_signing_key;

- (id) init {
    self = [super init];
    if (self) {
        public_key = malloc(crypto_box_PUBLICKEYBYTES * sizeof(unsigned char));
        public_signing_key = malloc(crypto_sign_PUBLICKEYBYTES * sizeof(unsigned char));
    }
    return self;
}

- (void) dealloc {
    free(public_key);
    free(public_signing_key);
}

- (NSData *)encKeyData {
    return [[NSData alloc] initWithBytes:public_key length:crypto_box_PUBLICKEYBYTES];
}

- (NSData *)signKeyData {
    return [[NSData alloc] initWithBytes:public_signing_key length:crypto_sign_PUBLICKEYBYTES];
}
@end
