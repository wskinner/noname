//
//  PrivateKey.m
//  firstTry
//
//  Created by Will Skinner on 8/31/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "PrivateKey.h"

@implementation PrivateKey
@synthesize private_key, private_signing_key;
- (id) init {
    self = [super init];
    if (self) {
        private_key = malloc(crypto_box_SECRETKEYBYTES * sizeof(unsigned char));
        private_signing_key = malloc(crypto_sign_SECRETKEYBYTES * sizeof(unsigned char));
    }
    return self;
}

- (void) dealloc {
    free(private_key);
    free(private_signing_key);
}

- (NSData *)encKeyData {
    return [[NSData alloc] initWithBytes:private_key length:crypto_box_SECRETKEYBYTES];
}

- (NSData *)signKeyData {
    return [[NSData alloc] initWithBytes:private_signing_key length:crypto_sign_SECRETKEYBYTES];
}

@end
