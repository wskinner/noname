//
//  Messages.m
//  firstTry
//
//  Created by Will Skinner on 9/4/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "Messages.h"

@implementation Messages

@dynamic date;
@dynamic nonce;
@dynamic text;
@dynamic identity;
@dynamic recipient;

@end
