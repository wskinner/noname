//
//  NSDictionary+GetValue.m
//  quis
//
//  Created by Will Skinner on 12/8/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "NSDictionary+GetValue.h"

@implementation NSDictionary (GetValue)
- (NSString *)getValueOrEmptyString:(NSString *)key {
    NSString *value = [self valueForKey:key];
    if (!value) {
        return @"";
    } else {
        return value;
    }
}

@end
