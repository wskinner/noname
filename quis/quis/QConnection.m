//
//  QConnection.m
//  quis
//
//  Created by Admin on 27/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//
// server

#import "QConnection.h"

NSString * QConnectionDidCloseNotification = @"QConnectionDidCloseNotification";

@interface QConnection () <NSStreamDelegate, UIAlertViewDelegate>

@end


@implementation QConnection

@synthesize inputStream  = _inputStream;
@synthesize outputStream = _outputStream;

@synthesize keyXDelegate;

- (id)initWithInputStream:(NSInputStream *)inputStream outputStream:(NSOutputStream *)outputStream
{
    self = [super init];
    if (self) {
        self->_inputStream = inputStream;
        self->_outputStream = outputStream;
        
        QAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        self.keyXDelegate = [[QKeyExchange alloc] initWithOutputStream:self.outputStream inputStream:self.inputStream managedObjectContext:delegate.managedObjectContext serverMode:YES caller:self];
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        QAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        self.keyXDelegate = [[QKeyExchange alloc] initWithOutputStream:self.outputStream inputStream:self.inputStream managedObjectContext:delegate.managedObjectContext serverMode:YES caller:self];
    }
    return self;
}

- (BOOL)open {
    [self.inputStream  setDelegate:self];
    [self.outputStream setDelegate:self];
    [self.inputStream  scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.inputStream  open];
    [self.outputStream open];
    return YES;
}

- (void)close {
    [self.inputStream  setDelegate:nil];
    [self.outputStream setDelegate:nil];
    [self.inputStream  close];
    [self.outputStream close];
    [self.inputStream  removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [(NSNotificationCenter *)[NSNotificationCenter defaultCenter] postNotificationName:QConnectionDidCloseNotification object:self];
}

// key exchange codes goes in here
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)streamEvent {
    assert(aStream == self.inputStream || aStream == self.outputStream);
    
    if (!self.keyXDelegate) {
        QAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        self.keyXDelegate = [[QKeyExchange alloc] initWithOutputStream:self.outputStream inputStream:self.inputStream managedObjectContext:delegate.managedObjectContext serverMode:YES caller:self];
        NSLog(@"initialized keyXDelegate in QConnection.handleEvent");
    }
    
    NSLog(@"stream event in QConnection: %u", streamEvent);
    
    switch(streamEvent) {
        case NSStreamEventHasBytesAvailable: {
            NSLog(@"server hasBytesAvailable");
            [self.keyXDelegate hasBytesAvailable];
        } break;
        case NSStreamEventEndEncountered:
        case NSStreamEventErrorOccurred: {
            [self close];
        } break;
        case NSStreamEventHasSpaceAvailable: {
            NSLog(@"server hasSpaceAvailable");
            [self.keyXDelegate hasSpaceAvailable];
        } break;
        case NSStreamEventOpenCompleted:
        default: {
            // do nothing
        } break;
    }
}


- (void)setTheirCode:(NSString *)code {
}

- (void)setMyCode:(NSString *)code {
}


- (NSString *)theirCode {
    return nil;
}

- (NSString *)myCode {
    return nil;
}


@end

