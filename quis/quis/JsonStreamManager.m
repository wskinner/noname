//
//  JsonStreamManager.m
//  quis
//
//  Created by Will Skinner on 12/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "JsonStreamManager.h"
#import "NSMutableArray+Queue.h"


@implementation JsonStreamManager
@synthesize data;

- (id)init {
    self = [super init];
    if (self) {
        self.data = @"";
    }
    return self;
}

- (void)addData:(NSString *)newData {
    if (newData) {
        self.data = [self.data stringByAppendingString:newData];
    } else {
        self.data = [self.data stringByAppendingString:@""];
    }
}

- (NSString *)nextJson {
    NSInteger parenCount = 0;
    NSUInteger endIndex = 0;
    
    for (NSUInteger i = 0; i < self.data.length; i++) {
        unichar c = [self.data characterAtIndex:i];
        if (c == '{') {
            parenCount++;
        } else if (c == '}') {
            parenCount--;
        }
        
        if (parenCount == 0) {
            endIndex = i+1;
            break;
        }
    }
    
    if (parenCount != 0 || endIndex == 0) {
        return nil;
    }
    
    NSString *json = [self.data substringToIndex:endIndex];
    NSUInteger remainderStart = endIndex;
    NSUInteger remainderLength = self.data.length - remainderStart;
    self.data = [self.data substringWithRange:NSMakeRange(remainderStart, remainderLength)];
    return json;
}

- (BOOL)hasData {
    return self.data.length > 0;
}

@end
