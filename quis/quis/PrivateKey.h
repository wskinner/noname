//
//  PrivateKey.h
//  firstTry
//
//  Created by Will Skinner on 8/31/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sodium.h>

@interface PrivateKey : NSObject {
    unsigned char *private_key;
}
@property (atomic) unsigned char *private_key;
@property (atomic) unsigned char *private_signing_key;

- (NSData *)encKeyData;
- (NSData *)signKeyData;
@end
