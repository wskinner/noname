//
//  UID.h
//  quis
//
//  Created by Admin on 15/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UID : NSManagedObject

@property (nonatomic, retain) NSNumber * uid;

@end
