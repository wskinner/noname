//
//  QConversationListViewController.m
//  quis
//
//  Created by Admin on 07/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//
#import "QConversationViewController.h"
#import "QConversationListViewController.h"
#import "QMessage.h"
#import "QContacts.h"
#import "QAppDelegate.h"

@interface QConversationListViewController ()<UITableViewDelegate>

@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic,strong) NSString* myText;
@property (nonatomic,strong) NSString* sender;
@property (nonatomic, strong) MCOIMAPSession* session;

@end
#define NAME_TAG 1
#define TIME_TAG 2
#define IMAGE_TAG 3
#define TEXT_TAG 4
#define ROW_HEIGHT 70
#define MESSAGES_PER_PAGE 20
#define IMAGE_SIDE 48
#define BORDER_WIDTH 5
#define TEXT_OFFSET_X (BORDER_WIDTH * 2 + IMAGE_SIDE)
#define LABEL_HEIGHT 20
#define LABEL_WIDTH 130
#define TEXT_WIDTH (320 - TEXT_OFFSET_X - BORDER_WIDTH)
#define TEXT_OFFSET_Y (BORDER_WIDTH * 2 + LABEL_HEIGHT)
#define TEXT_HEIGHT (ROW_HEIGHT - TEXT_OFFSET_Y - BORDER_WIDTH)

@implementation QConversationListViewController
@synthesize managedObjectContext = _managedObjectContext;
@synthesize fetchedResultsController = _fetchedResultsController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//    QConversationViewController* parentView = (QConversationViewController*)  self.parentViewController;
//    self.managedObjectContext =  parentView.managedObjectContext;
//    self.sender = parentView.sender;
    QAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    self.managedObjectContext = delegate.managedObjectContext;
    
    self.conversation.separatorColor = [UIColor clearColor];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendData" object:self.myText userInfo:nil];
    
    // refresh stuff
    SEL refreshQuis = @selector(refreshQuis);
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    //refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    refreshControl.tintColor = [UIColor blueColor];
    [refreshControl addTarget:self action:refreshQuis forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    self.session = [QEmailHelpers session];    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear");
    if(!self.conversationItems) {
        NSLog(@"!self.conversationItems");
        [self.conversation reloadData];
        
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(emailDatabaseDidUpdate:)
                               name:NSManagedObjectContextDidSaveNotification
                             object:self.managedObjectContext];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopAllRefreshing)
                                                 name:@"stopAllRefreshing"
                                               object:nil];
}

-(void) viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NSManagedObjectContextDidSaveNotification object:nil]; // TODO need to change this to app delegate MOC
}

-(void) emailDatabaseDidUpdate:(NSNotification*)saveNotification {
    // faulted objects dont fire required notification, so fault them in
    
    for(NSManagedObject *object in [[saveNotification userInfo] objectForKey:NSUpdatedObjectsKey]) {
        [[self.managedObjectContext objectWithID:[object objectID]] willAccessValueForKey:nil];
    }
    
    [self.managedObjectContext  mergeChangesFromContextDidSaveNotification:saveNotification];
    
    
    [self.managedObjectContext setStalenessInterval:0];

    NSError *error;
    
    //    // get messages
    //    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    //    NSEntityDescription *entity = [NSEntityDescription entityForName:@"QContacts" inManagedObjectContext: self.managedObjectContext];
    //    [fetchRequest setEntity:entity];
    //    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    //    NSLog(@"Fetched %d objects",[fetchedObjects count]);
    //    for (QContacts *info in fetchedObjects) {
    //        NSLog(@"Sender: %@", info.emailAddress);
    //    }
    
    //@property (nonatomic, strong) NSMutableArray *conversationItems;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"QContacts" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    //[fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    //    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"mostRecentMessageDate" ascending:NO];
    //    NSArray *sortDescriptors = @[sortDescriptor];
    //
    //    [fetchRequest setSortDescriptors:sortDescriptors];
    
    //we do not want to fetch those contacts whose mostRecentMessageDate is nil
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K like %@",@"emailAddress",self.sender];
    NSLog(@"Query %@",predicate);
    [fetchRequest setPredicate:predicate];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSArray *array = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (array == nil)
        
    {
        
        // Deal with error...
        
    }
    
    if ([array count] != 1) {
        NSLog(@"WARNING: same email address has two contacts");
    }
    
    SecQContacts* contact = [array objectAtIndex:0];
    
    NSSet* qMessages = contact.qMessages;
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    NSArray* sortDescriptors = @[sortDescriptor];
    self.conversationItems = [[NSMutableArray alloc] initWithArray:[qMessages sortedArrayUsingDescriptors:sortDescriptors]];
    [self.conversation reloadData];
}


-(void) refreshQuis {

    [QPersistenceHelpers refreshQuis:self.refreshControl];
    
}

-(void) stopAllRefreshing {
    if (self.refreshControl.refreshing) [self.refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.conversationItems count];
}

- (void)configureCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    
    //    if([self noMessages])
    //    {
    //        if(_errorDesc)
    //            cell.text = _errorDesc;
    //        else
    //            cell.text = _loading? [self loadingMessagesString]: [self noMessagesString];
    //        return;
    //    }
    
    if(indexPath.row < [self.conversationItems count])
    {
        static NSDateFormatter *dateFormatter = nil;
        if (dateFormatter == nil)
            dateFormatter = [[NSDateFormatter alloc] init];
        
        static NSCalendar *calendar;
        if(calendar == nil)
            calendar= [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        //        NSDictionary *messageData = [_messages objectAtIndex:indexPath.row];
        //        NSDictionary *userData = [messageData objectForKey:@"user"];
        //        if(!userData)
        //            userData = [messageData objectForKey:@"sender"];
        
        CGRect cellFrame = [cell frame];
        //Set message text
        UILabel *label;
        label = (UILabel *)[cell viewWithTag:TEXT_TAG];
        
        //label.text = DecodeEntities([messageData objectForKey:@"text"]);
        SecQMessage* qMessage = (SecQMessage*) [self.conversationItems objectAtIndex:indexPath.row];
        if ([qMessage encrypted]) {
            [qMessage decrypt];
        }
        label.text = [[NSString alloc] initWithData:qMessage.content encoding:NSUTF8StringEncoding];
        if ([qMessage encrypted]) {
            label.text = @"Message decryption failed";
        }
        [label setFrame:CGRectMake(TEXT_OFFSET_X, TEXT_OFFSET_Y, TEXT_WIDTH, TEXT_HEIGHT)];
        [label sizeToFit];
        if(label.frame.size.height > TEXT_HEIGHT)
        {
            cellFrame.size.height = ROW_HEIGHT + label.frame.size.height - TEXT_HEIGHT;
        }
        else
        {
            cellFrame.size.height = ROW_HEIGHT;
        }
        
        [cell setFrame:cellFrame];
        
        
        //Set message date and time
        NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
        label = (UILabel *)[cell viewWithTag:TIME_TAG];
        NSDate *createdAt = qMessage.date;
        NSDateComponents *nowComponents = [calendar components:unitFlags fromDate:[NSDate date]];
        NSDateComponents *yesterdayComponents = [calendar components:unitFlags fromDate:[NSDate dateWithTimeIntervalSinceNow:-60*60*24]];
        NSDateComponents *createdAtComponents = [calendar components:unitFlags fromDate:createdAt];
        
        if([nowComponents year] == [createdAtComponents year] &&
           [nowComponents month] == [createdAtComponents month] &&
           [nowComponents day] == [createdAtComponents day])
        {
            [dateFormatter setDateStyle:NSDateFormatterNoStyle];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            label.text = [dateFormatter stringFromDate:createdAt];
        }
        else if([yesterdayComponents year] == [createdAtComponents year] &&
                [yesterdayComponents month] == [createdAtComponents month] &&
                [yesterdayComponents day] == [createdAtComponents day])
        {
            [dateFormatter setDateStyle:NSDateFormatterNoStyle];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            label.text = [NSString stringWithFormat:@"Yesterday, %@", [dateFormatter stringFromDate:createdAt]];
        }
        else
        {
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            label.text = [dateFormatter stringFromDate:createdAt];
        }
        
        //
        //        //Set userpic
        //        UIImageView *imageView = (UIImageView *)[cell viewWithTag:IMAGE_TAG];
        //        imageView.image = nil;
        //        [[ImageLoader sharedLoader] setImageWithURL:[userData objectForKey:@"profile_image_url"] toView:imageView];
        //
        //        //Set user name
        //        label = (UILabel *)[cell viewWithTag:NAME_TAG];
        //        label.text = [userData objectForKey:@"screen_name"];
    }
    else
    {
        cell.text = @"Load More...";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"quisListCell";//![self noMessages] && indexPath.row < [_messages count]? @"TwittListCell": @"UICell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [self tableviewCellWithReuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:cell forIndexPath:indexPath];
    
    cell.contentView.backgroundColor = indexPath.row % 2? [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]: [UIColor whiteColor];
    
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row >= [self.conversationItems count]) return 50;
    
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}




- (UITableViewCell *)tableviewCellWithReuseIdentifier:(NSString *)identifier
{
    if([identifier isEqualToString:@"UICell"])
    {
        UITableViewCell *uiCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        uiCell.textAlignment = UITextAlignmentCenter;
        uiCell.font = [UIFont systemFontOfSize:16];
        return uiCell;
    }
    
    
    
    if([identifier isEqualToString:@"quisListCell"])
    {
        CGRect rect;
        
        rect = CGRectMake(0.0, 0.0, 320.0, ROW_HEIGHT);
        
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.frame = rect;
        //Userpic view
        rect = CGRectMake(BORDER_WIDTH, (ROW_HEIGHT - IMAGE_SIDE) / 2.0, IMAGE_SIDE, IMAGE_SIDE);
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:rect];
        imageView.tag = IMAGE_TAG;
        [cell.contentView addSubview:imageView];
        
        UILabel *label;
        
        //Username
        rect = CGRectMake(TEXT_OFFSET_X, BORDER_WIDTH, LABEL_WIDTH, LABEL_HEIGHT);
        label = [[UILabel alloc] initWithFrame:rect];
        label.tag = NAME_TAG;
        label.font = [UIFont boldSystemFontOfSize:[UIFont labelFontSize]];
        label.highlightedTextColor = [UIColor whiteColor];
        [cell.contentView addSubview:label];
        label.opaque = NO;
        label.backgroundColor = [UIColor clearColor];
        
        
        //Message creation time
        rect = CGRectMake(TEXT_OFFSET_X + LABEL_WIDTH, BORDER_WIDTH, LABEL_WIDTH, LABEL_HEIGHT);
        label = [[UILabel alloc] initWithFrame:rect];
        label.tag = TIME_TAG;
        label.font = [UIFont systemFontOfSize:[UIFont smallSystemFontSize]];
        label.textAlignment = UITextAlignmentRight;
        label.highlightedTextColor = [UIColor whiteColor];
        label.textColor = [UIColor lightGrayColor];
        [cell.contentView addSubview:label];
        label.opaque = NO;
        label.backgroundColor = [UIColor clearColor];
        
        
        //Message body
        rect = CGRectMake(TEXT_OFFSET_X, TEXT_OFFSET_Y, TEXT_WIDTH, TEXT_HEIGHT);
        label = [[UILabel alloc] initWithFrame:rect];
        label.tag = TEXT_TAG;
        label.lineBreakMode = UILineBreakModeWordWrap;
        label.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        label.highlightedTextColor = [UIColor whiteColor];
        label.numberOfLines = 0;
        [cell.contentView addSubview:label];
        label.opaque = NO;
        label.backgroundColor = [UIColor clearColor];
        
        
        return cell;
    }
    
    return nil;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
