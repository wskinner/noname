//
//  SecQContacts.m
//  quis
//
//  Created by Will Skinner on 10/12/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "SecQContacts.h"

@interface SecQContacts ()
@property NSMutableDictionary *msgIds;
@property BOOL saved;

@end


@implementation SecQContacts
@synthesize keyPair, certificate, signingAlgorithm, signingKeyPublic, encryptionAlgorithm, encryptionKeyPublic, encryptionKeyPrivate, signingKeyPrivate, msgIds, saved;

- (void)awakeFromInsert {
    [super awakeFromInsert];
    if (self.messageIds) {
        NSError *error;
        self.msgIds = [NSJSONSerialization JSONObjectWithData:[self.messageIds dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    } else {
        self.msgIds = [[NSMutableDictionary alloc] init];
    }
    self.saved = NO;
}

- (void)awakeFromFetch {
    [super awakeFromFetch];
    NSData *eKPu = [[[NSString alloc] initWithData:self.encryptionKeyPublic encoding:NSUTF8StringEncoding] base64DecodedData];
    NSData *eKPr = [[[NSString alloc] initWithData:self.encryptionKeyPrivate encoding:NSUTF8StringEncoding] base64DecodedData];
    NSData *sKPu = [[[NSString alloc] initWithData:self.signingKeyPublic encoding:NSUTF8StringEncoding] base64DecodedData];
    NSData *sKPr = [[[NSString alloc] initWithData:self.signingKeyPrivate encoding:NSUTF8StringEncoding] base64DecodedData];
    self.keyPair = [[QKeyPair alloc] initWithEncryptionKeyPublic:eKPu encryptionKeyPrivate:eKPr signingKeyPublic:sKPu signingKeyPrivate:sKPr];
    
    if (self.messageIds) {
        NSError *error;
        self.msgIds = [NSJSONSerialization JSONObjectWithData:[self.messageIds dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    } else {
        self.msgIds = [[NSMutableDictionary alloc] init];
    }
    self.saved = NO;
    
    self.certificate = [[Certificate alloc] initFromBlob:self.certificateData];
}

- (void)willSave {
    [super willSave];
    if (!self.saved) {
        self.saved = YES;
        self.messageIds = [self.msgIds sortRecursivelyAndSerialize];
    }
}

- (void)generateKeyPairs {
    QKeyPair *kp = [[QKeyPair alloc] init];
    self.keyPair = kp;
    self.encryptionKeyPublic = [kp.encryptionKeyPublic base64EncodedDataWithOptions:0];
    self.signingKeyPublic = [kp.signingKeyPublic base64EncodedDataWithOptions:0];
    self.encryptionKeyPrivate = [kp.encryptionKeyPrivate base64EncodedDataWithOptions:0];
    self.signingKeyPrivate = [kp.signingKeyPrivate base64EncodedDataWithOptions:0];
}

- (void)generateCertificate {
    self.certificate = [[Certificate alloc] initForQContacts:self];
    self.certificateData = [self.certificate toBlob];
}

- (void)populateInfoFromCert:(Certificate *)cert {
    self.firstName = [cert.subjectInfo getValueOrEmptyString:@"firstName"];
    self.lastName = [cert.subjectInfo getValueOrEmptyString:@"lastName"];
    self.emailAddress = [cert.subjectInfo getValueOrEmptyString:@"emailAddress"];
    
    self.encryptionAlgorithm = [cert.keyInfo getValueOrEmptyString:@"encryptionAlgorithm"];
    self.signingAlgorithm = [cert.keyInfo getValueOrEmptyString:@"signingAlgorithm"];
    self.encryptionKeyPublic = [[cert.keyInfo getValueOrEmptyString:@"encryptionKeyPublic"] dataUsingEncoding:NSUTF8StringEncoding];
    self.encryptionKeyPrivate = [[cert.keyInfo getValueOrEmptyString:@"encryptionKeyPrivate"] dataUsingEncoding:NSUTF8StringEncoding];
    self.signingKeyPublic = [[cert.keyInfo getValueOrEmptyString:@"signingKeyPublic"] dataUsingEncoding:NSUTF8StringEncoding];
    self.signingKeyPrivate = [[cert.keyInfo getValueOrEmptyString:@"signingKeyPrivate"] dataUsingEncoding:NSUTF8StringEncoding];
    
    self.keyPair = [[QKeyPair alloc] initWithEncryptionKeyPublic:self.encryptionKeyPublic encryptionKeyPrivate:self.encryptionKeyPrivate signingKeyPublic:self.signingKeyPublic signingKeyPrivate:self.signingKeyPrivate];
}


- (NSUInteger)nextMessageIdForRecipient:(SecQContacts *)recipient {
    NSString *currentId = [self.msgIds objectForKey:[recipient.uid stringValue]];
    NSUInteger result;
    if (currentId) {
        NSUInteger currentInt = currentId.integerValue;
        result = currentInt + 1;
    } else {
        result = 0;
    }
    [self.msgIds setObject:[NSString stringWithFormat:@"%ul", result] forKey:[recipient.uid stringValue]];
    return result;
}


- (void)describeKeys {
    NSLog(@"encryptionKeyPublic: %@", self.keyPair.encryptionKeyPublic);
    NSLog(@"signingKeyPublic: %@", self.keyPair.signingKeyPublic);
}

- (NSString *)SYN {
    return @"Contents of users SYN";
}

@end
