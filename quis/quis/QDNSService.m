//
//  QDNSService.m
//  quis
//
//  Created by Admin on 26/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QDNSService.h"

#include <dns_sd.h>

#pragma mark * QDNSService

@interface QDNSService ()

// read-write versions of public properties

@property (copy,   readwrite) NSString * resolvedHost;
@property (assign, readwrite) NSUInteger resolvedPort;

// private properties

@property (assign, readwrite) DNSServiceRef      sdRef;
@property (retain, readwrite) NSTimer *          resolveTimeoutTimer;

// forward declarations

- (void)stopWithError:(NSError *)error notify:(BOOL)notify;

@end

@implementation QDNSService

@synthesize domain = domain_;
@synthesize type = type_;
@synthesize name = name_;

@synthesize delegate = delegate_;

@synthesize resolvedHost = resolvedHost_;
@synthesize resolvedPort = resolvedPort_;

@synthesize sdRef = sdRef_;
@synthesize resolveTimeoutTimer = resolveTimeoutTimer_;

- (id)initWithDomain:(NSString *)domain type:(NSString *)type name:(NSString *)name
// See comment in header.
{
    assert(domain != nil);
    assert([domain length] != 0);
    assert( ! [domain hasPrefix:@"."] );
    assert(type != nil);
    assert([type length] != 0);
    assert( ! [type hasPrefix:@"."] );
    assert(name != nil);
    assert([name length] != 0);
    self = [super init];
    if (self != nil) {
        if ( ! [domain hasSuffix:@"."] ) {
            domain = [domain stringByAppendingString:@"."];
        }
        if ( ! [type hasSuffix:@"."] ) {
            type = [type stringByAppendingString:@"."];
        }
        self->domain_ = [domain copy];
        self->type_   = [type copy];
        self->name_   = [name copy];
    }
    return self;
}

- (void)dealloc
{
    if (self->sdRef_ != NULL) {
        DNSServiceRefDeallocate(self->sdRef_);
    }
//    [self->resolveTimeoutTimer_ invalidate];
//    [self->resolveTimeoutTimer_ release];
//    [self->domain_ release];
//    [self->type_ release];
//    [self->name_ release];
//    [self->resolvedHost_ release];
//    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone
// Part of the NSCopying protocol, as discussed in the header.
{
    return [[[self class] allocWithZone:zone] initWithDomain:self.domain type:self.type name:self.name];
}

// We have to implement -isEqual: and -hash to allow the object to function correctly
// when placed in sets.

- (BOOL)isEqual:(id)object
// See comment in header.
{
    QDNSService *   other;
    
    // Boilerplate stuff.
    
    if (object == self) {
        return YES;
    }
    if ( ! [object isKindOfClass:[self class]] ) {
        return NO;
    }
    
    // Compare the domain, type and name.
    
    other = (QDNSService *) object;
    return [self.domain isEqualToString:other.domain] && [self.type isEqualToString:other.type] && [self.name isEqualToString:other.name];
}

- (NSUInteger)hash
// See comment in header.
{
    return [self.domain hash] ^ [self.type hash] ^ [self.name hash];
}

- (NSString *)description
// We override description to make it easier to debug operations that involve lots of DNSSDService
// objects.  This is really helpful, for example, when you have an NSSet of discovered services and
// you want to check that new services are being added correctly.
{
    return [NSString stringWithFormat:@"%@ {%@, %@, %@}", [super description], self.domain, self.type, self.name];
}

- (void)resolveReplyWithTarget:(NSString *)resolvedHost port:(NSUInteger)port
// Called when DNS-SD tells us that a resolve has succeeded.
{
    assert(resolvedHost != nil);
    
    // Latch the results.
    
    self.resolvedHost = resolvedHost;
    self.resolvedPort = port;
    
    // Tell our delegate.
    
    if ([self.delegate respondsToSelector:@selector(QDNSServiceDidResolveAddress:)]) {
        [self.delegate QDNSServiceDidResolveAddress:self];
    }
    
    // Stop resolving.  It's common for clients to forget to do this, so we always do
    // it as a matter of policy.  If you want to issue a long-running resolve, you're going
    // to have tweak this code.
    
    [self stopWithError:nil notify:YES];
}

static void ResolveReplyCallback(
                                 DNSServiceRef           sdRef,
                                 DNSServiceFlags         flags,
                                 uint32_t                interfaceIndex,
                                 DNSServiceErrorType     errorCode,
                                 const char *            fullname,
                                 const char *            hosttarget,
                                 uint16_t                port,
                                 uint16_t                txtLen,
                                 const unsigned char *   txtRecord,
                                 void *                  context
                                 )
// Called by DNS-SD when something happens with the resolve operation.
{
    QDNSService *       obj;
#pragma unused(interfaceIndex)
    
    assert([NSThread isMainThread]);        // because sdRef dispatches to the main queue
    
    obj = (__bridge QDNSService *) context;
    assert([obj isKindOfClass:[QDNSService class]]);
    assert(sdRef == obj->sdRef_);
#pragma unused(sdRef)
#pragma unused(flags)
#pragma unused(fullname)
#pragma unused(txtLen)
#pragma unused(txtRecord)
    
    if (errorCode == kDNSServiceErr_NoError) {
        [obj resolveReplyWithTarget:[NSString stringWithUTF8String:hosttarget] port:ntohs(port)];
    } else {
        [obj stopWithError:[NSError errorWithDomain:NSNetServicesErrorDomain code:errorCode userInfo:nil] notify:YES];
    }
}

- (void)startResolve
// See comment in header. kDNSServiceInterfaceIndexAny|
{
    if (self.sdRef == NULL) {
        DNSServiceErrorType errorCode;
                errorCode = DNSServiceResolve(&self->sdRef_, 0,kDNSServiceInterfaceIndexAny |kDNSServiceInterfaceIndexP2P, [self.name UTF8String], [self.type UTF8String], [self.domain UTF8String], ResolveReplyCallback, (__bridge void *)(self));
        if (errorCode == kDNSServiceErr_NoError) {
            errorCode = DNSServiceSetDispatchQueue(self.sdRef, dispatch_get_main_queue());
        }
        if (errorCode == kDNSServiceErr_NoError) {
            
            // Service resolution /never/ times out.  This is convenient in some circumstances,
            // but it's generally best to use some reasonable timeout.  Here we use an NSTimer
            // to trigger a failure if we spend more than 30 seconds waiting for the resolve.
            
            self.resolveTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(didFireResolveTimeoutTimer:) userInfo:nil repeats:NO];
            
            if ([self.delegate respondsToSelector:@selector(QDNSServiceWillResolve:)]) {
                [self.delegate QDNSServiceWillResolve:self];
            }
        } else {
            [self stopWithError:[NSError errorWithDomain:NSNetServicesErrorDomain code:errorCode userInfo:nil] notify:YES];
        }
    }
}

- (void)didFireResolveTimeoutTimer:(NSTimer *)timer
{
    assert(timer == self.resolveTimeoutTimer);
    [self stopWithError:[NSError errorWithDomain:NSNetServicesErrorDomain code:kDNSServiceErr_Timeout userInfo:nil] notify:YES];
}

- (void)stopWithError:(NSError *)error notify:(BOOL)notify
// An internal bottleneck for shutting down the object.
{
    if (notify) {
        if (error != nil) {
            if ([self.delegate respondsToSelector:@selector(QDNSService:didNotResolve:)]) {
                [self.delegate QDNSService:self didNotResolve:error];
            }
        }
    }
    if (self.sdRef != NULL) {
        DNSServiceRefDeallocate(self.sdRef);
        self.sdRef = NULL;
    }
    [self.resolveTimeoutTimer invalidate];
    self.resolveTimeoutTimer = nil;
    if (notify) {
        if ([self.delegate respondsToSelector:@selector(QDNSServiceDidStop:)]) {
            [self.delegate QDNSServiceDidStop:self];
        }
    }
}

- (void)stop
// See comment in header.
{
    [self stopWithError:nil notify:NO];
}
@end

