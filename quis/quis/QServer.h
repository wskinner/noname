//
//  QServer.h
//  quis
//
//  Created by Admin on 27/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QConnection.h"

#import <Foundation/Foundation.h>

@interface QServer : NSObject
@property (nonatomic, assign, readonly ) NSUInteger     port;   // the actual port bound to, valid after -start

- (BOOL)start;
- (void)stop;

- (void)acceptConnection:(CFSocketNativeHandle)nativeSocketHandle;
- (int) activeConnections;

// return the a connection - assumes set contains only one connection!!!
- (QConnection *)getConnection;

@end
