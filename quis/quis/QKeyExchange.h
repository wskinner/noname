//
//  QKeyExchange.h
//  quis
//
//  Created by Will Skinner on 11/17/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyExchangeDelegate.h"
#import "QKeyExchangeViewDelegate.h"
#import "NSDictionary+GetValue.h"

@class HandleConfirmChlngDelegate;

@interface QKeyExchange : NSObject <KeyExchangeDelegate>

@property (nonatomic, strong) NSOutputStream *outputStream;
@property (nonatomic, strong) NSInputStream *inputStream;
@property (nonatomic, strong) NSManagedObjectContext *moc;
@property (nonatomic, strong) SecQContacts *friend;
@property (nonatomic, strong) SecQContacts *dqc;

@property (nonatomic) BOOL myCertSent;
@property (nonatomic) BOOL myChallengeSent;
@property (nonatomic) BOOL theirCertConfirmed;
@property (nonatomic) BOOL myCertConfirmed;

@property (nonatomic) BOOL hasReceivedHello;
@property (nonatomic) BOOL hasSentHello;
@property (nonatomic) BOOL serverMode;

@property (nonatomic, strong) NSString *theirCode;
@property (nonatomic, strong) NSString *myCode;
@property (nonatomic, strong) SecQMessage *chlngMsg;

@property (nonatomic) HandleConfirmChlngDelegate *handleConfirmChallengeDelegate;
@property (nonatomic, strong) NSMutableArray *alerts;

@property (nonatomic) BOOL hasSpace;
@property (nonatomic) BOOL hasBytes;;

@property (retain) id <QKeyExchangeViewDelegate> caller;


- (id)initWithOutputStream:(NSOutputStream *)out inputStream:(NSInputStream *)in managedObjectContext:(NSManagedObjectContext *)moc serverMode:(BOOL)mode caller:(id)callingClass;
- (void)hasBytesAvailable;
- (void)hasSpaceAvailable;

+ (void)keyExchangeDidFinish;

@end
