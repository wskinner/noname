//
//  QSessionLocal.h
//  quis
//
//  Created by Admin on 21/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved. Git
//

@import MultipeerConnectivity;
#import <Foundation/Foundation.h>

@interface QSessionLocal : NSObject

-(id) initWithPeer: (MCPeerID*) userEmailPeerID;
@property (strong, nonatomic) MCSession* session;

@end
