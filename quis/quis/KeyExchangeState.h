//
//  KeyExchangeState.h
//  quis
//
//  Created by Will Skinner on 12/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//
// The state machine associated with an instance of a key exchange

#import <Foundation/Foundation.h>

@interface KeyExchangeState : NSObject

- (BOOL)canSendHello;
- (BOOL)canSendCert;
- (BOOL)canSendChallenge;

- (BOOL)isServerMode;

- (void)notifyHelloSent;
- (void)notifyCertSent;
- (void)notifyChallengeSent;
- (void)commitState;

@end
