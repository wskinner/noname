//
//  ComposeViewController.m
//  firstTry
//
//  Created by Admin on 24/09/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QComposeViewController.h"
#import <MailCore/MailCore.h>

@interface QComposeViewController ()
@property (strong,nonatomic) QResizingTextView *textView;
@property (strong, nonatomic) IBOutlet UITextField *emailAddress;

@end

@implementation QComposeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.tabBarController.tabBar setHidden:YES];
    
	if(self){
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillShow:)
													 name:UIKeyboardWillShowNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillHide:)
													 name:UIKeyboardWillHideNotification
												   object:nil];
	}
    NSLog(@"self.containerView.frame %f, %f",self.containerView.frame.size.height,self.containerView.frame.size.width);
    //self.containerView.backgroundColor =[UIColor colorWithRed:219.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1];
    
    NSLog(@"HPGrowingTextView alloc");
    self.textView = [[QResizingTextView alloc] initWithFrame:CGRectMake(6, 3, 240, 40)];
    //self.textView.backgroundColor = [UIColor colorWithRed:219.0f/255.0f green:226.0f/255.0f blue:237.0f/255.0f alpha:1];
    
    self.textView.isScrollable = NO;
    self.textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
	self.textView.minNumberOfLines = 1;
	//self.textView.maxNumberOfLines = 9999;
    // you can also set the maximum height in points with maxHeight
    // maxHeight is whole height less nav bar and keyboard
    self.textView.maxHeight = 800.0f;
	self.textView.returnKeyType = UIReturnKeyGo; //just as an example
	self.textView.font = [UIFont systemFontOfSize:15.0f];
	self.textView.delegate = self;
    self.textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    self.textView.backgroundColor = [UIColor whiteColor];
    //self.textView.placeholder = @"quis";
    
    
    
    [self.containerView addSubview:self.textView];
    
    
	UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn.frame = CGRectMake(self.containerView.frame.size.width - 69, 8, 63, 27);
    doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
	[doneBtn setTitle:@"Send" forState:UIControlStateNormal];
    [doneBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    //[doneBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    //doneBtn.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    //doneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    
    //[doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[doneBtn addTarget:self action:@selector(resignTextView) forControlEvents:UIControlEventTouchUpInside];
    
    //[doneBtn setBackgroundImage:sendBtnBackground forState:UIControlStateNormal];
    //[doneBtn setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];
    
	
    [self.containerView addSubview:doneBtn];
    self.containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    NSLog(@"Nav bar height %f y origin %f",self.navigationController.navigationBar.frame.size.height,self.navigationController.navigationBar.frame.origin.y);
    
    //
    //    NSLog(@"self.containerView.frame %f, origin %f %f",self.containerView.frame.size.height,self.tableViewContainer.frame.origin.x,self.containerView.frame.origin.y);
    //
    //    NSLog(@"childView.conversation.contentSize.height %f",self.childView.conversation.contentSize.height);
    //    // scroll down to the last row
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([self.conversationItems count]-1) inSection:0];
    //    [self.childView.conversation scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    //
    //    NSLog(@"tableViewContainer %f, origin %f %f",self.tableViewContainer.frame.size.height,self.tableViewContainer.frame.origin.x,self.tableViewContainer.frame.origin.y);
    //    NSLog(@"self.view.bounds.size.height %f",self.view.bounds.size.height);
    //    //self.textViewInternal = (HPTextViewInternal*) self.textView.internalTextView;
    //    //[self.containerView addSubview:self.textViewInternal];
}

//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    NSLog(@"keyboardWillShow");
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    
    
    
    
	// get a rect for the textView frame
	CGRect containerFrame = self.containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	
	// set views with new info
	self.containerView.frame = containerFrame;
    float navBarSize = self.navigationController.navigationBar.frame.size.height+self.navigationController.navigationBar.frame.origin.y;
    
    // maxHeight is whole height less nav bar and keyboard
    self.textView.maxHeight= keyboardBounds.origin.y - navBarSize - 3.0*5.0f - 52; // 5 is content inset top and bottom 52 is height of email entry bar
    
    
    
	// commit animations
	[UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSLog(@"keyboardWillHide");
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
	// get a rect for the textView frame
	CGRect containerFrame = self.containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
	
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
	self.containerView.frame = containerFrame;
    
	// commit animations
	[UIView commitAnimations];
}

- (void)growingTextView:(QResizingTextView *)growingTextView willChangeHeight:(float)height
{
    NSLog(@"growingTextView %f",growingTextView.frame.size.height);
    float diff = (growingTextView.frame.size.height - height);
    NSLog(@"diff %f",diff);
	CGRect r = self.containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	self.containerView.frame = r;
    
    
}


-(void)resignTextView
{
	[self.textView resignFirstResponder];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/tmp_bz.bzwx",documentsDirectory];
    
    //CRYPTO
    
    //save content to the document directory
    [self.textView.internalTextView.text writeToFile:fileName
                                          atomically:NO
                                            encoding:NSStringEncodingConversionAllowLossy
                                               error:nil];
    NSURL *qMessageURL = [[NSURL alloc] initFileURLWithPath:fileName];
    [QEmailHelpers sendNSURLtoEmailAddress:qMessageURL emailAddress:self.emailAddress.text];
     
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
