//
//  QComposeViewController.h
//  firstTry
//
//  Created by Admin on 24/09/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QResizingTextView.h"

@interface QComposeViewController : UIViewController<QResizingTextViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *containerView;


-(void)resignTextView;
@end
