//
//  NSMutableArray+Queue.m
//  quis
//
//  Created by Will Skinner on 12/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "NSMutableArray+Queue.h"

@implementation NSMutableArray (Queue)
- (id)deque {
    id el = [self lastObject];
    [self removeLastObject];
    return el;
}

- (void)enqueue:(id)item {
    [self insertObject:item atIndex:0];
}

@end
