//
//  QConversationListViewController.h
//  quis
//
//  Created by Admin on 07/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QConversationListViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *conversation;
@property (nonatomic, strong) NSMutableArray *conversationItems;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
