//
//  QKeyPair.m
//  quis
//
//  Created by Will Skinner on 11/18/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QKeyPair.h"

@implementation QKeyPair

- (id)init {
    self = [super init];
    if (self) {
        [self generateKeyPair];
    }
    return self;
}

- (id) initWithEncryptionKeyPublic:(NSData *)eKPu encryptionKeyPrivate:(NSData *)eKPr signingKeyPublic:(NSData *)sKPu signingKeyPrivate:(NSData *)skPr {
    self = [super init];
    if (self) {
        if (eKPu && eKPu.length == crypto_box_PUBLICKEYBYTES) {
            memcpy(encryption_key_public, [eKPu bytes], crypto_box_PUBLICKEYBYTES);
        }
        
        if (eKPr && eKPr.length == crypto_box_SECRETKEYBYTES) {
            memcpy(encryption_key_private, [eKPr bytes], crypto_box_SECRETKEYBYTES);
        }
        
        if (sKPu && sKPu.length == crypto_sign_PUBLICKEYBYTES) {
            memcpy(signing_key_public, [sKPu bytes], crypto_sign_PUBLICKEYBYTES);
        }
        
        if (skPr && skPr.length == crypto_sign_SECRETKEYBYTES) {
            memcpy(signing_key_private, [skPr bytes], crypto_sign_SECRETKEYBYTES);
        }
    }
    return self;
}


- (void)generateKeyPair {
    crypto_box_keypair(encryption_key_public,encryption_key_private);
    crypto_sign_keypair(signing_key_public, signing_key_private);
}

- (NSData *)encryptData:(NSData *)data nonce:(NSData *)nonce forKeyPair:(QKeyPair *)receiver {
    
    // nonce must be the right length
    if (nonce.length != crypto_box_NONCEBYTES) {
        NSLog(@"nonce was %lu bytes, but should have been %u bytes", (unsigned long)nonce.length, crypto_box_NONCEBYTES);
        return nil;
    }
    
    unsigned char *n = malloc(crypto_box_NONCEBYTES * sizeof(unsigned char));

    unsigned char *nonce_bytes = (unsigned char *)[nonce bytes];
    
    // copy the nonce bytes
    memcpy(n, nonce_bytes, crypto_box_NONCEBYTES);
    
    // copy the actual data into an array
    char *data_bytes = malloc(data.length * sizeof(unsigned char));
    [data getBytes:data_bytes length:data.length];
    
    unsigned long mlen = data.length + crypto_box_ZEROBYTES;
    char *m = malloc(mlen * sizeof(unsigned char));
    unsigned char *c = malloc(mlen * sizeof(unsigned char));
    
    // copy over the zerobytes
    memset(m, 0, crypto_box_ZEROBYTES);
    
    // copy over the message
    memcpy(m + crypto_box_ZEROBYTES, data_bytes, data.length);
    
    // encrypt
    int status = crypto_box(c,(const unsigned char *)m,mlen,n,[[receiver encryptionKeyPublic] bytes],encryption_key_private);
    NSData *result;
    if (status != 0) {
        result = nil;
    } else {
        result = [[NSData alloc] initWithBytes:c length:mlen];
    }
    free(n);
    free(data_bytes);
    free(m);
    free(c);
    return result;
}

- (NSData *)decryptData:(NSData *)data nonce:(NSData *)nonce fromKeyPair:(QKeyPair *)sender {
    // nonce must be the right length
    if (nonce.length != crypto_box_NONCEBYTES) {
        NSLog(@"nonce was %lu bytes, but should have been %u bytes", (unsigned long)nonce.length, crypto_box_NONCEBYTES);
        return nil;
    }
    
    unsigned char *n = malloc(crypto_box_NONCEBYTES * sizeof(unsigned char));
    
    unsigned char *nonce_bytes = (unsigned char *)[nonce bytes];
    
    // copy the nonce bytes
    memcpy(n, nonce_bytes, crypto_box_NONCEBYTES);
    
    // copy the actual data into an array
    char *data_bytes = malloc(data.length*sizeof(unsigned char));
    [data getBytes:data_bytes length:data.length];
    
    // allocate the plaintext buffer
    unsigned char *p = malloc(data.length * sizeof(unsigned char));
    
    unsigned long long mlen = data.length;

    // decrypt
    int status = crypto_box_open(p,(const unsigned char *)data_bytes,mlen,n,[[sender encryptionKeyPublic] bytes],encryption_key_private);
    NSData *result;
    if (status != 0) {
        result = nil;
    } else {
        unsigned char *result_data = malloc(mlen-crypto_box_ZEROBYTES * sizeof(unsigned char));
        for (int i = 0; i<mlen-crypto_box_ZEROBYTES; i++) {
            result_data[i] = p[crypto_box_ZEROBYTES + i];
        }
        result = [[NSData alloc] initWithBytes:(const void *)result_data length:mlen-crypto_box_ZEROBYTES];
        free(result_data);
    }
    free(n);
    free(data_bytes);
    free(p);
    return result;
}

- (NSData *)signData:(NSData *)data {
    unsigned char *m = malloc(data.length * sizeof(unsigned char));
    memcpy(m, [data bytes], data.length);
    unsigned long long mlen = data.length;
    unsigned char sm[mlen + crypto_sign_BYTES];
    unsigned long long smlen;
    int err = crypto_sign(sm,&smlen,m,mlen,signing_key_private);
    free(m);
    
    if (err == 0) {
        NSData *result = [NSData dataWithBytes:sm length:smlen];
        
        return result;
    }
    return nil;
}

- (NSData *)verifySignedData:(NSData *)data {
    unsigned char *sm = malloc(data.length *sizeof(unsigned char));
    memcpy(sm, data.bytes, data.length);
    unsigned long long smlen = data.length;
    unsigned char *m = malloc(smlen * sizeof(unsigned char));
    unsigned long long mlen;
    int err = crypto_sign_open(m,&mlen,sm,smlen,signing_key_public);
    free(sm);
    free(m);
    if (err == -1) {
        return NULL;
    }
    NSData *result = [NSData dataWithBytes:m length:mlen];
    return result;
}

- (NSData *)encryptionKeyPublic {
    NSData *key = [[NSData alloc] initWithBytes:encryption_key_public length:crypto_box_PUBLICKEYBYTES];
    
    return key;
}

- (NSData *)encryptionKeyPrivate {
    NSData *key = [[NSData alloc] initWithBytes:encryption_key_private length:crypto_box_SECRETKEYBYTES];
    
    return key;
}

- (NSData *)signingKeyPublic {
    NSData *key = [[NSData alloc] initWithBytes:signing_key_public length:crypto_sign_PUBLICKEYBYTES];
    
    return key;
}

- (NSData *)signingKeyPrivate {
    NSData *key = [[NSData alloc] initWithBytes:signing_key_private length:crypto_sign_SECRETKEYBYTES];
    
    return key;
}

+ (NSData *)nextNonce {
    char *nonce = malloc(crypto_box_NONCEBYTES * sizeof(char));
    randombytes_buf(nonce, crypto_box_NONCEBYTES);
    NSData *n = [[NSData alloc] initWithBytes:nonce length:crypto_box_NONCEBYTES];
    free(nonce);
    return n;
    
}


@end
