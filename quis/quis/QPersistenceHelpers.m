//
//  QPersistenceHelpers.m
//  quis
//
//  Created by Admin on 16/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QPersistenceHelpers.h"
#import "QMessage.h"

@implementation QPersistenceHelpers
#pragma mark look for messageUID

+(bool) queryMessageUID:(NSNumber*) uid {
    QAppDelegate* delegate= [[UIApplication sharedApplication] delegate];
    
    // use query template better perf
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UID" inManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"uid",uid];
    NSLog(@"Query %@", predicate);
    [fetchRequest setPredicate:predicate];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections"
    NSError* error;
    NSArray *array = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (array == nil)
        
    {
        // Deal with error...
        return NO;
    }
    
    if ([array count] > 0) {
        NSLog(@"Cache hit!");
        return YES;
    }
    return NO;
}

#pragma mark put the uid in core data

+(void) insertUID:(NSNumber*) uid {
    
    QAppDelegate* delegate= [[UIApplication sharedApplication] delegate];
    UID* newUID = (UID*) [NSEntityDescription insertNewObjectForEntityForName:@"UID"inManagedObjectContext:delegate.managedObjectContext];
    newUID.uid = uid;
    NSError *saveError;
    if (![delegate.managedObjectContext save:&saveError]) {
        NSLog(@"Message save failed: %@", [saveError localizedDescription]);
    }
    
}


#pragma mark query sender email
+ (SecQContacts*) queryEmailAddress:(NSString*) queryAddress {
    QAppDelegate* delegate= [[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SecQContacts" inManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    //NSDictionary *variables = @{ @"EMAIL_ADDRESS" :queryAddress};
    //NSPredicate *predicate = [[self predicateTemplate] predicateWithSubstitutionVariables:variables];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"emailAddress",queryAddress];
    
    NSLog(@"Query %@",predicate);
    
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects == nil) {
        
        // Handle the error.
        NSLog(@"Error in query %@",predicate);
        
    }
    
    NSLog(@"Query returned %d results",[fetchedObjects count]);
    
    if ([fetchedObjects count] == 0) return nil;
    return [fetchedObjects objectAtIndex:0];
}

#pragma mark save the message

+(void) insertMessage {
    
}

#pragma mark Active

+ (void) refreshQuis:(UIRefreshControl*) refreshControl {
    QAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"updateEmailDataBase" object:self];
    NSLog(@"Checking for new messages");
    
    delegate.session.connectionLogger = ^(void * connectionID, MCOConnectionLogType type, NSData * data) {
        NSLog(@"data: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    };
    
    MCOIMAPMessagesRequestKind requestKind = MCOIMAPMessagesRequestKindUid|MCOIMAPMessagesRequestKindStructure|MCOIMAPMessagesRequestKindHeaders;
    NSString *folder = @"INBOX";
    //NSString *folder = @"[Gmail]/Spam";
    //NSString *folder = @"CAPSTONE";
    
    
    MCOIMAPFolderInfoOperation *folderInfo = [delegate.session folderInfoOperation:folder];
    NSLog(@"just before fetch"); // need to lock against other refresh attempts
    
    [folderInfo start:^(NSError *error, MCOIMAPFolderInfo *info) {
        if (!error) {
            NSLog(@"!error");
            // this block is on the main thread
            int numberOfMessages = 100; // this wont work if we get more than 100 since last fetch
            if (info.messageCount < numberOfMessages) {
                numberOfMessages = info.messageCount;
            }
            numberOfMessages -= 1;
            NSLog(@"retrieving %d of %d", numberOfMessages, info.messageCount);
            if (numberOfMessages > 0) {
                MCOIndexSet *numbers = [MCOIndexSet indexSetWithRange:MCORangeMake([info messageCount] - numberOfMessages, numberOfMessages)];
                
                MCOIMAPFetchMessagesOperation *fetchOperation = [delegate.session fetchMessagesByNumberOperationWithFolder:folder requestKind:requestKind numbers:numbers];
                
                
                [fetchOperation start:^(NSError *error, NSArray *fetchedMessages, MCOIndexSet *vanishedMessages) {
                    // this block is on the ------------main thread
                    if (!error) {
                        for (MCOIMAPMessage *currentMessage in fetchedMessages) {
                            // we do not need to lock the DB we are on the main thread
                            // this does not add message
                            if ([QPersistenceHelpers queryMessageUID:[NSNumber numberWithInt:currentMessage.uid]]) {
                                continue;
                            }
                            
                            SecQContacts* sender = [QPersistenceHelpers queryEmailAddress:currentMessage.header.from.mailbox];
                            
                            if (!sender) {
                                // sender is not a contact so ignore
                                [QPersistenceHelpers insertUID:[NSNumber numberWithInt: currentMessage.uid]];
                                continue;
                            }
                            // we will have a context for each message and only one thread at a time uses the context
                            
                            NSLog(@"currentMessage %@",currentMessage);
                            NSLog(@"currentMessage attachments %@",currentMessage.attachments);
                            MCOIMAPPart* quisAttachment = nil;
                            for (MCOIMAPPart* attachment in currentMessage.attachments) {
                                NSLog(@"Mimetype %@",attachment.mimeType);
                                NSLog(@"partID %@",attachment.partID);
                                if ([attachment.mimeType isEqualToString: @"application/BZWX"] || [attachment.mimeType isEqualToString: @"application/bzwx"]) {
                                    // put somewhere
                                    
                                    NSLog(@"Found a beeswax! %@",attachment);
                                    NSLog(@"attachment partID %@",attachment.uniqueID);
                                    // put where we fetch the attachment elsewhere not size also
                                    
                                    quisAttachment = attachment;
                                    break;
                                }
                            }
                            if (quisAttachment) {
                                // put in fetch queue - just in memory is ok? will see UID in next refresh
                                [delegate.uidCache setObject:@[sender, currentMessage, quisAttachment] forKey:[NSNumber numberWithInt:currentMessage.uid]];
                            } else {
                                // put UID in database will not have to consider again
                                [QPersistenceHelpers insertUID:[NSNumber numberWithInt: currentMessage.uid]];
                            }
                        } // for messages
                        NSLog(@"Done iterating over the messages");
                    } // error
                    else {
                        NSLog(@"Error %@",error);
                    }
                    
                    
                    // examining all message is finished. we have a list of uids, partID, size we need to retreive
                    [refreshControl endRefreshing];
                    
                    NSLog(@"Messages to be retrieved %@",delegate.uidCache);
                    if ([delegate.uidCache count] > 0) {
                        [QPersistenceHelpers fetchMessages];
                    }
                }]; //fetch
                
            } else {
                //numberOfMessages < = 0
                [refreshControl endRefreshing];
                if ([delegate.uidCache count]>0) {
                    [QPersistenceHelpers fetchMessages];
                }
            }
        } else {
            NSLog(@"Error: %@", error);
            [refreshControl endRefreshing];
        }
    }];
    
}

+ (SecQContacts *)getDqc {
    QAppDelegate* delegate= [[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SecQContacts" inManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    //NSDictionary *variables = @{ @"EMAIL_ADDRESS" :queryAddress};
    //NSPredicate *predicate = [[self predicateTemplate] predicateWithSubstitutionVariables:variables];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"status",@"DQC"];
    
    NSLog(@"Query %@",predicate);
    
    [fetchRequest setPredicate:predicate];
    
    
    NSError *error;
    NSArray *fetchedObjects = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects == nil) {
        
        // Handle the error.
        NSLog(@"Error in query %@",predicate);
        
    }
    
    NSLog(@"Query returned %d results",[fetchedObjects count]);
    
    if ([fetchedObjects count] == 0) return nil;
    return [fetchedObjects objectAtIndex:0];
}


+ (void) fetchMessages {
    
    QAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    NSArray *KeysForL = [delegate.uidCache allKeys];
    
    for(NSNumber* uid in KeysForL) {
        
        if (!delegate.uidCache[uid]) {
            continue;
        }
        
        SecQContacts *sender = [delegate.uidCache[uid] objectAtIndex:0];
        MCOIMAPMessage *currentMessage = [delegate.uidCache[uid] objectAtIndex:1];
        MCOIMAPPart *attachment = [delegate.uidCache[uid] objectAtIndex:2];
        
        MCOIMAPFetchContentOperation *op = [delegate.session fetchMessageAttachmentByUIDOperationWithFolder:@"INBOX" uid:[uid intValue]  partID:attachment.partID encoding:attachment.encoding];
        [op start:^(NSError * error, NSData * partData) {
            if (!error) {
                // ----------------main thread-------------------
                NSLog(@"data: %@",[[NSString alloc] initWithData:partData encoding:NSUTF8StringEncoding]);
                // put message in db
                // what to do if we get interrupted here?
                SecQMessage *newMessage = (SecQMessage*) [NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage"inManagedObjectContext:delegate.managedObjectContext];
                
                [newMessage initializeFromBase64Blob:partData];
                [newMessage initializeForRecipient:[QPersistenceHelpers getDqc] fromSender:sender];
                [newMessage decrypt];
                
                newMessage.messageId = uid;
                newMessage.date = currentMessage.header.receivedDate;
                
                sender.mostRecentMessageDate = currentMessage.header.receivedDate;
                [sender addQMessagesObject:(SecQMessage *) newMessage];
                NSError *saveError;
                if (![delegate.managedObjectContext save:&saveError]) {
                    NSLog(@"Message save failed: %@", [saveError localizedDescription]);
                } else {
                    // we do not need to check this uid again
                    [QPersistenceHelpers insertUID:[NSNumber numberWithInt: currentMessage.uid]];
                    // remove form cache
                    [delegate.uidCache removeObjectForKey:uid];
                }
            } // not error
        }];
#pragma mark end download
    }
}

+ (NSManagedObjectContext *)getManagedObjectContext {
    QAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    return delegate.managedObjectContext;
}


@end
