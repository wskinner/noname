//
//  QAppDelegate.h
//  quis
//
//  Created by Admin on 04/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNetHelper.h"


@interface QAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) NSMutableDictionary* uidCache;
@property (nonatomic, strong) MCOIMAPSession* session;

@property (strong, nonatomic) QNetHelper* qNetHelper;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
