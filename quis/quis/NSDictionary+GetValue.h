//
//  NSDictionary+GetValue.h
//  quis
//
//  Created by Will Skinner on 12/8/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (GetValue)
- (NSString *)getValueOrEmptyString:(NSString *)key;
@end
