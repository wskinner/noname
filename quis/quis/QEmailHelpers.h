//
//  QEmailHelpers.h
//  quis
//
//  Created by Admin on 12/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MailCore/MailCore.h>

@interface QEmailHelpers : NSObject

+(MCOIMAPSession*) session;
+(void) sendNSURLtoEmailAddress:(NSURL*) nsurlPath emailAddress:(NSString*) address;

@end
