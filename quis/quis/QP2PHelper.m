//
//  QP2PHelper.m
//  quis
//
//  Created by Admin on 21/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

//#import "QP2PHelper.h"
//#import "KeyExchange.h"
//
//@interface QP2PHelper () <MCNearbyServiceBrowserDelegate,MCNearbyServiceAdvertiserDelegate,UIAlertViewDelegate>
//
//@property (nonatomic, copy) void (^handler)(BOOL accept, MCSession *session);
//
//@property (strong,nonatomic) KeyExchange* keyExchanger;
//
//@end
//
//@implementation QP2PHelper
//
//-(id) init {
//    self = [super init];
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    self.userEmail = [defaults objectForKey:@"username_preference"];
//    self.userEmailPeerID = [[MCPeerID alloc] initWithDisplayName:self.userEmail];
//    
//    self.serviceBrowser = nil;
//    self.serviceAdvertiser = nil;
//    
//    self.sessionLocal = [[QSessionLocal alloc] initWithPeer:self.userEmailPeerID];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startAdvertising) name:@"startAdvertising" object:nil];
//    
//    self.keyExchanger = [[KeyExchange alloc] init];
//
//    return self;
//}
//
//#pragma methods for external classes
//
//-(void) disconnectSession {
//    if (self.sessionLocal) [self.sessionLocal.session disconnect];
//}
//
//-(void) stopBrowsingForPeers {
//    if (self.serviceBrowser) [self.serviceBrowser stopBrowsingForPeers];
//}
//
//-(void) stopAdvertisingPeer {
//    if (self.serviceAdvertiser) [self.serviceAdvertiser stopAdvertisingPeer];
//}
//
//-(void) destroySession {
////    dispatch_async(dispatch_get_main_queue(),^{
////        self.sessionLocal = nil;
////    });
//}
//
//#pragma mark advert
//
//-(void) startAdvertising {
//    
//    if ([self.sessionLocal.session.connectedPeers count] >0) {
//        NSLog(@"WARN:Connected peer needs to be resolved");        
//    }
//    
//    if (self.serviceAdvertiser || self.serviceBrowser) {
//        NSLog(@"WARN:Advertiser , browser non nil - old session still active");
//        return;
//    }
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
//    NSString *strDate = [dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:10.0f]];
//    NSLog(@"Creating advertiser with %@",strDate);
//    self.discoveryInfo = [[NSDictionary alloc] initWithObjects:@[self.userEmail,strDate] forKeys:@[@"advertiserID",@"TTL"]];
//    
//
//    
//    self.serviceAdvertiser = [[MCNearbyServiceAdvertiser alloc] initWithPeer:self.userEmailPeerID discoveryInfo:self.discoveryInfo serviceType:@"quis"];
//    self.serviceAdvertiser.delegate = self;
//    
//    self.serviceBrowser = [[MCNearbyServiceBrowser alloc] initWithPeer:self.userEmailPeerID serviceType:@"quis"];
//    self.serviceBrowser.delegate = self;
//    
//    [self.serviceBrowser startBrowsingForPeers];
//    [self.serviceAdvertiser startAdvertisingPeer];
//    
//    NSLog(@"session info %@",self.sessionLocal.session);
//
//    [NSTimer scheduledTimerWithTimeInterval:11.0
//                                     target:self
//                                   selector:@selector(stopAdvertising)
//                                   userInfo:nil
//                                    repeats:NO];
// 
//}
//
//-(void) stopAdvertising {
//    NSLog(@"Destroying self.serviceAdvertiser");
//    [self stopAdvertisingPeer];
//    self.serviceAdvertiser = nil;
//    
//    NSLog(@"Destroying self.serviceBrowser");
//    [self stopBrowsingForPeers];
//    self.serviceBrowser = nil;
//    
////    if (self.sessionLocal) {
////        NSLog(@"connectedPeers = %lu",(unsigned long)[self.sessionLocal.session.connectedPeers count]);
////        if([self.sessionLocal.session.connectedPeers count] == 0) {
////            NSLog(@"Destroying self.sessionLocal");
////            dispatch_async(dispatch_get_main_queue(),^{
////                if (self.sessionLocal.session) [self.sessionLocal.session disconnect];
////                //self.sessionLocal = nil;
////            });
////        }
////    }
//}
//
//#pragma mark MCNearbyServiceBrowser delegate methods
//
//- (void)browser:(MCNearbyServiceBrowser *)browser didNotStartBrowsingForPeers:(NSError *)error {
//    NSLog(@"didNotStartBrowsingForPeers Error %@",error);
//}
//
//
//- (void)browser:(MCNearbyServiceBrowser *)browser foundPeer:(MCPeerID *)peerID withDiscoveryInfo:(NSDictionary *)info {
//    //NSLog(@"Found peer %@ TTL %@",[info objectForKey:@"advertiserID"],[info objectForKey:@"TTL"]);
//    
//    if ([[info objectForKey:@"advertiserID"] isEqualToString:self.userEmail]) {
//        NSLog(@"Found ourself");
//        return; // this is our own advert
//    }
//
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"foundPeer" object:self userInfo:@{@"PeerID" : peerID, @"TTL":[info objectForKey:@"TTL"]}];
//    
//}
//
//- (void)browser:(MCNearbyServiceBrowser *)browser lostPeer:(MCPeerID *)peerID {
//    NSLog(@"Lost Peer");
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"lostPeer" object:self userInfo:@{@"PeerID" : peerID}];
//    
//    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lost Booty" message:@"No booty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    //    [alert show];
//}
//
//#pragma mark MCNearbyServiceAdvertiserDelegate methods
//
//- (void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didNotStartAdvertisingPeer:(NSError *)error {
//    NSLog(@"MCNearbyServiceAdvertiser Error %@",error);
//}
//
//- (void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didReceiveInvitationFromPeer:(MCPeerID *)peerID withContext:(NSData *)context invitationHandler:(void (^)(BOOL accept, MCSession *session))invitationHandler {
//    
//    NSLog(@"Received invite from %@",peerID.displayName);
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contact Exchange" message:peerID.displayName delegate:self cancelButtonTitle:@"Decline" otherButtonTitles:@"Accept",nil];
//    [alert show];
//    NSLog(@"After alert");
//    self.handler = invitationHandler;
//
//}
//
//#pragma mark aletrview delegate
//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    NSLog(@"Button Index %d",buttonIndex);
//    if (buttonIndex == 0) {
//        NSLog(@"Declining Invitation");
//        self.handler(NO,self.sessionLocal.session);
//    } else {
//        NSLog(@"Accepting Invitation");
//        self.handler(YES,self.sessionLocal.session);
//    }
//}
//
//@end
