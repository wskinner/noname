//
//  QConnection.h
//  quis
//
//  Created by Admin on 27/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SecQMessage.h"
#import "QNetHelper.h"
#import "QKeyExchange.h"
#import "KeyExchangeDelegate.h"
#import "QKeyExchangeViewDelegate.h"

@interface QConnection : NSObject


@property (nonatomic, strong, readonly ) NSInputStream *    inputStream;
@property (nonatomic, strong, readonly ) NSOutputStream *   outputStream;

@property (nonatomic, strong) id <KeyExchangeDelegate> keyXDelegate;


- (id)initWithInputStream:(NSInputStream *)inputStream outputStream:(NSOutputStream *)outputStream;

- (BOOL)open;
- (void)close;

extern NSString * QConnectionDidCloseNotification;
// This notification is posted when the connection closes, either because you called
// -close or because of on-the-wire events (the client closing the connection, a network
// error, and so on).
@end
