//
//  QKeyExchangeViewController.h
//  quis
//
//  Created by Will Skinner on 11/24/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QKeyExchangeViewDelegate.h"

@interface QKeyExchangeViewController : UIViewController  <QKeyExchangeViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *theirCode;
@property (strong, nonatomic) IBOutlet UILabel *myCode;

- (IBAction)noButton:(id)sender;
- (IBAction)yesButton:(id)sender;
@end
