//
//  QKeyExchangeViewController.m
//  quis
//
//  Created by Will Skinner on 11/24/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QKeyExchangeViewController.h"

@interface QKeyExchangeViewController ()

@end

@implementation QKeyExchangeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)noButton:(id)sender {
}

- (IBAction)yesButton:(id)sender {
}
@end
