//
//  Retryer.h
//  quis
//
//  Created by Will Skinner on 12/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Retryer : NSObject

@property (retain) id <KeyExchangeDelegate> parent;

- (void)notifyMessageSent:(SecQMessage *)msg;
- (void)notifyAckReceived:(NSUInteger)msgId;

@end
