//
//  QMessageNavigationViewController.m
//  quis
//
//  Created by Admin on 05/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QMessageNavigationViewController.h"
#import "QTabBarController.h"
@interface QMessageNavigationViewController ()

@end

@implementation QMessageNavigationViewController
@synthesize managedObjectContext = _managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    QTabBarController* parentView = (QTabBarController*)  self.parentViewController;
    self.managedObjectContext =  parentView.managedObjectContext;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
