////
////  QP2PTableViewController.m
////  quis
////
////  Created by Admin on 22/10/2013.
////  Copyright (c) 2013 Admin. All rights reserved.
////
//
//#import "QP2PTableViewController.h"
//#import "QConnectionViewController.h"
//
//@interface QP2PTableViewController ()
//
//@property (strong, nonatomic) MCPeerID* selectedPeerID;
//
//@property (strong,nonatomic) QP2PHelper* p2pHelper;
//
//@property (nonatomic, strong) NSMutableArray *nearbyPeers;
//@property (strong, nonatomic) IBOutlet UITableView *peersTable;
//
//@property (strong, nonatomic) NSMutableDictionary* TTLDict;
//
//
//@property (nonatomic, strong) UIRefreshControl *refreshControl;
//-(void) refreshPeers;
//@end
//
//@implementation QP2PTableViewController
//
//- (id)initWithStyle:(UITableViewStyle)style
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//
//    // Uncomment the following line to preserve selection between presentations.
//    // self.clearsSelectionOnViewWillAppear = NO;
// 
//    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//    
//	// Do any additional setup after loading the view.
//    
//
//    QAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
//    self.p2pHelper= delegate.p2pHelper;
//                     
//    //self.userEmail = self.p2pHelper.userEmail;
//    //self.userEmailPeerID = self.p2pHelper.userEmailPeerID;
//    
//    SEL refreshQuis = @selector(refreshPeers);
//    
//    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
//                                        init];
//    //refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
//    refreshControl.tintColor = [UIColor blueColor];
//    [refreshControl addTarget:self action:refreshQuis forControlEvents:UIControlEventValueChanged];
//    self.refreshControl = refreshControl;
//    
//    
//    self.nearbyPeers = [[NSMutableArray alloc] init];
//    self.TTLDict = [[NSMutableDictionary alloc] init];
//
//}
//
//-(void) viewWillAppear:(BOOL)animated {
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(peersDidAdd:) name:@"foundPeer" object:nil];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(peersDidLose:) name:@"lostPeer" object:nil];
//}
//
//-(void) viewWillDisappear:(BOOL)animated {
//    
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"foundPeer" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"lostPeer" object:nil];
//    
//    if  ([self.navigationController.viewControllers  indexOfObject:self]==NSNotFound) {
//        //clean up here
//        NSLog(@"Back button pressed");
//
//        NSLog(@"stopBrowsingForPeers");
//        [self.p2pHelper stopBrowsingForPeers];
//
//        
//        NSLog(@"stop Advestising");
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"stopAdvertising" object:self];
//    }
//
//}
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//#pragma mark new peer
//
//-(void) peersDidAdd:(NSNotification*) note {
//    NSLog(@"Peer found %@ TTL %@",note.userInfo[@"PeerID"],note.userInfo[@"TTL"]);
//    
//    //NSLog(@"Peer found %@",note.userInfo[@"PeerID"]);
//    
//    [self.nearbyPeers addObject:note.userInfo[@"PeerID"]];
//    
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
//    
//    NSDate* now = [NSDate date];
//    NSDate* TTL = [dateFormatter dateFromString:note.userInfo[@"TTL"]];
//    
//    NSTimeInterval interval =[TTL timeIntervalSinceDate:now];
//    
//    NSLog(@"Adding peer with life %f s",interval);
//    
//    if (interval >0) {
//    
//    [self.TTLDict setObject:TTL forKey:note.userInfo[@"PeerID"]];
//    [NSTimer scheduledTimerWithTimeInterval:interval
//                                     target:self
//                                   selector:@selector(removePeerID:)
//                                   userInfo:note.userInfo[@"PeerID"]
//                                    repeats:NO];
//    [self.peersTable reloadData];
//    } else {
//        NSLog(@"WARN: peer found TTL <0");
//    }
//}
//
//#pragma mark lost peer
//-(void) removePeerID:(NSTimer*) timer {
//    NSLog(@"Peer timimg out %@",timer.userInfo);
//    [self.nearbyPeers removeObject:timer.userInfo];
//    [self.TTLDict removeObjectForKey:timer.userInfo];
//    [self.peersTable reloadData];
//
//}
//-(void) peersDidLose:(NSNotification*) note {
//    NSLog(@"Peer lost %@",note.userInfo[@"PeerID"]);
//    [self.nearbyPeers removeObject:note.userInfo[@"PeerID"]];
//    [self.TTLDict removeObjectForKey:note.userInfo[@"PeerID"]];
//    [self.peersTable reloadData];
//}
//
//#pragma mark refreshQuis
//
//-(void) refreshPeers {
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"startAdvertising" object:self];
//    NSLog(@"refreshState %u",self.refreshControl.refreshing);
//    // TODO put timer and stop refreshing after timer
//    [NSTimer scheduledTimerWithTimeInterval:2.0
//                                     target:self
//                                   selector:@selector(stopRefreshQuis)
//                                   userInfo:nil
//                                    repeats:NO];
//    
//}
//
//-(void) stopRefreshQuis {
//    [self.refreshControl endRefreshing];
//}
//
//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    // Return the number of sections.
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    // Return the number of rows in the section.
//    return [self.nearbyPeers count];
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"P2PCell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    
//    // Configure the cell...
//    cell.textLabel.text = [[self.nearbyPeers objectAtIndex:indexPath.row] displayName];
//    return cell;
//}
//
//#pragma mark 
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//
//{
//    [tableView deselectRowAtIndexPath:indexPath animated:NO];
//
//    self.selectedPeerID = [self.nearbyPeers objectAtIndex:indexPath.row];
//    
//    NSDate* TTL = [self.TTLDict objectForKey:self.selectedPeerID];
//    NSDate* now = [NSDate date];
//    
//    NSLog(@"Checking invite is before TTL:%@ now %@",TTL,now);
//    
//    if ([now compare:TTL] == NSOrderedAscending) {
//        [self performSegueWithIdentifier:@"connect" sender:self];
//    } else {
//        NSLog(@"Not sending invitation to %@ after TTL",self.selectedPeerID);
//    }
//}
//
///*
//// Override to support conditional editing of the table view.
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Return NO if you do not want the specified item to be editable.
//    return YES;
//}
//*/
//
///*
//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        // Delete the row from the data source
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    }   
//    else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//    }   
//}
//*/
//
///*
//// Override to support rearranging the table view.
//- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
//{
//}
//*/
//
///*
//// Override to support conditional rearranging of the table view.
//- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Return NO if you do not want the item to be re-orderable.
//    return YES;
//}
//*/
//
//
//#pragma mark - Navigation
//
//// In a story board-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    QConnectionViewController* vc = [segue destinationViewController];
//    vc.peerID = self.selectedPeerID;
//}
//
//
//@end
