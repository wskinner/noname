//
//  QSessionLocal.m
//  quis
//
//  Created by Admin on 21/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QSessionLocal.h"

@interface QSessionLocal () <MCSessionDelegate>

@end

@implementation QSessionLocal

-(id) initWithPeer: (MCPeerID*) userEmailPeerID {
    self = [super init];
    self.session = [[MCSession alloc] initWithPeer:userEmailPeerID];
    
    self.session.delegate = self;
    
    return self;
}

//Important: Delegate calls occur on a private operation queue. If your app needs to perform an action on a particular run loop or operation queue, its delegate method should explicitly dispatch or schedule that work.
#pragma mark MCSessionDelegate this is for the session we were invited to join

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID {
    NSLog(@"didReceiveData: %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);

    dispatch_async(dispatch_get_main_queue(),^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"peerConnected" object:self userInfo:@{@"peerID":peerID,@"data":data}];
    });
}

- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID {
    
}

- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress {
    
}

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state {
    NSLog(@"didChangeState %d",state);
    NSLog(@"session info %@",self.session);
    if (state == MCSessionStateConnected) {

        NSLog(@"Peer %@ connected to Local session",peerID);
        dispatch_async(dispatch_get_main_queue(),^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"peerConnected" object:self userInfo:@{@"peerID":peerID}];
        });
    } else {

        NSLog(@"Peer %@ disconnected from Local session",peerID);
        dispatch_async(dispatch_get_main_queue(),^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"peerDisconnected" object:self userInfo:@{@"peerID":peerID}];
        });
    }
}

- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error {
    
}
@end