//
//  QNetConnectionViewController.m
//  quis
//
//  Created by Admin on 28/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//
// client

#import "QNetConnectionViewController.h"

@interface QNetConnectionViewController () <NSStreamDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) NSInputStream *inputStream;
@property (strong, nonatomic) NSOutputStream * outputStream;
@property (nonatomic, strong, readwrite) NSMutableData * inputBuffer;
@property (nonatomic, strong, readwrite) NSMutableData * outputBuffer;


@property (strong, nonatomic) IBOutlet UILabel *status;
@property (strong, nonatomic) IBOutlet UILabel *messageRec;

@property (strong, nonatomic) IBOutlet UIButton *connectRequest;


@property (assign,readwrite) BOOL hasReceivedHello;
@property (assign,readwrite) BOOL hasSpaceAvailable;

@end

@implementation QNetConnectionViewController
@synthesize keyXDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
    }
    self.hasReceivedHello = NO;
    self.hasSpaceAvailable = NO;

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"QNetViewController didLoad");
	// Do any additional setup after loading the view.
    if (!self.theirCode) {
        self.theirCode = [[UILabel alloc] init];
        NSLog(@"theirCode was nil");
    }
    if (!self.myCode) {
        self.myCode = [[UILabel alloc] init];
        NSLog(@"myCode was nil");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma cleanup

- (void) viewWillDisappear:(BOOL)animated {
    [self.inputStream close];
    [self.outputStream close];
    self.inputStream = nil;
    self.outputStream = nil;
    self.status.text = @"";
    
}

#pragma test
- (IBAction)connect:(UIButton *)sender {
    if (!self.connectRequest.selected) {
        NSLog(@"Attempting to connect service %@",self.resolvedHost);
        NSLog(@"which resolved to %@:%zu", self.resolvedHost, (size_t) 55777);
        self.connectRequest.selected = YES;
        [self.connectRequest setTitle:@"" forState:UIControlStateSelected];
        [self initNetworkCommunication:(NSString* )self.resolvedHost port:55777];
        
    }
}

// send data
- (IBAction)send:(UIButton *)sender {
    [self sendString:@"Booty"];
}

#pragma comm
- (void)initNetworkCommunication:(NSString *) resolvedHost port:(NSUInteger) resolvedPort {
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    NSLog(@"CFStreamCreatePairWithSocketToHost resolved Host %@, port %lu",resolvedHost,(unsigned long)resolvedPort);
    
    
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)resolvedHost, resolvedPort, &readStream, &writeStream);
    
    NSLog(@"CFStreamCreatePairWithSocketToHost result %@ %@",readStream,writeStream);
    
    self.inputStream = (NSInputStream *)CFBridgingRelease(readStream);
    [self.inputStream setDelegate:self];
    [self.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.inputStream open];
    
    self.outputStream = (NSOutputStream *)CFBridgingRelease(writeStream);
    [self.outputStream setDelegate:self];
    [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.outputStream open];
}

// push some data through the stream
- (void)sendString:(NSString *)string {
    NSData *data = [[NSData alloc] initWithData:[string dataUsingEncoding:NSASCIIStringEncoding]];
    NSLog(@"Stream status %u",self.outputStream.streamStatus);
    [self.outputStream write:[data bytes] maxLength:[data length]];
}

// key exchange code in here
// when done do: something like [[NSNotificationCenter defaultCenter] postNotificationName:@"lostPeer" object:self userInfo:@{@"name" : service.name}];
// to indicate done
- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    NSLog(@"stream event %u", streamEvent);
    assert(theStream == self.inputStream || theStream == self.outputStream);
    
    if (!self.keyXDelegate) {
        QAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        self.keyXDelegate = [[QKeyExchange alloc] initWithOutputStream:self.outputStream inputStream:self.inputStream managedObjectContext:delegate.managedObjectContext serverMode:self.serverMode caller:self];
        NSLog(@"Initialized key exchange delegate with serverMode=%hhd", self.serverMode);
    }
    
	switch (streamEvent) {
            
		case NSStreamEventOpenCompleted:
			NSLog(@"Stream opened");
            self.status.text = @"Opened";
            
			break;
            
            // this is where the data shows up
		case NSStreamEventHasBytesAvailable: {
            [self.keyXDelegate hasBytesAvailable];
        } break;
        
		case NSStreamEventHasSpaceAvailable: {
            [self.keyXDelegate hasSpaceAvailable];
        }
        break;
        
        case NSStreamEventErrorOccurred:
			NSLog(@"Can not connect to the host!");
            self.status.text = @"Can not connect";
            self.connectRequest.selected = NO;
            
            break;
            
		case NSStreamEventEndEncountered:
            NSLog(@"End of stream");
            self.status.text = @"Disconnected";
            self.connectRequest.selected = NO;
            
            break;
            
		default:
			NSLog(@"Unknown event");
	}
}


- (IBAction)yesButton:(id)sender {
    if (self.keyXDelegate) {
        [self.keyXDelegate userConfirmedCode:YES];
    }
    
}

- (IBAction)noButton:(id)sender {
    if (self.keyXDelegate) {
        [self.keyXDelegate userConfirmedCode:NO];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)keyXDone {
    [self.exchangeDoneLabel setText:@"YES"];
    NSLog(@"updated ui");
        
    // pop back to the contacts view
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)putMyCode:(NSString *)code {
    NSLog(@"self.myCode = %@", self.myCode);
    NSLog(@"Set my code to %@", code);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.myCode setText:code];
    });
}

- (void)putTheirCode:(NSString *)code {
    NSLog(@"self.theirCode = %@", self.theirCode);
    NSLog(@"Set their code to %@", code);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.theirCode setText:code];
    });
}


- (NSString *)getTheirCode {
    return self.theirCode.text;
}

- (NSString *)getMyCode {
    return self.myCode.text;
}

@end
