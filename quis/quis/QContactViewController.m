//
//  QContactViewController.m
//  quis
//
//  Created by Admin on 06/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QContactViewController.h"
#import "QContactNavigationViewController.h"
#import "QContacts.h"
#import "QContactComposeViewController.h"

@interface QContactViewController ()
@property (nonatomic, strong) NSMutableArray *QContactsArray;
@property (strong, nonatomic) IBOutlet UITableView *contactsTable;

@end

@implementation QContactViewController

@synthesize managedObjectContext = _managedObjectContext;
@synthesize QContactsArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    QAppDelegate *delegate= [[UIApplication sharedApplication] delegate];
    self.managedObjectContext = delegate.managedObjectContext;

//    QContactNavigationViewController* parentView = (QContactNavigationViewController*)  self.parentViewController;
//    self.managedObjectContext =  parentView.managedObjectContext;
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    // TODO at the moment display DQC
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SecQContacts" inManagedObjectContext: self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error;
    self.QContactsArray = [[NSMutableArray alloc] initWithArray:[self.managedObjectContext executeFetchRequest:fetchRequest error:&error]];
}

-(void) viewWillAppear:(BOOL)animated {
    [self.tabBarController.tabBar setHidden:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(contactDatabaseDidUpdate:)
                                                 name:NSManagedObjectContextDidSaveNotification
                                               object:nil];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SecQContacts" inManagedObjectContext: self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error;
    self.QContactsArray = [[NSMutableArray alloc] initWithArray:[self.managedObjectContext executeFetchRequest:fetchRequest error:&error]];
    [self.contactsTable reloadData];
}


-(void) contactDatabaseDidUpdate:(NSNotification*)saveNotification {
    
    // faulted objects dont fire required notification, so fault them in
    
    for(NSManagedObject *object in [[saveNotification userInfo] objectForKey:NSUpdatedObjectsKey]) {
        [[self.managedObjectContext objectWithID:[object objectID]] willAccessValueForKey:nil];
    }
    
    [self.managedObjectContext mergeChangesFromContextDidSaveNotification:saveNotification];
    
    
    [self.managedObjectContext setStalenessInterval:0];
}


-(void) viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NSManagedObjectContextDidSaveNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [self.QContactsArray count];;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"QCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    SecQContacts *info = [self.QContactsArray objectAtIndex:indexPath.row];
    cell.textLabel.text = info.emailAddress;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (self.QContactsArray.count == 1) {
            NSLog(@"Can't delete the DQC.");
            return;
        }
        [self.contactsTable beginUpdates];
        // Delete the row from the data source
        [self.contactsTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];

        // Delete the contact from core data
        SecQContacts *contact = [self.QContactsArray objectAtIndex:indexPath.row];
        [self.managedObjectContext deleteObject:contact];
        NSError *error;
        [self.managedObjectContext save:&error];
        
        if (error) {
            NSLog(@"There was an error saving the managed object context: %@", error);
        }
        [self.QContactsArray removeObjectAtIndex:indexPath.row];
        [self.contactsTable endUpdates];
        [self.contactsTable reloadData];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"composeFromContact"]) {
        
        NSIndexPath *indexPath = [self.contactsTable indexPathForSelectedRow];
        NSLog(@"Index path %@",indexPath);
        SecQContacts *contact = (SecQContacts*) [self.QContactsArray objectAtIndex:indexPath.row];
        
        QContactComposeViewController* childView = (QContactComposeViewController*) [segue destinationViewController];
        NSLog(@"Setting email for mesaage to %@",contact.emailAddress);
        
        childView.targetEmail = contact.emailAddress;

    }
    
}



@end
