//
//  KeyDerivation.h
//  firstTry
//
//  Created by Will Skinner on 9/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PseudoRandomFunction.h"

@interface KeyDerivationFunction : NSObject
@property PseudoRandomFunction *prf;
@property NSUInteger iterations;
@property NSData *salt;
@property NSUInteger keyLength;

- (id)initWithSalt:(NSData *)salt usingIterations:(NSUInteger)iterations;

- (NSData *)deriveKey:(NSData *)password;
@end
