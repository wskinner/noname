//
//  QBrowser.h
//  quis
//
//  Created by Admin on 26/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QDNSService.h"

@protocol QBrowserDelegate;

#pragma mark * QBrowser

@interface QBrowser : NSObject

- (id)initWithDomain:(NSString *)domain type:(NSString *)type;

@property (copy,   readonly ) NSString * domain;
@property (copy,   readonly ) NSString * type;

@property (assign, readwrite) id <QBrowserDelegate> delegate;

- (void)startBrowse;

- (void)stop;

@end

@protocol QBrowserDelegate <NSObject>


@optional

- (void) browserWillBrowse:(QBrowser *)browser;
// Called before the browser starts browsing.

- (void) browserDidStopBrowse:(QBrowser *)browser;
// Called when a browser stops browsing (except if you call -stop on it).

- (void) browser:(QBrowser *)browser didNotBrowse:(NSError *)error;
// Called when the browser fails to start browsing.  The browser will be stopped
// immediately after this delegate method returns.

- (void) browser:(QBrowser *)browser didAddService:(QDNSService *)service moreComing:(BOOL)moreComing;
// Called when the browser finds a new service.

- (void) browser:(QBrowser *)browser didRemoveService:(QDNSService *)service moreComing:(BOOL)moreComing;
// Called when the browser sees an existing service go away.

@end

