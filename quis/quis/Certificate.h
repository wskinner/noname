//
//  Certificate.h
//  firstTry
//
//  Created by Will Skinner on 9/8/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Required keys:
//  certificateHolderInfo
//      name
//  certificateInfo
//      version
//  keyInfo
//      publicKey
//  signature

#import <Foundation/Foundation.h>
#import "QKeyPair.h"
#import "NSDictionary+JsonDictionary.h"
#import "NSArray+JsonArray.h"
#import "SecQContacts.h"
#import "Base64.h"
#import "CryptoAPI.h"

@class SecQContacts;

@interface Certificate : NSObject
@property (strong, nonatomic) NSDictionary *certificateInfo;
@property (strong, nonatomic) NSDictionary *subjectInfo;
@property (strong, nonatomic) NSDictionary *keyInfo;
@property (strong, nonatomic) NSString *signatureAlgorithm;
@property (strong, nonatomic) NSString *signature;
@property (strong, nonatomic) NSString *certificateVersion;
@property (strong, nonatomic) QKeyPair *keyPair;

- (NSData *)toBlob;
- (NSData *)toBase64Blob;
- (id)initForQContacts:(SecQContacts *)contact;
- (id)initFromBlob:(NSData *)blob;
- (id)intitForSubjectInfo:(NSDictionary *)certificateHolderInfo andCertificateInfo:(NSDictionary *)certificateInfo andKeyInfo:(NSDictionary *)keyInfo andSignatureAlgorithm:(NSString *)signatureAlgorithm;
- (id)initFromBase64Blob:(NSData *)blob;

- (void)sign;
- (BOOL)verifySignature;

- (NSData *)signingKeyPublic;
- (NSData *)encryptionKeyPublic;


@end
