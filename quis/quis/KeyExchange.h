//
//  KeyExchange.h
//  quis
//
//  Created by Will Skinner on 10/20/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QPersistenceHelpers.h"
#import "KeyPair.h"

@import MultipeerConnectivity;

@interface KeyExchange : NSObject
@property (strong, nonatomic)SecQContacts *friend;
@property (strong, nonatomic)SecQContacts *dqc;
@property (strong, nonatomic)NSString *myCode;
@property (strong, nonatomic)NSString *theirCode;


@end
