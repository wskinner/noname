//
//  QConversationViewController.h
//  quis
//
//  Created by Admin on 07/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QResizingTextView.h"
#import "QEmailHelpers.h"
#import <UIKit/UIKit.h>

@interface QConversationViewController : UIViewController <QResizingTextViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *tableViewContainer;
@property (strong, nonatomic) IBOutlet UIView *containerView;

-(void)resignTextView;

@property (nonatomic,strong) NSString* sender;
@property (nonatomic, strong) NSMutableArray *conversationItems;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end