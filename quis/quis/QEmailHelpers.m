//
//  QEmailHelpers.m
//  quis
//
//  Created by Admin on 12/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QEmailHelpers.h"

@implementation QEmailHelpers

+(void) sendNSURLtoEmailAddress:(NSURL*)nsurlPath emailAddress:(NSString*)address {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *email = [defaults stringForKey:@"username_preference"];
    
    
    MCOMailProvider *accountProvider = [[MCOMailProvidersManager sharedManager] providerForEmail:email];
    if (!accountProvider) {
        NSLog(@"No provider available for email: %@", email);
        return;
    }
    NSLog(@"accountProvider %@",accountProvider);
    
    //Check if the account provides you with SMTP services
    NSArray *smtpServices = accountProvider.smtpServices;
    MCOSMTPSession *smtpSession = [[MCOSMTPSession alloc] init];
    
    if (smtpServices.count != 0) {
        
        MCONetService *smtpService = smtpServices[0];
        
        [smtpSession setHostname:smtpService.hostname];
        [smtpSession setPort:smtpService.port];
        [smtpSession setUsername:email];
        [smtpSession setPassword:[defaults stringForKey:@"password_preference"]];
        [smtpSession setConnectionType:smtpService.connectionType];
        
        NSLog(@"hostname:%@",smtpSession.hostname);
        NSLog(@"port:%u",smtpSession.port);
        NSLog(@"email:%@",smtpSession.username);
        NSLog(@"connectionType:%u",smtpSession.connectionType);
    } else {
        return;
    }
    
    NSLog(@"Building message.");
    
    MCOMessageBuilder *builder = [[MCOMessageBuilder alloc] init];
    
    [[builder header] setFrom:[MCOAddress addressWithDisplayName:nil mailbox:smtpSession.username]];
    NSMutableArray *to = [[NSMutableArray alloc] init];

    MCOAddress *newAddress = [MCOAddress addressWithMailbox:address];
    [to addObject:newAddress];
 
    [[builder header] setTo:to];
    [[builder header] setSubject:@"quis"];
    //[builder setHTMLBody:@"Body"];

    NSString *pathToFile = nsurlPath.path;
    // this will crash if the file is not found
    MCOAttachment* attachment = [MCOAttachment attachmentWithContentsOfFile:pathToFile];
    [attachment setMimeType:@"application/bzwx"];
    [builder addAttachment:attachment];
    
    NSData * rfc822Data = [builder data];
    
    MCOSMTPSendOperation *sendOperation = [smtpSession sendOperationWithData:rfc822Data];
    [sendOperation start:^(NSError *error) {
        if(error) {
            NSLog(@"%@ Error sending email:%@", smtpSession.username, error);
        } else {
            NSLog(@"%@ Successfully sent email!", smtpSession.username);
            // move this to the email controller and update and add QContact in syn state
        }
    }];
}

+(MCOIMAPSession*) session {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *email = [defaults stringForKey:@"username_preference"];
    MCOMailProvider* accountProvider = [[MCOMailProvidersManager sharedManager] providerForEmail:email];
    if (!accountProvider) {
        NSLog(@"No provider available for email: %@", email);
        return nil;
    }
    
    NSLog(@"accountProvider %@",accountProvider);    // Get user preference
    
    //self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing..."];
    //Check if the account provides you with IMAP services
    NSArray *imapServices = accountProvider.imapServices;
    
    MCOIMAPSession* session = [[MCOIMAPSession alloc] init];
    
    if (imapServices.count != 0) {
        
        MCONetService *imapService = imapServices[0];
        
        [session setHostname:imapService.hostname];
        [session setPort:imapService.port];
        [session setUsername:email];
        [session setPassword:[defaults stringForKey:@"password_preference"]];
        [session setConnectionType:imapService.connectionType];
        //[session setTimeout:(NSTimeInterval) 60.0];
        
        NSLog(@"hostname:%@",session.hostname);
        NSLog(@"port:%u",session.port);
        NSLog(@"email:%@",session.username);
        NSLog(@"connectionType:%u",session.connectionType);
        
    } else {
        return nil;
    }
    
    NSLog(@"defaults %@",session.username);
    return session;
}


@end
