//
//  PseudoRandomFunction.h
//  firstTry
//
//  Created by Will Skinner on 9/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PseudoRandomFunction : NSObject

- (NSData *)computeHash:(NSData *)data withKey:(NSData *)key;
@end
