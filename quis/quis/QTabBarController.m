//
//  QTabBarController.m
//  quis
//
//  Created by Admin on 05/10/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "QTabBarController.h"


@interface QTabBarController ()

@end

@implementation QTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view

}

-(void) viewDidAppear:(BOOL)animated {
    NSLog(@"viewDidAppear %hhd",[[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"]);
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"]) [self performSegueWithIdentifier:@"QSetUp" sender:self];}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
