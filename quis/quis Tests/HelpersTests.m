//
//  HelpersTests.m
//  quis
//
//  Created by Will Skinner on 12/14/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "JsonStreamManager.h"

@interface HelpersTests : XCTestCase

@end

@implementation HelpersTests

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testSanity
{
    ////////////// sanity
    NSString *someJson = @"{\"nonce\":\"dmIzvicqJn3DMyGnobb7Cb1rGaoarT9Q\"}";
    JsonStreamManager *manager = [[JsonStreamManager alloc] init];
    [manager addData:someJson];
    
    NSString *next = [manager nextJson];
    
    XCTAssertEqualObjects(someJson, next, @"not equal");
}

- (void)testSplit
{
    ///////////// 1 json split over two additions
    NSString *part1 = @"{\"nonce\":\"dmIzvicqJn";
    NSString *part2 = @"3DMyGnobb7Cb1rGaoarT9Q\"}";
    
    JsonStreamManager *manager = [[JsonStreamManager alloc] init];
    [manager addData:part1];
    NSString *shoulbBeNil = [manager nextJson];
    XCTAssertNil(shoulbBeNil, @"not nil after all");
    
    [manager addData:part2];
    NSString *expected = @"{\"nonce\":\"dmIzvicqJn3DMyGnobb7Cb1rGaoarT9Q\"}";

    XCTAssertEqualObjects(expected , [manager nextJson], @"not equal");
}

- (void)test2
{
    //////////// 2 jsons in a row
    NSString *json = @"{\"nonce\":\"dmIzvicqJn3DMyGnobb7Cb1rGaoarT9Q\"}{\"nonce\":\"dmIzvicqJn3DMyGnobb7Cb1rGaoarT9Q\"}";
    
    JsonStreamManager *manager = [[JsonStreamManager alloc] init];
    [manager addData:json];
    NSString *expected = @"{\"nonce\":\"dmIzvicqJn3DMyGnobb7Cb1rGaoarT9Q\"}";
    
    XCTAssertEqualObjects(expected, [manager nextJson], @"didn't match");
    XCTAssertEqualObjects(expected, [manager nextJson], @"didn't match");
    XCTAssertNil([manager nextJson], @"didn't match");
}

@end
