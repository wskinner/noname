//
//  quis_Tests.m
//  quis Tests
//
//  Created by Will Skinner on 10/26/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SecQContacts.h"
#import "SecQMessage.h"
#import "KeyPair.h"
#import "ManagedObjectHelper.h"
#import "Certificate.h"
#import "QKeyPair.h"

@interface quis_Tests : XCTestCase

@end

@implementation quis_Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

////////////////////////////////// Helper methods
// Assumes shallow dictionaries with string values...
- (BOOL)dictionary:(NSDictionary *)dict isEqualTo:(NSDictionary *)otherDict {
    for (id key in dict) {
        if (![otherDict objectForKey:key] || !([[dict objectForKey:key] isEqualToString:[otherDict objectForKey:key]])) {
            return false;
        }
    }
    return true;
}


///////////////////////////////////////////////////////////////////////////////



- (void)testEncryptDecrypt {
    ManagedObjectHelper *ad = [[ManagedObjectHelper alloc] init];
    NSManagedObjectContext *moc = [ad managedObjectContext];
    SecQContacts *sender = (SecQContacts *)[NSEntityDescription
                                    insertNewObjectForEntityForName:@"SecQContacts"
                                    inManagedObjectContext:moc];
    [sender generateKeyPairs];
    SecQContacts *receiver = (SecQContacts *)[NSEntityDescription
                                            insertNewObjectForEntityForName:@"SecQContacts"
                                            inManagedObjectContext:moc];
    
    [receiver generateKeyPairs];
    SecQMessage *mess = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:moc];
    [mess initializeForRecipient:receiver fromSender:sender];
    
    [sender describeKeys];
    [receiver describeKeys];
    
    NSData *initialContent = [@"super secret message" dataUsingEncoding:NSUTF8StringEncoding];
    [mess setContent:initialContent];
    [mess encrypt];
    
    NSString *content = [mess.content base64EncodedString];
    NSLog(@"Encrypted text: %@", content);
    XCTAssert([mess isEncrypted], @"encrypted");
    XCTAssertNotEqualObjects([mess content], initialContent, @"jlkj");
    [mess decrypt];
    content = [mess.content base64EncodedString];
    NSLog(@"Decrypted text: %@ = '%@'", content, [content base64DecodedString]);
    NSLog(@"mess.isEncrypted: %@", mess.isEncrypted);
    XCTAssert(!mess.isEncrypted.boolValue, @"decrypted");
    XCTAssertEqualObjects([mess content], initialContent, @"success");
}

- (void)testLoop {
    for (int i = 0; i<100; i++) {
        [self testEncryptDecrypt];
    }
}

- (void)testEncryptAndSerialize {
    ManagedObjectHelper *ad = [[ManagedObjectHelper alloc] init];
    NSManagedObjectContext *moc = [ad managedObjectContext];
    SecQContacts *sender = (SecQContacts *)[NSEntityDescription
                                            insertNewObjectForEntityForName:@"SecQContacts"
                                            inManagedObjectContext:moc];
    [sender generateKeyPairs];
    SecQContacts *receiver = (SecQContacts *)[NSEntityDescription
                                              insertNewObjectForEntityForName:@"SecQContacts"
                                              inManagedObjectContext:moc];
    
    [receiver generateKeyPairs];
    
    SecQMessage *mess = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:moc];
    [mess initializeForRecipient:receiver fromSender:sender];
    
    [sender describeKeys];
    [receiver describeKeys];
    
    NSString *initialContentString = @"super secret message";
    NSData *initialContent = [initialContentString dataUsingEncoding:NSUTF8StringEncoding];
    mess.content = initialContent;
    
    NSLog(@"mess nonce: %@", [mess.nonce base64EncodedString]);
    NSLog(@"Pre encryption: %@", [mess.content base64EncodedString]);
    [mess encrypt];
    NSLog(@"Post encryption: %@", [mess.content base64EncodedString]);
    
    NSData *blob = [mess toBase64Blob];
    
    // ---------------------------------------------------------------------------
    SecQMessage *receipt = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:moc];
    [receipt initializeFromBase64Blob:blob];
    [receipt initializeForRecipient:receiver fromSender:sender];
    
    XCTAssertEqualObjects(mess.content, receipt.content, @"not equal");
    XCTAssertEqualObjects(mess.date, receipt.date, @"not equal");
    XCTAssertEqualObjects(mess.nonce, receipt.nonce, @"not equal");
    XCTAssertEqualObjects(mess.isEncrypted, receipt.isEncrypted, @"not equal");
    XCTAssertEqualObjects(mess.messageId, receipt.messageId, @"not equal");
    XCTAssertEqualObjects(mess.messageType, receipt.messageType, @"not equal");
    
    [receipt decrypt];
    XCTAssertTrue([receipt.isEncrypted isEqual:@NO], @"receipt should not be encrypted at this point");
    XCTAssertEqualObjects(receipt.content, initialContent, @"content should be equal");


}

- (void)testRepeatedEncryption {
    ManagedObjectHelper *ad = [[ManagedObjectHelper alloc] init];
    NSManagedObjectContext *moc = [ad managedObjectContext];
    SecQContacts *sender = (SecQContacts *)[NSEntityDescription
                                            insertNewObjectForEntityForName:@"SecQContacts"
                                            inManagedObjectContext:moc];
    [sender generateKeyPairs];
    SecQContacts *receiver = (SecQContacts *)[NSEntityDescription
                                              insertNewObjectForEntityForName:@"SecQContacts"
                                              inManagedObjectContext:moc];
    [receiver generateKeyPairs];
    
    SecQMessage *mess = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:moc];
    [mess initializeForRecipient:receiver fromSender:sender];
    
    for (int i = 0; i<100; i++) {
        SecQMessage *mess = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:moc];
        [sender generateKeyPairs];
        [receiver generateKeyPairs];
        [mess initializeForRecipient:receiver fromSender:sender];
        NSString *initialContentString = @"super secret message";
        NSData *initialContent = [initialContentString dataUsingEncoding:NSUTF8StringEncoding];
        
        mess.content = initialContent;
        [mess encrypt];
        XCTAssertNotEqualObjects([[NSString alloc] initWithData:mess.content encoding:NSUTF8StringEncoding], initialContentString, @"none");
        [mess decrypt];
        XCTAssertEqualObjects([[NSString alloc] initWithData:mess.content encoding:NSUTF8StringEncoding], initialContentString, @"none");
        if (i % 100 == 0)
            NSLog(@"%d iterations completed", i);
    }
}

- (void)testSoAnnoying {
    ManagedObjectHelper *ad = [[ManagedObjectHelper alloc] init];
    NSManagedObjectContext *moc = [ad managedObjectContext];
    SecQContacts *sender = (SecQContacts *)[NSEntityDescription
                                            insertNewObjectForEntityForName:@"SecQContacts"
                                            inManagedObjectContext:moc];
    [sender generateKeyPairs];
    SecQContacts *receiver = (SecQContacts *)[NSEntityDescription
                                              insertNewObjectForEntityForName:@"SecQContacts"
                                              inManagedObjectContext:moc];
    [receiver generateKeyPairs];
    
    SecQMessage *mess = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:moc];
    [mess initializeForRecipient:receiver fromSender:sender];
    
    [sender describeKeys];
    [receiver describeKeys];
    
    NSString *initialContentString = @"super secret message";
    NSData *initialContent = [initialContentString dataUsingEncoding:NSUTF8StringEncoding];
    
    mess.content = initialContent;
    [mess encrypt];
    

    XCTAssertNotEqualObjects([[NSString alloc] initWithData:mess.content encoding:NSUTF8StringEncoding], initialContentString, @"not");
    [mess decrypt];
    XCTAssertEqualObjects([[NSString alloc] initWithData:mess.content encoding:NSUTF8StringEncoding], initialContentString, @"not");
    
    [mess encrypt];
    
    NSData *blob = [mess toBase64Blob];

    SecQMessage *mess2 = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:moc];
    [mess2 initializeForRecipient:receiver fromSender:sender];
    [mess2 initializeFromBase64Blob:blob];
    
    XCTAssertEqualObjects([mess.content base64EncodedString], [mess2.content base64EncodedString], @"not equal");
    XCTAssertEqualObjects(mess.date, mess2.date, @"not equal");
    XCTAssertEqualObjects([mess.nonce base64EncodedString], [mess2.nonce base64EncodedString], @"not equal");
    XCTAssertEqualObjects(mess.isEncrypted, mess2.isEncrypted, @"not equal");
    XCTAssertEqualObjects(mess.messageId, mess2.messageId, @"not equal");
    XCTAssertEqualObjects(mess.messageType, mess2.messageType, @"not equal");
    XCTAssertTrue(mess2.isEncrypted, @"fuck");
    
    //mess.content = mess2.content;
    //mess.nonce = mess2.nonce;
    //mess.sender.encryptionKeyPublic = mess2.sender.encryptionKeyPublic;
    //mess.sender.encryptionKeyPrivate = mess2.sender.encryptionKeyPrivate;
    //mess.recipient.encryptionKeyPublic = mess2.recipient.encryptionKeyPublic;
    //mess.recipient.encryptionKeyPrivate = mess2.recipient.encryptionKeyPrivate;
    
    
    
    XCTAssertNotEqualObjects([[NSString alloc] initWithData:mess2.content encoding:NSUTF8StringEncoding], initialContentString, @"not");
    [mess2 decrypt];
    XCTAssertEqualObjects([[NSString alloc] initWithData:mess2.content encoding:NSUTF8StringEncoding], initialContentString, @"not");
    

}

- (void)testSanity {
    ManagedObjectHelper *ad = [[ManagedObjectHelper alloc] init];
    NSManagedObjectContext *moc = [ad managedObjectContext];
    SecQContacts *sender = (SecQContacts *)[NSEntityDescription
                                            insertNewObjectForEntityForName:@"SecQContacts"
                                            inManagedObjectContext:moc];
    [sender generateKeyPairs];
    SecQContacts *receiver = (SecQContacts *)[NSEntityDescription
                                              insertNewObjectForEntityForName:@"SecQContacts"
                                              inManagedObjectContext:moc];
    [receiver generateKeyPairs];
    
    SecQMessage *mess = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:moc];
    [mess initializeForRecipient:receiver fromSender:sender];
    
    [sender describeKeys];
    [receiver describeKeys];
    
    NSString *initialContentString = @"super secret message";
    NSData *initialContent = [initialContentString dataUsingEncoding:NSUTF8StringEncoding];
    mess.content = initialContent;
    
    [mess encrypt];
    
    SecQMessage *mess2 = (SecQMessage *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQMessage" inManagedObjectContext:moc];
    [mess2 initializeForRecipient:receiver fromSender:sender];
    mess2.content = mess.content;
    mess2.nonce = mess.nonce;
    mess2.isEncrypted = @YES;
    
    XCTAssertNotEqualObjects([[NSString alloc] initWithData:mess.content encoding:NSUTF8StringEncoding], initialContentString, @"not");
    [mess decrypt];
    XCTAssertEqualObjects([[NSString alloc] initWithData:mess.content encoding:NSUTF8StringEncoding], initialContentString, @"not");
    
    XCTAssertNotEqualObjects([[NSString alloc] initWithData:mess2.content encoding:NSUTF8StringEncoding], initialContentString, @"not");
    [mess2 decrypt];
    XCTAssertEqualObjects([[NSString alloc] initWithData:mess2.content encoding:NSUTF8StringEncoding], initialContentString, @"not");

}

/////////////////////// Certificates

// A valid certificate should be verified.
- (void)testVerifyCert {
    ManagedObjectHelper *ad = [[ManagedObjectHelper alloc] init];
    NSManagedObjectContext *moc = [ad managedObjectContext];
    SecQContacts *subject = (SecQContacts *)[NSEntityDescription
                                    insertNewObjectForEntityForName:@"SecQContacts"
                                    inManagedObjectContext:moc];
    [subject generateKeyPairs];
    subject.firstName = @"Randy Waterhouse";
    subject.emailAddress = @"waterhouse@epiphyte.net";
    
    Certificate *cert = [[Certificate alloc] initForQContacts:subject];
    [cert sign];
    NSLog(@"Signed the cert");
    NSLog(@"The certificate: %@", [[NSString alloc] initWithData:[cert toBlob] encoding:NSUTF8StringEncoding]);
    
    XCTAssertTrue([cert verifySignature], @"Certificate verification failed: ");
}

// The proper key is replaced with an imposter's key.
- (void)testCertBadKey {
    ManagedObjectHelper *ad = [[ManagedObjectHelper alloc] init];
    NSManagedObjectContext *moc = [ad managedObjectContext];
    SecQContacts *subject = (SecQContacts *)[NSEntityDescription
                                             insertNewObjectForEntityForName:@"SecQContacts"
                                             inManagedObjectContext:moc];
    [subject generateKeyPairs];
    subject.firstName = @"Randy Waterhouse";
    subject.emailAddress = @"waterhouse@epiphyte.net";
    Certificate *cert = [[Certificate alloc] initForQContacts:subject];
    [cert sign];
    
    SecQContacts *mallory = (SecQContacts *)[NSEntityDescription insertNewObjectForEntityForName:@"SecQContacts" inManagedObjectContext:moc];
    [mallory generateKeyPairs];
    
    [cert.keyInfo setValue:[mallory.encryptionKeyPublic base64EncodedString] forKey:@"publicKey"];
    
    XCTAssertFalse([cert verifySignature], @"Certificate verification failed");
}

// Test of the basic signature functionality.
- (void)testSignature {
    QKeyPair *kp = [[QKeyPair alloc] init];
    
    NSString *msg = @"foobar";
    NSData *msgData = [msg dataUsingEncoding:NSUTF8StringEncoding];
    NSData *signedData = [CryptoAPI sign:msgData withKeyPair:kp];
    
    XCTAssertTrue([CryptoAPI verifySignedData:signedData withSignerKeyPair:kp], @"Verification");
}

// Create a cert, serialize it, and deserialize.
- (void)testCertSerialization {
    ManagedObjectHelper *ad = [[ManagedObjectHelper alloc] init];
    NSManagedObjectContext *moc = [ad managedObjectContext];
    SecQContacts *subject = (SecQContacts *)[NSEntityDescription
                                    insertNewObjectForEntityForName:@"SecQContacts"
                                             inManagedObjectContext:moc];
    [subject generateKeyPairs];
    subject.firstName = @"Randy Waterhouse";
    subject.emailAddress = @"waterhouse@epiphyte.net";
    Certificate *cert = [[Certificate alloc] initForQContacts:subject];
    [cert sign];
    
    NSData *certBlob = [cert toBlob];
    
    Certificate *newCert = [[Certificate alloc] initFromBlob:certBlob];
    XCTAssertTrue([self dictionary:cert.keyInfo isEqualTo:newCert.keyInfo], @"Keys are equal");
    XCTAssertTrue([self dictionary:cert.keyInfo isEqualTo:newCert.keyInfo], @"Subjects are equal");
    XCTAssertTrue([self dictionary:cert.keyInfo isEqualTo:newCert.keyInfo], @"Certificate info is equal");
    XCTAssertTrue([cert.signature isEqualToString:newCert.signature], @"Signatures are equal");
}


- (void)testRaw {
    unsigned char *pk_sender = malloc(crypto_box_PUBLICKEYBYTES * sizeof(unsigned char));
    unsigned char *sk_sender = malloc(crypto_box_SECRETKEYBYTES * sizeof(unsigned char));
    unsigned char *pk_receiver = malloc(crypto_box_PUBLICKEYBYTES * sizeof(unsigned char));
    unsigned char *sk_receiver = malloc(crypto_box_SECRETKEYBYTES * sizeof(unsigned char));
    unsigned char *n = malloc(crypto_box_NONCEBYTES * sizeof(unsigned char));
    for (int i = 0; i<crypto_box_NONCEBYTES; i++) {
        n[i] = 'a';
    }

    const char *mess = "abcdefg";
    unsigned long mlen = strlen(mess) + crypto_box_ZEROBYTES;
    char *m = malloc(mlen * sizeof(unsigned char));
    unsigned char *c = malloc(mlen * sizeof(unsigned char));
    
    unsigned char *p = malloc(mlen);
    char *result = malloc(strlen(mess) * sizeof(unsigned char));

    for (int i = 0; i<100; i++) {
        if (i % 100 == 0) {
            NSLog(@"%d iterations completed", i);
        }
        // generate a new keypair
        crypto_box_keypair(pk_sender,sk_sender);
        crypto_box_keypair(pk_receiver,sk_receiver);
        
        // copy over the zerobytes
        for (int j = 0; j<crypto_box_ZEROBYTES; j++) {
            m[j] = 0;
        }
        
        // copy over the message
        memcpy(m + crypto_box_ZEROBYTES, mess, strlen(mess));
        
        // encrypt
        int status = crypto_box(c,(const unsigned char *)m,mlen,n,pk_receiver,sk_sender);
        if (status != 0) {
            XCTFail(@"encryption failed");
        }
        
        
        // decrypt
        status = crypto_box_open(p,c,mlen,n,pk_sender,sk_receiver);
        if (status != 0) {
            XCTFail(@"decryption failed");
        }
        
        int j = 0;
        for (; j<strlen(mess); j++) {
            result[j] = p[crypto_box_ZEROBYTES + j];
        }
        result[j] = '\0';
        if (strcmp(result, mess) != 0) {
            XCTFail(@"expected %s, but got %s", mess, result);
        }
    }
    
}


- (void)testQKeyPair {
    for (int i = 0; i<100; i++) {
        if (i % 100 == 0) {
            NSLog(@"%d iterations completed", i);
        }
        QKeyPair *sender = [[QKeyPair alloc] init];
        QKeyPair *receiver = [[QKeyPair alloc] init];
        
        NSString *msg = @"super secret message";
        NSData *data = [msg dataUsingEncoding:NSUTF8StringEncoding];
        
        NSData *nonce = [QKeyPair nextNonce];
        
        NSData *encrypted = [sender encryptData:data nonce:nonce forKeyPair:receiver];
        
        XCTAssertNotEqualObjects([[NSString alloc] initWithData:encrypted encoding:NSUTF8StringEncoding], msg, @"equal");
        
        NSData *decrypted = [receiver decryptData:encrypted nonce:nonce fromKeyPair:sender];
        
        XCTAssertEqualObjects([[NSString alloc] initWithData:decrypted encoding:NSUTF8StringEncoding], msg, @"not equal");
    }
}



@end
