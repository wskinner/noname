//
//  ManagedObjectHelper.h
//  quis
//
//  Created by Will Skinner on 10/27/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ManagedObjectHelper : NSObject
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) NSMutableDictionary* uidCache;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
