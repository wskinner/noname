
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// libsodium-ios
#define COCOAPODS_POD_AVAILABLE_libsodium_ios
#define COCOAPODS_VERSION_MAJOR_libsodium_ios 0
#define COCOAPODS_VERSION_MINOR_libsodium_ios 4
#define COCOAPODS_VERSION_PATCH_libsodium_ios 3

