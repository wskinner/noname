//
//  Tests.m
//  Tests
//
//  Created by Will Skinner on 10/26/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Tests : XCTestCase

@end

@implementation Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

- (void)testEncryptDecrypt {
    SecQContacts *sender = [[SecQContacts alloc] init];
    [sender generateKeyPairs];
    SecQContacts *receiver = [[SecQContacts alloc] init];
    
    SecQMessage *mess = [[SecQMessage alloc] initForRecipient:receiver  fromSender:sender];
    NSData *initialContent = [@"super secret message" dataUsingEncoding:NSUTF8StringEncoding];
    [mess setContent:initialContent];
    [mess encrypt];
    XCTAssert([mess isEncrypted], @"encrypted");
    XCTAssertNotEqualObjects([mess content], initialContent, @"jlkj");
    [mess decrypt];
    XCTAssert(![mess isEncrypted], @"decrypted");
    XCTAssertEqualObjects([mess content], initialContent, @"success");
}

@end
